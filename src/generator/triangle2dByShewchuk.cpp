#include "triangle2dByShewchuk.h"

using namespace geometry;


//
// Costruttori
//

triangle2dByShewchuk::triangle2dByShewchuk()
{
      belements = NULL;
      elements  = NULL;
      
      // faccio un reserve per i punti 
      holes.reserve(10);
      regions.reserve(10);
      area.reserve(10);
      geoIds.reserve(10);
}
		  
triangle2dByShewchuk::triangle2dByShewchuk(mesh1d<Line> * _belements, mesh2d<Triangle> * _elements)
{
      belements = _belements;
      elements  = _elements;
      
      // faccio un reserve per i punti 
      holes.reserve(10);
      regions.reserve(10);
      area.reserve(10);
      geoIds.reserve(10);
}
		 
//
// Cambio delle variabili
//
void triangle2dByShewchuk::setBelements(mesh1d<simplePoint> * _belements)
{
      assert(_belements!=NULL);
      cout << "Errore stai cercando di convertire una mesh0d in una mesh1d" << endl;
}

void triangle2dByShewchuk::insertHole(vector<point> * _hole)
{
      // faccio un resize
      holes.resize(_hole->size());
      
      // copio il contenuto 
      copy(_hole->begin(), _hole->end(), holes.begin());
}

void triangle2dByShewchuk::insertRegion(vector<point> * _regions, vector<Real> * _area, vector<UInt> * _geoIds)
{
      // faccio un resize
      regions.resize(_regions->size());
      area.resize(_area->size());
      geoIds.resize(_geoIds->size());
      
      // copio il contenuto 
      copy(_regions->begin(), _regions->end(), regions.begin());
      copy(_area->begin(),    _area->end(),    area.begin());
      copy(_geoIds->begin(),  _geoIds->end(),  geoIds.begin());
      
      // devono avere la stessa dimensione 
      assert(regions.size()==area.size());
      assert(geoIds.size()==area.size());
}

void triangle2dByShewchuk::insertElementArea(vector<Real> * _elemArea)
{
      // faccio un resize
      elemArea.resize(_elemArea->size());
      
      // copio i dati
      copy(_elemArea->begin(), _elemArea->end(), elemArea.begin());
}

void triangle2dByShewchuk::reset()
{
      belements = NULL;
      elements  = NULL;
      holes.clear();
      regions.clear();
      area.clear();
      geoIds.clear();
      elemArea.clear();
}


//
// Funzioni che creano l'input e output per triangle
//

void triangle2dByShewchuk::dataForShewchuk(triangulateio * in, bool ref)
{
	// varaibili in uso 
	UInt pos = 0;
	
	// ref ci dice se dobbiamo raffinare o meno una mesh in input
    	
	// informazioni sul numero di punti e sui loro attributi
	if(!ref)
	{
	  in->numberofpoints = belements->getNumNodes();
	  in->numberofpointattributes = 1;
	  in->pointlist = (double *) malloc(in->numberofpoints * 2 * sizeof(double));
	    
	  // riempimento della lista dei nodi per essere triangolata
	  for(UInt i=0; i<static_cast<UInt>(in->numberofpoints); ++i)
	  {
	  
	    in->pointlist[pos]   = belements->getNode(i).getX();
	    in->pointlist[pos+1] = belements->getNode(i).getY();
	  
	    pos = pos + 2;
	  }
	  	  
	  // riempimento della lista degli attributi dei punti
	  in->pointattributelist = (double *) malloc(in->numberofpoints * in->numberofpointattributes * sizeof(double));
	
	  for(UInt i=0; i<static_cast<UInt>(in->numberofpoints); ++i)
	  {
	      in->pointattributelist[i] = 1.0;
	  }
	
	  // riempimento della lista dei marker
	  in->pointmarkerlist = (int *) malloc(in->numberofpoints * sizeof(int));
	
	  for(UInt i=0; i<static_cast<UInt>(in->numberofpoints); ++i)
	  {
	      in->pointmarkerlist[i] = 0;
	  }
	
	  // creo la lista degli edge
	  in->numberofsegments = belements->getNumElements();
	  in->segmentlist = (int *) malloc(2 * in->numberofsegments * sizeof(int));
	
	  pos = 0;
	  for(UInt i=0; i<static_cast<UInt>(in->numberofsegments); ++i)
	  {
		in->segmentlist[pos]   = belements->getElement(i).getConnectedId(0);
		in->segmentlist[pos+1] = belements->getElement(i).getConnectedId(1);
			
		pos = pos + 2;
	  }
	  
	  // creo la lista dei loro marker
	  in->segmentmarkerlist = (int *) malloc(in->numberofsegments * sizeof(int));
	  for(UInt i=0; i<static_cast<UInt>(in->numberofsegments); ++i)
	  {
	      in->segmentmarkerlist[i] = 0 ;
	  }	
	}
	else
	{
	  in->numberofpoints = elements->getNumNodes();
	  in->numberofpointattributes = 1;
	  in->pointlist = (double *) malloc(in->numberofpoints * 2 * sizeof(double));
	    
	  // riempimento della lista dei nodi per essere triangolata
	  UInt pos = 0;
	  for(UInt i=0; i<static_cast<UInt>(in->numberofpoints); ++i)
	  {
	  
	    in->pointlist[pos]   = elements->getNode(i).getX();
	    in->pointlist[pos+1] = elements->getNode(i).getY();
	  
	    pos = pos + 2;
	  }
	  	  
	  // riempimento della lista degli attributi dei punti
	  in->pointattributelist = (double *) malloc(in->numberofpoints * in->numberofpointattributes * sizeof(double));
	
	  for(UInt i=0; i<static_cast<UInt>(in->numberofpoints); ++i)
	  {
	      in->pointattributelist[i] = 1.0;
	  }
	
	  // riempimento della lista dei marker
	  in->pointmarkerlist = (int *) malloc(in->numberofpoints * sizeof(int));
	
	  for(UInt i=0; i<static_cast<UInt>(in->numberofpoints); ++i)
	  {
	      in->pointmarkerlist[i] = 0;
	  }
	  
	  // creo la lista dei triangoli
	  in->numberoftriangles = elements->getNumElements();
	  in->trianglelist = (int *) malloc(3 * in->numberoftriangles * sizeof(int));

	  pos = 0;
	  for(UInt i=0; i<static_cast<UInt>(in->numberoftriangles); ++i)
	  {
		in->trianglelist[pos]   = elements->getElement(i).getConnectedId(0);
		in->trianglelist[pos+1] = elements->getElement(i).getConnectedId(1);
		in->trianglelist[pos+2] = elements->getElement(i).getConnectedId(2);
		
		pos = pos + 3;
	  }

	  in->numberofcorners = 3;
	  in->numberoftriangleattributes= 1;
	  in->triangleattributelist = (double *) malloc(in->numberoftriangles * sizeof(double));
	  for(UInt i=0; i<static_cast<UInt>(in->numberoftriangles); ++i)
	  {
		  in->triangleattributelist[i] = elements->getElement(i).getGeoId();
	  }
	
	  // parte dedicata ai size dei triangoli quando raffino una mesh preesistente
	  if(elemArea.size()!=0)
	  {
	    // controllo che ci siano tutti i triangoli
	    assert(elemArea.size()==static_cast<UInt>(in->numberoftriangles));
	    
	    // li metto nei dati 
	    in->trianglearealist = (double *) malloc(in->numberoftriangles * sizeof(double));
	    for(UInt i=0; i<static_cast<UInt>(in->numberoftriangles); ++i)
	    {
		    in->trianglearealist[i] = elemArea[i];
	    }
	  }
	}
	
	// informazioni topologiche dei buchi
	pos = 0;
	in->numberofholes = holes.size();
	in->holelist = (double *) malloc(in->numberofholes * 2 * sizeof(double));
	for(UInt i=0; i<static_cast<UInt>(in->numberofholes); ++i)
	{
		in->holelist[pos]   = holes[i].getX();
		in->holelist[pos+1] = holes[i].getY();
		
		pos = pos + 2;
	}
	
	// informazioni sui vincoli sulla regione 
	in->numberofregions = regions.size();
	if(regions.size()!=0)
	{
	    in->regionlist = (double *) malloc(in->numberofregions * 4 * sizeof(double));
	    for(UInt i=0; i<static_cast<UInt>(in->numberofregions); ++i)
	    {
		  in->regionlist[pos]   = regions[i].getX();
		  in->regionlist[pos+1] = regions[i].getY();
		  in->regionlist[pos+2] = geoIds[i];
		  in->regionlist[pos+3] = area[i];
		
		  pos = pos + 4;
	    }
	}
}
		  
void triangle2dByShewchuk::dataFromShewchuk(triangulateio * out)
{
	// pulisco le liste
	elements->clear();
	
	// faccio i reserve
	elements->getNodePointer()->resize(static_cast<UInt>(out->numberofpoints));
	elements->getElementPointer()->resize(static_cast<UInt>(out->numberoftriangles));
	
	// riempio la lista dei nodi
	point p;
	UInt pos = 0;
	for(UInt i=0; i<static_cast<UInt>(out->numberofpoints); ++i)
	{  
		// prendo le informazioni 
		p.setX(out->pointlist[pos]);
		p.setY(out->pointlist[pos+1]);		
		p.setZ(0.0);
		p.setId(i);
		
		// le salvo
		elements->getNodePointer()->at(i) = p;
		
		// incremento pos che mi serve per esplorare i punti in out
		pos = pos + 2;
	}
	
	// riempio la lista dei triangloli
	geoElement<Triangle>  tria;
	pos = 0;
	for(UInt i=0; i<static_cast<UInt>(out->numberoftriangles); ++i)
	{
 
		// creo il triangolo 
		tria.setConnectedId(0, out->trianglelist[pos]);
		tria.setConnectedId(1, out->trianglelist[pos+1]);
		tria.setConnectedId(2, out->trianglelist[pos+2]);
		tria.setId(i);	tria.setGeoId(0);
		
		// salvo
		elements->getElementPointer()->at(i) = tria;			
		
		// incremento pos che mi serve per esplorare gli elementi in out
		pos = pos + 3;
	}
}

void triangle2dByShewchuk::inizializeForShewchuk(triangulateio * out)
{
	// inizializzo le quantità per l'output
	out->pointlist = (double *) NULL;      
  	out->pointattributelist = (double *) NULL;
  	out->pointmarkerlist = (int *) NULL; 
  	out->trianglelist = (int *) NULL;    
  	out->triangleattributelist = (double *) NULL;	
  	out->neighborlist = (int *) NULL;    
  	out->segmentlist = (int *) NULL;
  	out->segmentmarkerlist = (int *) NULL;
  	out->edgelist = (int *) NULL;   
  	out->edgemarkerlist = (int *) NULL;
	out->regionlist = (double *) NULL;
}

void triangle2dByShewchuk::freeMem(triangulateio * str)
{
   	free(str->pointlist);
   	free(str->pointattributelist);
   	free(str->pointmarkerlist);
   	free(str->trianglelist);
   	free(str->triangleattributelist);
   	free(str->segmentlist);
  	free(str->segmentmarkerlist);
}
	    
//
// Funzione che genera i triangloi
//

void triangle2dByShewchuk::createConnection2d()
{
	// struttura dati per il programma triangle
	triangulateio in,out,vor;
	
	// inizializzo la struttura di output e input
	inizializeForShewchuk(&out);
	inizializeForShewchuk(&in);
	  
	// carico i dati nella struttura in
	dataForShewchuk(&in); 
	
	// creo la riga di comando per il comando triangulate
	char command [1024];
	sprintf(command,"pczYY");

	// eseguo la triangolazione e metto tutto nella struttura out
	triangulate(command, &in, &out, &vor);
	
	// salvo i dati nella struttura della classe
	dataFromShewchuk(&out);
	
	// pulisco la memoria
	freeMem(&out);
	freeMem(&in);
}

void triangle2dByShewchuk::createMesh2d(bool addOnBound)
{
	// struttura dati per il programma triangle
	triangulateio in,out,vor;
	
	// inizializzo la struttura di output e input
	inizializeForShewchuk(&out);
	inizializeForShewchuk(&in);
	  
	// carico i dati nella struttura in
	dataForShewchuk(&in); 
	
	// creo la riga di comando per il comando triangulate
	char command [1024];
	
	if(addOnBound)
	{
	  // creo la riga di comando per il comando triangulate
	  sprintf(command,"pDq33z");
	}
	else
	{
	  // creo la riga di comando per il comando triangulate
	  sprintf(command,"pYYz");
	}

	// eseguo la triangolazione e metto tutto nella struttura out
	triangulate(command, &in, &out, &vor);
	
	// salvo i dati nella struttura della classe
	dataFromShewchuk(&out);
	
	// pulisco la memoria
	freeMem(&out);
	freeMem(&in);
}

void triangle2dByShewchuk::createMesh2d(Real area, bool addOnBound)
{
	// struttura dati per il programma triangle
	triangulateio in,out,vor;
	char command [1024];
	double areaTmp = static_cast<double>(area);
	
	// inizializzo la struttura di output e input
	inizializeForShewchuk(&out);
	inizializeForShewchuk(&in);
	  
	// carico i dati nella struttura in
	dataForShewchuk(&in);
	
	if(addOnBound)
	{
	  // creo la riga di comando per il comando triangulate
	  sprintf(command,"pzDq33a%f",areaTmp);
	  //	n = sprintf(command,"pza%f",area);
	}
	else
	{
	  // creo la riga di comando per il comando triangulate
	  sprintf(command,"pzDYYa%f",areaTmp);
	}
	
	// eseguo la triangolazione e metto tutto nella struttura out
	triangulate(command, &in, &out, &vor);
	
	// salvo i dati nella struttura della classe
	dataFromShewchuk(&out);
	
	// pulisco la memoria
	freeMem(&out);
	freeMem(&in);
}

void triangle2dByShewchuk::refine(Real area)
{
	// struttura dati per il programma triangle
	triangulateio in,out,vor;
	double areaTmp = static_cast<double>(area);
	
	// inizializzo la struttura di output e input
	inizializeForShewchuk(&out);
	inizializeForShewchuk(&in);
	  
	// carico i dati nella struttura in
	dataForShewchuk(&in, true);
	
	// creo la riga di comando per il comando triangulate
	char command [1024];
	sprintf(command,"rzDq33a%f",areaTmp);
	
	// eseguo la triangolazione e metto tutto nella struttura out
	triangulate(command, &in, &out, &vor);
	
	// salvo i dati nella struttura della classe
	dataFromShewchuk(&out); 
	
	// pulisco la memoria
	freeMem(&out);
	freeMem(&in);
}

void triangle2dByShewchuk::refine()
{
	// struttura dati per il programma triangle
	triangulateio in,out,vor;
	
	// inizializzo la struttura di output e input
	inizializeForShewchuk(&out);
	inizializeForShewchuk(&in);
	  
	// carico i dati nella struttura in
	dataForShewchuk(&in, true);
	
	// creo la riga di comando per il comando triangulate
	char command [1024];
	sprintf(command,"rzDq33a");
	
	// eseguo la triangolazione e metto tutto nella struttura out
	triangulate(command, &in, &out, &vor);
	
	// salvo i dati nella struttura della classe
	dataFromShewchuk(&out); 
	
	// pulisco la memoria
	freeMem(&out);
	freeMem(&in);
}

void triangle2dByShewchuk::createMesh2dNoUniform(bool addOnBound)
{
	// struttura dati per il programma triangle
	triangulateio in,out,vor;
	
	// inizializzo la struttura di output e input
	inizializeForShewchuk(&out);
	inizializeForShewchuk(&in);
	  
	// carico i dati nella struttura in
	dataForShewchuk(&in); 
	
	// creo la riga di comando per il comando triangulate
	char command [1024];
	
	if(addOnBound)
	{
	  // creo la riga di comando per il comando triangulate
	  sprintf(command,"pDaq33z");
	}
	else
	{
	  // creo la riga di comando per il comando triangulate
	  sprintf(command,"pYYzA");
	}

	// eseguo la triangolazione e metto tutto nella struttura out
	triangulate(command, &in, &out, &vor);
	
	// salvo i dati nella struttura della classe
	dataFromShewchuk(&out);
	
	// pulisco la memoria
	freeMem(&out);
	freeMem(&in);
}

void triangle2dByShewchuk::createMesh2d(point pMax, point pMin, UInt divX, UInt divY, bool updateBor)
{
      // variabili in uso 
      UInt		      cont=0,tmp=0;
      Real 		passoX,passoY,zero;
      connect2d<Triangle>	      conn;
      geoElement<Triangle>	      tria;
      map<UInt,UInt>		 borToSurf;
      
      // setto zero 
      // 
      // lo faccio così per il problema di quando si decide di usare dei long double per via dell'errore ambigus
      // 
      zero = 0.0;
      
      // faccio un resize 
      elements->clear();
      elements->getNodePointer()->resize((divX+1)*(divY+1));
      
      // calcolo il passo 
      passoX = (pMax.getX()-pMin.getX())/static_cast<Real>(divX);
      passoY = (pMax.getY()-pMin.getY())/static_cast<Real>(divY);
      
      // metto i nodi
      for(UInt i=0; i<(divX+1); ++i)
      {
	  for(UInt j=0; j<(divY+1); ++j)
	  {
	    elements->getNodePointer(j+i*(divY+1))->setX(pMin.getX()+passoX*static_cast<Real>(i));
	    elements->getNodePointer(j+i*(divY+1))->setY(pMin.getY()+passoY*static_cast<Real>(j));
	    elements->getNodePointer(j+i*(divY+1))->setZ(zero);
	  }
      }
      
      // faccio un reserve 	
      elements->getElementPointer()->reserve(divX*divY*2);
      
      // setto tria
      tria.setGeoId(1);
	
      // faccio i triangoli
      for(UInt i=0; i<divX; ++i)
      {
	    for(UInt j=0; j<divY; ++j)
	    {
		tria.setConnectedId(0, j+i*(divY+1));
		tria.setConnectedId(1, j+(i+1)*(divY+1));
		tria.setConnectedId(2, j+1+i*(divY+1));
		tria.setId(cont);
		++cont;
		
		// metto l'elemento
		elements->insertElement(tria);
		
		tria.setConnectedId(0, j+1+i*(divY+1));
		tria.setConnectedId(1, j+(i+1)*(divY+1));
		tria.setConnectedId(2, j+1+(i+1)*(divY+1));
		tria.setId(cont);
		++cont;
		
		// metto l'elemento
		elements->insertElement(tria);
		
	    }
      }
      
      // guardo se devo aggiornare le connettività di bordo 
      if(updateBor)
      {
	  // setto le connettività
	  conn.setMeshPointer(elements);
	  conn.buildBoundaryConnectivity(belements, &borToSurf);
	  
	  // metto i nodi 
	  belements->insertNode(elements->getNodePointer());
	  
	  // salvo gli elementi 
	  for(UInt j=0; j<belements->getNumElements(); ++j)
	  {
	      tmp = borToSurf[belements->getElement(j).getConnectedId(0)];
	      belements->getElementPointer(j)->setConnectedId(0, tmp);
	      
	      tmp = borToSurf[belements->getElement(j).getConnectedId(1)];
	      belements->getElementPointer(j)->setConnectedId(1, tmp);
	  }
      }
      
      
      
}

