#ifndef TRIANGLE2DBYSHEWCHUK_H_
#define TRIANGLE2DBYSHEWCHUK_H_

#include <cassert>	
#include <cstdio>
#include <iostream>
#include <vector>

#include "../geometry/mesh0d.hpp"
#include "../geometry/mesh1d.hpp"
#include "../geometry/mesh2d.hpp"
#include "../geometry/connect1d.hpp"
#include "../geometry/connect2d.hpp"

#include "triangle.h"

namespace geometry
{

using namespace std;

/*! Questa classe ha come scopo quello di prendere in input un puntatore a una mesh1d e, sfruttando il programma triangle di
Shewchuk generare una mesh2d di alta qualità. Si presuppone che la mesh 1d o 2d data in input giaccia sul piano xOy.
Al suo interno contiene le seguenti funzionalità:
<ol>
<li> generare una mesh2d;
<li> generare un raffinamento di una mesh di partenza.
</ol>
Per il momento il programma si limita a generare mesh2d senza buchi */

class triangle2dByShewchuk
{
	  public:	  
		  /*! Puntatore a una mesh1d  */
		  mesh1d<Line>  *   belements;
		  
		  /*! Variabile che contiene la mesh2d */
		  mesh2d<Triangle> * elements;
		  
		  /*! Variabile che contiene le informazioni dei buchi, in triangle i buchi sono identificati da un punto
		  contenuto nel buco */
		  vector<point>	    	holes;
		  
		  /*! variabili che servono per fare una discretizzazione non uniforme 
		       regions -> identifica le regioni con un punto 
		       area    -> da il valore di area
		       geoIds  -> da il valore del geoIds della regione */
		  vector<point>      regions;   
		  vector<Real>          area;
		  vector<UInt>        geoIds;
		  
		  /*! Variabile che da il vincolo di area per guidare il processo di raffinamento della mesh */
		  vector<Real>      elemArea;
		  
	  //
	  // Costruttori
	  //
	  public:
		  /*! Costruttore di default */
		  triangle2dByShewchuk();
		  
		  /*! Costruttore che parte dalla mesh1d di bordo e setta il puntatore all'oggetto mesh che conterrà la mesh2d 
		      \param _belements puntatore alla mesh che contiene il bordo 
		      \param _elements puntatore alla mesh che conterrà la mesh2d */
		  triangle2dByShewchuk(mesh1d<Line> * _belements, mesh2d<Triangle> * _elements);
		   
	  //
	  // Cambio delle variabili
	  //
		  /*! Cambiamento della mesh1d di partenza 
		      \param _belements puntatore alla mesh che contiene solo i punti  
		      N.B. questo metodo non fa nulla serve essenzialmente per evitare errori nell'implementazione della classe
		      adding.hpp */
		  void setBelements(mesh1d<simplePoint> * _belements);
		  
		  /*! Cambiamento della mesh1d di partenza 
		      \param _belements puntatore alla mesh che contiene il bordo */
		  inline void setBelements(mesh1d<Line> * _belements);
		  
		  /*! Cambiamanto della mesh2d di arrivo 
		      \param _elements puntatore alla mesh che conterrà la mesh2d*/
		  inline void setElements(mesh2d<Triangle> * _elements);
		  
		  /*! inserimento buchi 
		      \param _hole punto*/
		  inline void insertHole(point _hole);
		  
		  /*! inserimento buchi 
		      \param _hole puntatore a un vettore che contiene i punti che identificano i buchi*/
		  void insertHole(vector<point> * _hole);
		  
		  /*! Inserimento delle condizioni sulle regioni 
		      \param _regions indice che indica le regioni 
		      \param _area vettore che da il valore delle are nelle regioni
		      \param _geoIds identificatore delle regioni */
		  void insertRegion(vector<point> * _regions, vector<Real> * _area, vector<UInt> * _geoIds);
		  
		  /*! Inserimento dei vincoli di area per i triangoli 
		      \param _elemArea vincolo di area per i triangoli */
		  void insertElementArea(vector<Real> * _elemArea);
		  
		  /*! Reset dei puntatori */
		  void reset();
		  
	  //
	  // Funzioni che creano l'input e output per triangle
	  //
		  /*! Trasformazione dei dati contenuti in belements in input per "triangle" 
		      \param in struttura utilizzata da triangle 
		      \param ref booleano che dice se devi raffinare */
		  void dataForShewchuk(triangulateio * in, bool ref=false);
		  
		  /*! Trasformazione dei dati contenuti nell'output di triangle in dati per mesh2d, aggiorna l'oggetto puntato da 
		      elements 
		      \param out struttura utilizzata da triangle */
		  void dataFromShewchuk(triangulateio * out);
		  
		  /*! metodo che inizializza una struttura dati di triangle 
		      \param out struttura utilizzata da triangle */
		  void inizializeForShewchuk(triangulateio * out);
		  
		  /*! Metodo che libera la memoria 
		      \param str struttura utilizzata da triangle */
		  void freeMem(triangulateio * str);
	    
	  //
	  // Funzione che genera i triangloi
	  //
		  /*! Trianglolazione */
		  void createConnection2d();
		  
		  /*! Trianglolazione 
		      \param addOnBound booleano che dice se aggiungere o meno i punti al bordo */
		  void createMesh2d(bool addOnBound=true);
		  
		  /*! Triangolazione con già il vincolo di area
		      \param area vincolo d'area sulla mesh 
		      \param addOnBound booleano che dice se aggiungere o meno i punti al bordo */
		  void createMesh2d(Real area, bool addOnBound=true);
		  
		  /*! Raffina una mesh in input secondo un vincolo d'area
		      \param area vincolo d'area sulla mesh */
		  void refine(Real area);
		  
		  /*! Raffina una mesh in input secondo un vincolo d'area definito sui triangoli */
		  void refine();
		  
		  /*! Genera una mesh tenendo presente il vincolo di area nelle varie regioni */
		  void createMesh2dNoUniform(bool addOnBound=true);
		  
		  /*! Genera una mesh "strutturata" di un rettangolo 
		      \param pMax punto massimo
		      \param pMin punto minimo
		      \param divX divisioni lungo le X
		      \param divY divisioni lungo le Y
		      \param updateBor booleano che dice se mettere a posto o meno la connettività di bordo 
		      N.B. è "strutturata" perché non è detto che le diagonali siano dirette nello stesso modo*/
		  void createMesh2d(point pMax, point pMin, UInt divX, UInt divY, bool updateBor=false);
};

// -----------------------------------------------------------------------------------------------
// 					INLINE FUNCTION
// -----------------------------------------------------------------------------------------------

inline void triangle2dByShewchuk::setBelements(mesh1d<Line> * _belements)
{
      belements = _belements;
}

inline void triangle2dByShewchuk::setElements(mesh2d<Triangle> * _elements)
{
      elements  = _elements;
}

inline void triangle2dByShewchuk::insertHole(point _hole)
{
      holes.push_back(_hole);
}


}

#endif
