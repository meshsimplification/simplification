#include "HSSmoothing.h"

using namespace std;
using namespace geometry;

//
// Costruttore 
//
HSSmoothing::HSSmoothing() : doctor2d<Triangle>(), 
                             alpha(0.1), beta(0.6), 
                             whenSwap(10)
{
}

HSSmoothing::HSSmoothing(mesh2d<Triangle> * _meshPointer, Real _alpha, Real _beta) 
  : doctor2d<Triangle>(_meshPointer), alpha(_alpha), beta(_beta), whenSwap(10)
{
}

//
// Basic methods to run the routine 
//
void HSSmoothing::runTheSmoothing(UInt iter)
{
    writeSmoothInfo(iter, 0.0);
    
    computePointToPointConnection(pointToPointConnection);
    initializeVectorsWithPoints();

    volumeSequence.resize(iter+1);
    volumeSequence[0] = computeVolume();
    
    for(UInt i=0; i<iter; ++i)
    {
        computeFistMoveForAllPoints();
        computeSecondMoveForAllPoints();

        moveAllThePoints(pPoints);

        volumeSequence[i+1] = computeVolume();
    }
    cout << "Smoothing done!!                                             \n";
}

void HSSmoothing::runTheSmoothingAccordingToTheVolume(Real percentage)
{
    writeSmoothInfo(0, percentage);

    computePointToPointConnection(pointToPointConnection);
    initializeVectorsWithPoints();

    Real initialVolume = computeVolume(); 
    queue<Real> queueOfVolumes;
    queueOfVolumes.push(initialVolume);
    
    queue<Real> queueOfQualities;
    Real oldQual = getMeanMeshQuality();
    queueOfQualities.push(oldQual);
    
    bool stopTheIteration = false;
    UInt maxIter = meshPointer->getNumNodes();
    UInt countIter = 0;
    
    vector<point> oldPositions;
    do  
    {
        // save the all points 
        saveActualPoints(oldPositions);

        computeFistMoveForAllPoints();
        computeSecondMoveForAllPoints();

        moveAllThePoints(pPoints);
        
        // compute all the geometrical quantities 
        Real actualVolume = computeVolume();
        Real actualQual = getMeanMeshQuality();
        Real actualPercent = fabs(actualVolume-initialVolume)/initialVolume;
        
        // check the geometrical quantities and eventually stop 
        if(actualPercent<percentage)
        {
            queueOfVolumes.push(actualVolume);
            queueOfQualities.push(actualQual);
            oldQual = actualQual;
        }
        else
        {
            moveAllThePoints(oldPositions);    
            stopTheIteration = true;
        }

        std::cout << "Iteration " << countIter << std::endl;

        if(countIter>maxIter)
        {
            std::cout << "!! !! !! Max number of iteration reached\n";
            stopTheIteration = true;
        }

        ++countIter;


    }while(!stopTheIteration);
    
    saveAllQueueData(queueOfVolumes, queueOfQualities);
    cout << "Smoothing done in " << volumeSequence.size() << " iterations                                           \n";
}

void HSSmoothing::runTheSmoothingAccordingToTheVolumeAndQuality(Real percentage, bool useSwap)
{
    writeSmoothInfo(0, percentage);

    computePointToPointConnection(pointToPointConnection);
    initializeVectorsWithPoints();

    Real initialVolume = computeVolume(); 
    queue<Real> queueOfVolumes;
    queueOfVolumes.push(initialVolume);
    
    queue<Real> queueOfQualities;
    Real oldQual = getMeanMeshQuality();
    queueOfQualities.push(oldQual);
    
    bool stopTheIteration = false;
    UInt maxIter = meshPointer->getNumNodes();
    UInt countIter = 0;
   
    vector<point> oldPositions;
    do
    {
        // save the all points 
        saveActualPoints(oldPositions);
        
        computeFistMoveForAllPoints();
        computeSecondMoveForAllPoints();

        moveAllThePoints(pPoints);
        
        // compute all the geometrical quantities 
        Real actualVolume = computeVolume();
        Real actualQual = getMeanMeshQuality();
        Real actualPercent = fabs(actualVolume-initialVolume)/initialVolume;

        // improve the quality of the mesh via the swapping
        if(useSwap && (countIter!=0) && (countIter%whenSwap==0))
        {
            actualQual = swapping();
            // we have to redo the connections
            computePointToPointConnection(pointToPointConnection);
        }
        
        // check the geometrical quantities and eventually stop 
        if(actualPercent<percentage && oldQual<actualQual)
        {
            queueOfVolumes.push(actualVolume);
            queueOfQualities.push(actualQual);
            oldQual = actualQual;
        }
        else
        {
            moveAllThePoints(oldPositions);    
            stopTheIteration = true;

            if(actualPercent>percentage)
                cout << "Stop for VOLUME constraint                  \n";
            if(oldQual>actualQual)
                cout << "Stop for QUALITY decrease                   \n";
        }

        std::cout << "Iteration " << countIter << std::endl;

        if(countIter>maxIter)
        {
            std::cout << "!! !! !! Max number of iteration reached\n";
            stopTheIteration = true;
        }

        ++countIter;
        
    }while(!stopTheIteration);
    

    saveAllQueueData(queueOfVolumes, queueOfQualities);
    
    
    cout << "Smoothing done in " << volumeSequence.size() << " iterations                                           \n";
}

//
//  Internal method to exploit for the smoothing 
//

void HSSmoothing::findFirstMove(UInt nodeId, const std::vector<UInt> & actualPointConnection)
{
    point pTmp(0.,0.,0.);
    for(UInt i=0; i<actualPointConnection.size(); ++i)
        pTmp.add(meshPointer->getNode(actualPointConnection[i]));
    point pTmp2 = pTmp * (1./static_cast<Real>(actualPointConnection.size()));
    bPoints[nodeId] = pTmp2 - (originalPoints[nodeId] * alpha + meshPointer->getNode(nodeId) * (1.-alpha));
}

void HSSmoothing::findSecondMove(UInt nodeId, const std::vector<UInt> & actualPointConnection)
{
    point pTmp(0.,0.,0.);
    for(UInt i=0; i<actualPointConnection.size(); ++i)
        pTmp.add(bPoints[actualPointConnection[i]]);
    point pTmp2 = pTmp * (1.-beta)/static_cast<Real>(actualPointConnection.size());
    pPoints[nodeId] = pPoints[nodeId] + (bPoints[nodeId] * beta + pTmp2);
}

void HSSmoothing::computeFistMoveForAllPoints()
{
    for(UInt nodeId=0; nodeId<meshPointer->getNumNodes(); ++nodeId)
        findFirstMove(nodeId, pointToPointConnection[nodeId]);
}

void HSSmoothing::computeSecondMoveForAllPoints()
{
    for(UInt nodeId=0; nodeId<meshPointer->getNumNodes(); ++nodeId)
        findSecondMove(nodeId, pointToPointConnection[nodeId]);
}


void HSSmoothing::moveAllThePoints(vector<point> & newPositions)
{
    for(UInt nodeId = 0; nodeId<meshPointer->getNumNodes(); ++nodeId)
        changeNode(nodeId, newPositions[nodeId]);
}

void HSSmoothing::computePointToPointConnection(vector<vector<UInt> > & pointToPointConnection)
{
    pointToPointConnection.clear();
    pointToPointConnection.resize(meshPointer->getNumNodes());
    
    for(UInt i=0; i<pointToPointConnection.size(); ++i)
        createStellata(i, &pointToPointConnection[i]);
}

void HSSmoothing::initializeVectorsWithPoints()
{
    originalPoints.resize(meshPointer->getNumNodes());
    for(UInt nodeId = 0; nodeId<meshPointer->getNumNodes(); ++nodeId)
        originalPoints[nodeId] = meshPointer->getNode(nodeId);
    bPoints = originalPoints;
    pPoints = originalPoints;
}

void HSSmoothing::saveActualPoints(vector<point> & actualPoints) const
{
    if(actualPoints.size()!=meshPointer->getNumNodes())
        actualPoints.resize(meshPointer->getNumNodes());

    // save the all points 
    for(UInt i=0; i<meshPointer->getNumNodes(); ++i)
        actualPoints[i] = meshPointer->getNode(i);
}

//
// Method to compute the volume 
//
Real HSSmoothing::computeVolume()
{
    //
    // compute the volume via the divergence theorem
    //
    Real volume = 0.;
    for(UInt i=0; i<meshPointer->getNumElements(); ++i)
    {
        point normal = getTriangleNormal(i);
        Real area = getTriangleArea(i);
        
        std::vector<point> listOfPoints;
        meshPointer->getNodeOfElement(i, &listOfPoints);
        
        point pBar;
        pBar.replace(listOfPoints[0], listOfPoints[1], listOfPoints[2], 1./3.);
    
        volume += area * pBar.getX() * normal.getX();
    }
    
    return(volume);
}
                                 
void HSSmoothing::writeMatlabFileWithVolumes(string filename)
{
    cout << "Writing file "  << filename << endl;
    ofstream out(filename.c_str());
                                 
    out << "A = [ ";
    for(UInt i=0; i<volumeSequence.size(); ++i)
        out << volumeSequence[i] << "  " << (fabs(volumeSequence[0] - volumeSequence[i])/volumeSequence[0]) << endl;
    out << " ];\n";
    
    out << "figure(1);                               " << endl;
    out << "plot(A(:,1));                            " << endl;
    out << "title('Values of the volumes');          " << endl;
    
    out << "figure(2);                               " << endl;
    out << "plot(A(:,2));                            " << endl;
    out << "title('Values of the relative error');   " << endl;
    
    
    
    out.close();
}

//
// Method to compute the quality 
//
Real HSSmoothing::getMeanMeshQuality()
{
    isotropicQuality2d<Triangle> iso(meshPointer);
    
    Real val = 0.;
    for(UInt i=0; i<meshPointer->getNumElements(); ++i)
        val += iso.triangleQual(i);
    val /= static_cast<Real>(meshPointer->getNumElements());
    
    return(val);
}

void HSSmoothing::saveAllQueueData(queue<Real> & queueOfVolumes, queue<Real>  & queueOfQualities)
{
    volumeSequence.clear();                         qualitySequence.clear();
    volumeSequence.reserve(queueOfVolumes.size());  qualitySequence.reserve(queueOfQualities.size());
    
    while(!queueOfVolumes.empty())
    {
        volumeSequence.push_back(queueOfVolumes.front());
        qualitySequence.push_back(queueOfQualities.front());
        queueOfVolumes.pop();
        queueOfQualities.pop();
    }
}

void HSSmoothing::writeMatlabFileWithQualityAndVolumes(string filename)
{
    cout << "Writing file "  << filename << endl;
    ofstream out(filename.c_str());
    
    out << "iter = [\n";
    for(UInt i=0; i<volumeSequence.size(); ++i)
        out << i << endl;
    out << " ];\n";
                                 
    out << "V = [\n";
    for(UInt i=0; i<volumeSequence.size(); ++i)
        out << volumeSequence[i] << "  " << (fabs(volumeSequence[0] - volumeSequence[i])/volumeSequence[0]) << endl;
    out << " ];\n";
    
    out << "Q = [\n";
    for(UInt i=0; i<qualitySequence.size(); ++i)
        out << qualitySequence[i] << endl;
    out << " ];\n";
    
    out << "figure(1);                                                                " << endl;
    out << "plot(iter, V(:,1), '-r','LineWidth',2');                                  " << endl;
    out << "title('Values of the volumes');                                           " << endl;
    out << "tmpLegend = legend('volume values');                                      " << endl;
    out << "set(tmpLegend,'Interpreter','latex');                                     " << endl;
    out << "set(tmpLegend,'FontSize', 12);                                            " << endl;  
                                                                                      
    out << "figure(2);                                                                " << endl;
    out << "plot(iter, V(:,2), '-r','LineWidth',2');                                  " << endl;
    out << "title('Values of the relative error');                                    " << endl;
    out << "tmpLegend = legend('relative error','Location','southeast');              " << endl;
    out << "set(tmpLegend,'Interpreter','latex');                                     " << endl;
    out << "set(tmpLegend,'FontSize', 12);                                            " << endl;
                                                         
    out << "figure(3);                                                                " << endl;
    out << "plot(iter, Q, '-b','LineWidth',2');                                       " << endl;
    out << "title('Mean quality');                                                    " << endl;
    out << "tmpLegend = legend('$q_{\\textrm{mean}}$','Location','southeast');        " << endl;
    out << "set(tmpLegend,'Interpreter','latex');                                     " << endl;
    out << "set(tmpLegend,'FontSize', 12);                                            " << endl;
    
    out.close();
}

//
// Method to make the swapping 
// 
Real HSSmoothing::swapping() 
{
    isotropicQuality2d<Triangle> iso(meshPointer);
    iso.swapping(5.);

    Real val = 0.;
    for(UInt i=0; i<meshPointer->getNumElements(); ++i)
        val += iso.triangleQual(i);
    val /= static_cast<Real>(meshPointer->getNumElements());

    return(val);
}

//
// internal methods to do some checking 
//
void HSSmoothing::writeSmoothInfo(UInt iter, Real percentage)
{
     cout << "Starting Improved Laplacian Smoothing\n";
     
     if(iter!=0)
        cout << "\t iteration: " << iter << endl;
     else
        cout << "\t     perc.: " << percentage << endl;
     
     cout << "\t     alpha: " << alpha << endl;
     cout << "\t      beta: " << beta   << endl;
}                
