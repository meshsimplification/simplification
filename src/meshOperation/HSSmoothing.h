#ifndef HCSMOOTHING_H_
#define HCSMOOTHING_H_

#include <algorithm>
#include <cassert>	
#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <functional>
#include <numeric>

#include "../core/shapes.hpp"
#include "../core/point.h"
#include "../core/graphItem.h"

#include "../geometry/geoElement.hpp"
#include "../geometry/geoElementSize.hpp"
#include "../geometry/mesh2d.hpp"

#include "../doctor/doctor2d.h"

#include "../meshOperation/isotropicQuality2d.h"

#include "../file/createFile.h"

namespace geometry
{
    
using namespace std;

/*! 
    class to implement the smoothing procedure descried in 
    "Improved Laplacian Smoothing of Noisy Surface Mehes"
    The names are ispired by Algorithm 1 in this article 
*/

class HSSmoothing : public doctor2d<Triangle>
{
      //
      // Constructors 
      //
      public:
	  	/*! Costruttore vuoto */
		  HSSmoothing();
		      
		  /*! Costruttore con definizione dei due puntatori
		    \param _meshPointer puntatore alla mesh
        \param _alpha parameters for the definition of the smoothing  
        \param _beta parameters for the definition of the smoothing */
  		HSSmoothing(mesh2d<Triangle> * _meshPointer, 
                  Real _alpha=0.1, 
                  Real _beta=0.6);
  		
  		/*! Metodo per prendere il puntatore alla mesh*/
  		mesh2d<Triangle> * getMeshPointer()
      {
          return(meshPointer);
      }

      /*! Method to set when apply the swapping 
          \param _whenSwap the input parameter */
      void setWhenSwap(UInt _whenSwap)
      {
         whenSwap = _whenSwap;
      }
        
      /*! Method to set the parameters 
          \param _mu parameters for the definition of the smoothing  
          \param _lambda parameters for the definition of the smoothing 
          \param _weightsToUse kind of weights */
      void setHSSmoothingParameters(Real _alpha, Real _beta)
      {
          alpha = _alpha;
          beta = _beta;
      }
      
      //
      // Basic methods to run the routine 
      //
      public:
      
      /*! Method to run the smoothing 
          \param iter number of iteration */
      void runTheSmoothing(UInt iter);
      
      /*! Method to run the smoothing util we reach a percentage of the volume 
          \param percentage percentage of the initial volume */
      void runTheSmoothingAccordingToTheVolume(Real percentage);

      /*! Method to run the smoothing util we reach a percentage of the volume and we do not decrease the quality
          \param percentage percentage of the initial volume 
          \param useSwap a boolean to allow the use of swapping */
      void runTheSmoothingAccordingToTheVolumeAndQuality(Real percentage, bool useSwap=false);
                
      //
      //  Internal method to exploit for the smoothing 
      //
      private:

      /*! Method to compute the fist moviment, it fills the vector bPoints */
      void computeFistMoveForAllPoints();

      /*! Method to compute the fist moviment, it fills the vector pPoints */
      void computeSecondMoveForAllPoints();
      
      /*! Method to find the first move b_i in the article "Improved Laplacian Smoothing of Noisy Surface Meshes"
          \param nodeId id of the node 
          \param pointToPointConnection connection of the point */
      void findFirstMove(UInt nodeId, const std::vector<UInt> & actualPointConnection);

      /*! Method to find the first move b_i in the article "Improved Laplacian Smoothing of Noisy Surface Meshes"
          \param nodeId id of the node 
          \param pointToPointConnection connection of the point */
      void findSecondMove(UInt nodeId, const std::vector<UInt> & actualPointConnection);
      
      /*! Method to change the positio 
          \param newPositions vector with all the positions */
      void moveAllThePoints(vector<point> & newPositions); 
      
      /*! Method to set the connections */
      void computePointToPointConnection(vector<vector<UInt> > & pointToPointConnection);

      /*! Method to initialize the points list */
      void initializeVectorsWithPoints(); 

      /*! Method to sace the ponts */
      void saveActualPoints(vector<point> & actualPoints) const;
      
      //
      // Method to compute the volume 
      //
      public:
      /*! Method to write the .m file to plot the graph of volumes
          \param filename string with the filename */
      void writeMatlabFileWithVolumes(string filename);
      
      /*! Method to get the vector of the volumes 
          \param _volumeSequence vector that will be filled*/
      void getVolumeSequence(vector<Real> _volumeSequence)
      {
          _volumeSequence = volumeSequence;
      }
      
      private:

      /*! Method to compute the volume of the mesh
          \return the volume */
      Real computeVolume();
      
      //
      // Method to compute the quality 
      //
      public:
      /*! Method to get the mean quality of the mesh 
          \return the mean quality*/
      Real getMeanMeshQuality();
      
      /*! Method to get the vector of quality
          \param _qualitySequence vector that will be filled*/
      void getMeanQualitySequence(vector<Real> _qualitySequence)
      {
          _qualitySequence = qualitySequence;
      }

      /*! Method to transfer the data from the queue to the class vector 
          \param queueOfVolumes the queue of volumes
          \param queueOfQualities the queue of qualities */
      void saveAllQueueData(queue<Real> & queueOfVolumes, queue<Real>  & queueOfQualities);
      
      /*! Method to write the .m file to plot the graph of the quality and volumes 
          \param filename string with the filename */
      void writeMatlabFileWithQualityAndVolumes(string filename);

      //
      // Method to make the swapping 
      //
      private:

      /*! Method to make the flipping 
          \param it restuns the quality */ 
      Real swapping();
                
      //
      // internal methods to do some checking 
      //
      private:
      /*! Method to write the information about the smoothing 
          \param iter number of iterations 
          \param percentage real number to say the percentage when I stop*/
      void writeSmoothInfo(UInt iter, Real percentage);
                        
      //
      // Internal variables 
      //
      private:    
          
      Real alpha,beta;
      UInt whenSwap;
      
      vector<vector<UInt> > pointToPointConnection;
      vector<point> originalPoints;
      vector<point> pPoints;
      vector<point> bPoints;
      
      vector<Real>  volumeSequence;
      vector<Real>  qualitySequence;
  
};











}













#endif
