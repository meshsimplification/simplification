#include "taubinSmoothing.h"

using namespace std;
using namespace geometry;

//
// Costruttore 
//
taubinSmoothing::taubinSmoothing() : doctor2d<Triangle>(), 
                                     mu(-0.331), lambda(-0.330), 
                                     weightsToUse(UNIFORMWEIGHTS),
                                     smoothToUse(TAUBIN),
                                     whenSwap(10)
{
}

taubinSmoothing::taubinSmoothing(mesh2d<Triangle> * _meshPointer, Real _mu, Real _lambda, kindOfWeights _weightsToUse) 
  : doctor2d<Triangle>(_meshPointer), mu(_mu), lambda(_lambda), weightsToUse(_weightsToUse), smoothToUse(TAUBIN), whenSwap(10)
{
    checkParameters();
}

//
// Basic methods to run the routine 
//
void taubinSmoothing::runTheSmoothing(UInt iter)
{
    writeSmoothInfo(iter, 0.0);
    
    vector<vector<UInt> > pointToPointConnection;
    computePointToPointConnection(pointToPointConnection);

    volumeSequence.resize(iter+1);
    volumeSequence[0] = computeVolume();
    
    vector<point> newPositions,oldPositions;
    for(UInt i=0; i<iter; ++i)
    {
        computeAllNewPositionAndSaveOldOne(pointToPointConnection, newPositions, oldPositions);
        moveAllThePoints(newPositions);

        volumeSequence[i+1] = computeVolume();
    }
    cout << "Smoothing done!!                                            \n";
}

void taubinSmoothing::runTheSmoothingAccordingToTheVolume(Real percentage)
{
    writeSmoothInfo(0, percentage);
    
    vector<vector<UInt> > pointToPointConnection;
    computePointToPointConnection(pointToPointConnection);

    Real initialVolume = computeVolume(); 
    queue<Real> queueOfVolumes;
    queueOfVolumes.push(initialVolume);
    
    queue<Real> queueOfQualities;
    Real oldQual = getMeanMeshQuality();
    queueOfQualities.push(oldQual);
    
    vector<point> newPositions,oldPositions;
    bool stopTheIteration = false;
    UInt maxIter = meshPointer->getNumNodes();
    UInt countIter = 0; 
    do
    {
        computeAllNewPositionAndSaveOldOne(pointToPointConnection, newPositions, oldPositions);
        moveAllThePoints(newPositions);
        
        // compute all the geometrical quantities 
        Real actualVolume = computeVolume();
        Real actualQual = getMeanMeshQuality();
        Real actualPercent = fabs(actualVolume-initialVolume)/initialVolume;
       
        // check the geometrical quantities and eventually stop 
        if(actualPercent<percentage)
        {
            queueOfVolumes.push(actualVolume);
            queueOfQualities.push(actualQual);
            oldQual = actualQual;
        }
        else
        {
            moveAllThePoints(oldPositions);    
            stopTheIteration = true;
        }


        if(countIter>maxIter)
        {
            std::cout << "!! !! !! Max number of iteration reached\n";
            stopTheIteration = true;
        }

        std::cout << "Iteration " << countIter << std::endl;
        ++countIter;
        
    }while(!stopTheIteration);
    

    saveAllQueueData(queueOfVolumes, queueOfQualities);
    
    
    cout << "Smoothing done in " << volumeSequence.size() << " iterations                                           \n";
}

void taubinSmoothing::runTheSmoothingAccordingToTheVolumeAndQuality(Real percentage, bool useSwap)
{
    writeSmoothInfo(0, percentage);

    Real initialVolume = computeVolume(); 
    queue<Real> queueOfVolumes;
    queueOfVolumes.push(initialVolume);
    
    queue<Real> queueOfQualities;
    Real oldQual = getMeanMeshQuality();
    queueOfQualities.push(oldQual);
    
    vector<vector<UInt> > pointToPointConnection;
    computePointToPointConnection(pointToPointConnection);
    
    vector<point> newPositions,oldPositions;
    bool stopTheIteration = false;
    UInt maxIter = meshPointer->getNumNodes();
    UInt countIter = 0;
    do
    {
        computeAllNewPositionAndSaveOldOne(pointToPointConnection, newPositions, oldPositions);
        moveAllThePoints(newPositions);
        
        // compute all the geometrical quantities 
        Real actualVolume = computeVolume();
        Real actualQual = getMeanMeshQuality();
        Real actualPercent = fabs(actualVolume-initialVolume)/initialVolume;

        // improve the quality of the mesh via the swapping
        if(useSwap && (countIter!=0) && (countIter%whenSwap==0))
        {
            actualQual = swapping();
            // we have to redo the connections
            computePointToPointConnection(pointToPointConnection);
        }
        
        // check the geometrical quantities and eventually stop 
        if(actualPercent<percentage && oldQual<actualQual)
        {
            queueOfVolumes.push(actualVolume);
            queueOfQualities.push(actualQual);
            oldQual = actualQual;
        }
        else
        {
            moveAllThePoints(oldPositions);    
            stopTheIteration = true;

            if(actualPercent>percentage)
                cout << "Stop for VOLUME constraint                  \n";
            if(oldQual>actualQual)
                cout << "Stop for QUALITY decrease                   \n";
        }

        std::cout << "Iteration " << countIter << std::endl;

        if(countIter>maxIter)
        {
            std::cout << "!! !! !! Max number of iteration reached\n";
            stopTheIteration = true;
        }

        ++countIter;
        
    }while(!stopTheIteration);
    

    saveAllQueueData(queueOfVolumes, queueOfQualities);
    
    
    cout << "Smoothing done in " << volumeSequence.size() << " iterations                                           \n";
}

//
//  Internal method to exploit for the smoothing 
//
point taubinSmoothing::moveOnePoint(UInt nodeId, std::vector<UInt> & pointToPointConnection)
{
    // get the point to move 
    point pointToMove = meshPointer->getNode(nodeId);
    pointToMove.setId(nodeId); 
    
    point resultingPoint = pointToMove;
    if(meshPointer->getNode(nodeId).getBoundary()==0)
    {
        // do the first move 
        point firstMove  = moveTheIthPoint(pointToMove, pointToPointConnection, lambda);
        firstMove.setId(nodeId);
    
        if(smoothToUse==TAUBIN)
            resultingPoint = moveTheIthPoint(firstMove, pointToPointConnection, mu);
        else
            resultingPoint = firstMove;
    }
    
    return(resultingPoint);
}

void taubinSmoothing::computeAllNewPositionAndSaveOldOne(vector<vector<UInt> > & pointToPointConnection, 
                                                         vector<point> & newPositions, 
                                                         vector<point> & oldPositions)
{
    oldPositions.resize(meshPointer->getNumNodes());
    newPositions.resize(meshPointer->getNumNodes());
    for(UInt nodeId = 0; nodeId<meshPointer->getNumNodes(); ++nodeId)
    {
        // get the point to move 
        newPositions[nodeId] = moveOnePoint(nodeId, pointToPointConnection[nodeId]);

        // save the old one 
        oldPositions[nodeId] = meshPointer->getNode(nodeId);
    }
}

void taubinSmoothing::moveAllThePoints(vector<point> & newPositions)
{
    for(UInt nodeId = 0; nodeId<meshPointer->getNumNodes(); ++nodeId)
        changeNode(nodeId, newPositions[nodeId]);
}

void taubinSmoothing::computePointToPointConnection(vector<vector<UInt> > & pointToPointConnection)
{
    pointToPointConnection.clear();
    pointToPointConnection.resize(meshPointer->getNumNodes());
    
    for(UInt i=0; i<pointToPointConnection.size(); ++i)
        createStellata(i, &pointToPointConnection[i]);
}
                
point taubinSmoothing::moveTheIthPoint(point & pointToMove, const vector<UInt> & pointToPointConnection, Real smoothPara)
{
    point result(0.0, 0.0, 0.0);
    switch(weightsToUse)
    {
        case(UNIFORMWEIGHTS):
                result = moveTheIthPointWithUniformWeights(pointToMove, pointToPointConnection, smoothPara);
                break;
        case(FUJIWARAWEIGHTS):
                result = moveTheIthPointWithFuijwaraWeights(pointToMove, pointToPointConnection, smoothPara);
                break;
        case(DESBRUNWEIGHTS):
                result = moveTheIthPointWithDesbrunWeights(pointToMove, pointToPointConnection, smoothPara);
                break;
    }
    return(result);
}

point taubinSmoothing::moveTheIthPointWithUniformWeights(point & pointToMove, 
                                                         const vector<UInt> & pointToPointConnection, Real smoothPara)
{
    //
    // The formula behind this smoothing method come from the paper "Curved and Surface Smoothing wihtout shrinkage"
    // 
    // https://graphics.stanford.edu/courses/cs468-01-fall/Papers/taubin-smoothing.pdf
    //
    
    Real weigth = 1./static_cast<Real>(pointToPointConnection.size());
    point deltaV(0.0, 0.0, 0.0);
    for(UInt i=0; i<pointToPointConnection.size(); ++i)
    {
        point tmpPt = meshPointer->getNode(pointToPointConnection[i]);        
        deltaV.setX(deltaV.getX()+((tmpPt.getX()-pointToMove.getX())*weigth));
        deltaV.setY(deltaV.getY()+((tmpPt.getY()-pointToMove.getY())*weigth));
        deltaV.setZ(deltaV.getZ()+((tmpPt.getZ()-pointToMove.getZ())*weigth));
    }
    
    deltaV.setX(deltaV.getX()*smoothPara);
    deltaV.setY(deltaV.getY()*smoothPara);
    deltaV.setZ(deltaV.getZ()*smoothPara);

    return(pointToMove+deltaV);
}

point taubinSmoothing::moveTheIthPointWithFuijwaraWeights(point & pointToMove, 
                                                          const vector<UInt> & pointToPointConnection, Real smoothPara)
{
    //
    // The formula behind this smoothing method come from the paper "Geometric Signal Processing on Polygonal Meshes"
    // 
    // http://mesh.brown.edu/taubin/pdfs/taubin-eg00star.pdf
    //
    Real totalWeight = 0.;
    std::vector<Real> allLenghts(pointToPointConnection.size());
    for(UInt i=0; i<pointToPointConnection.size(); ++i)
    {
        allLenghts[i] = (pointToMove - meshPointer->getNode(pointToPointConnection[i])).norm2();
        totalWeight += 1./allLenghts[i];
    }
    
    point deltaV(0.0, 0.0, 0.0);
    for(UInt i=0; i<pointToPointConnection.size(); ++i)
    {
        Real weight = (1./allLenghts[i])/totalWeight;
        point tmpPt = meshPointer->getNode(pointToPointConnection[i]);        
        
        deltaV.setX(deltaV.getX()+((tmpPt.getX()-pointToMove.getX())*weight));
        deltaV.setY(deltaV.getY()+((tmpPt.getY()-pointToMove.getY())*weight));
        deltaV.setZ(deltaV.getZ()+((tmpPt.getZ()-pointToMove.getZ())*weight));
    }
    
    deltaV.setX(deltaV.getX()*smoothPara);
    deltaV.setY(deltaV.getY()*smoothPara);
    deltaV.setZ(deltaV.getZ()*smoothPara);

    return(pointToMove+deltaV);
}

point taubinSmoothing::moveTheIthPointWithDesbrunWeights(point & pointToMove, 
                                                         const vector<UInt> & pointToPointConnection, Real smoothPara)
{
    //
    // The formula behind this smoothing method come from the paper "Geometric Signal Processing on Polygonal Meshes"
    // 
    // http://mesh.brown.edu/taubin/pdfs/taubin-eg00star.pdf
    //
    
    // save all the angles 
    std::vector<std::vector<Real> > oppositeAngles(pointToPointConnection.size());
    for(UInt i=0; i<oppositeAngles.size(); ++i)
    {
        std::vector<UInt> elem;
        UInt idPointToMove = pointToMove.getId();
        elementOnEdge(idPointToMove, pointToPointConnection[i], &elem);
        // I do in this way since there can me the boundary points 
        oppositeAngles[i].resize(elem.size());
        for(UInt j=0; j<oppositeAngles[i].size(); ++j)
        {
            UInt idOpposite = lastNode(idPointToMove, pointToPointConnection[i], elem[j]);
            oppositeAngles[i][j] = angolo(idOpposite, elem[j]);
        }
    }
    
    // compute all the weights 
    Real totalWeight = 0.;
    std::vector<Real> allCotAlphaCotBeta(pointToPointConnection.size(), 0.0);
    for(UInt i=0; i<pointToPointConnection.size(); ++i)
    {       
        for(UInt j=0; j<oppositeAngles[i].size(); ++j)
            allCotAlphaCotBeta[i] += 1./tan(oppositeAngles[i][j]);
        totalWeight += allCotAlphaCotBeta[i];
    }
    
    point deltaV(0.0, 0.0, 0.0);
    for(UInt i=0; i<pointToPointConnection.size(); ++i)
    {
        Real weight = allCotAlphaCotBeta[i]/totalWeight;
        point tmpPt = meshPointer->getNode(pointToPointConnection[i]);        
        
        deltaV.setX(deltaV.getX()+((tmpPt.getX()-pointToMove.getX())*weight));
        deltaV.setY(deltaV.getY()+((tmpPt.getY()-pointToMove.getY())*weight));
        deltaV.setZ(deltaV.getZ()+((tmpPt.getZ()-pointToMove.getZ())*weight));
    }
    
    deltaV.setX(deltaV.getX()*smoothPara);
    deltaV.setY(deltaV.getY()*smoothPara);
    deltaV.setZ(deltaV.getZ()*smoothPara);

    return(pointToMove+deltaV);
}

//
// Method to compute the volume 
//
Real taubinSmoothing::computeVolume()
{
    //
    // compute the volume via the divergence theorem
    //
    Real volume = 0.;
    for(UInt i=0; i<meshPointer->getNumElements(); ++i)
    {
        point normal = getTriangleNormal(i);
        Real area = getTriangleArea(i);
        
        std::vector<point> listOfPoints;
        meshPointer->getNodeOfElement(i, &listOfPoints);
        
        point pBar;
        pBar.replace(listOfPoints[0], listOfPoints[1], listOfPoints[2], 1./3.);
    
        volume += area * pBar.getX() * normal.getX();
    }
    
    return(volume);
}
                                 
void taubinSmoothing::writeMatlabFileWithVolumes(string filename)
{
    cout << "Writing file "  << filename << endl;
    ofstream out(filename.c_str());
                                 
    out << "A = [ ";
    for(UInt i=0; i<volumeSequence.size(); ++i)
        out << volumeSequence[i] << "  " << (fabs(volumeSequence[0] - volumeSequence[i])/volumeSequence[0]) << endl;
    out << " ];\n";
    
    out << "figure(1);                               " << endl;
    out << "plot(A(:,1));                            " << endl;
    out << "title('Values of the volumes');          " << endl;
    
    out << "figure(2);                               " << endl;
    out << "plot(A(:,2));                            " << endl;
    out << "title('Values of the relative error');   " << endl;
    
    
    
    out.close();
}

//
// Method to compute the quality 
//
Real taubinSmoothing::getMeanMeshQuality()
{
    isotropicQuality2d<Triangle> iso(meshPointer);
    
    Real val = 0.;
    for(UInt i=0; i<meshPointer->getNumElements(); ++i)
        val += iso.triangleQual(i);
    val /= static_cast<Real>(meshPointer->getNumElements());
    
    return(val);
}

void taubinSmoothing::saveAllQueueData(queue<Real> & queueOfVolumes, queue<Real>  & queueOfQualities)
{
    volumeSequence.clear();                         qualitySequence.clear();
    volumeSequence.reserve(queueOfVolumes.size());  qualitySequence.reserve(queueOfQualities.size());
    
    while(!queueOfVolumes.empty())
    {
        volumeSequence.push_back(queueOfVolumes.front());
        qualitySequence.push_back(queueOfQualities.front());
        queueOfVolumes.pop();
        queueOfQualities.pop();
    }
}

void taubinSmoothing::writeMatlabFileWithQualityAndVolumes(string filename)
{
    cout << "Writing file "  << filename << endl;
    ofstream out(filename.c_str());
    
    out << "iter = [\n";
    for(UInt i=0; i<volumeSequence.size(); ++i)
        out << i << endl;
    out << " ];\n";
                                 
    out << "V = [\n";
    for(UInt i=0; i<volumeSequence.size(); ++i)
        out << volumeSequence[i] << "  " << (fabs(volumeSequence[0] - volumeSequence[i])/volumeSequence[0]) << endl;
    out << " ];\n";
    
    out << "Q = [\n";
    for(UInt i=0; i<qualitySequence.size(); ++i)
        out << qualitySequence[i] << endl;
    out << " ];\n";
    
    out << "figure(1);                                                                " << endl;
    out << "plot(iter, V(:,1), '-r','LineWidth',2');                                  " << endl;
    out << "title('Values of the volumes');                                           " << endl;
    out << "tmpLegend = legend('volume values');                                      " << endl;
    out << "set(tmpLegend,'Interpreter','latex');                                     " << endl;
    out << "set(tmpLegend,'FontSize', 12);                                            " << endl;  
                                                                                      
    out << "figure(2);                                                                " << endl;
    out << "plot(iter, V(:,2), '-r','LineWidth',2');                                  " << endl;
    out << "title('Values of the relative error');                                    " << endl;
    out << "tmpLegend = legend('relative error','Location','southeast');              " << endl;
    out << "set(tmpLegend,'Interpreter','latex');                                     " << endl;
    out << "set(tmpLegend,'FontSize', 12);                                            " << endl;
                                                         
    out << "figure(3);                                                                " << endl;
    out << "plot(iter, Q, '-b','LineWidth',2');                                       " << endl;
    out << "title('Mean quality');                                                    " << endl;
    out << "tmpLegend = legend('$q_{\\textrm{mean}}$','Location','southeast');        " << endl;
    out << "set(tmpLegend,'Interpreter','latex');                                     " << endl;
    out << "set(tmpLegend,'FontSize', 12);                                            " << endl;
    
    out.close();
}

//
// Method to make the swapping 
// 
Real taubinSmoothing::swapping() 
{
    isotropicQuality2d<Triangle> iso(meshPointer);
    iso.swapping(5.);

    Real val = 0.;
    for(UInt i=0; i<meshPointer->getNumElements(); ++i)
        val += iso.triangleQual(i);
    val /= static_cast<Real>(meshPointer->getNumElements());

    return(val);
}

//
// internal methods to do some checking 
//
void taubinSmoothing::writeSmoothInfo(UInt iter, Real percentage)
{
    switch(smoothToUse)
    {
        case(TAUBIN):
                     cout << "Starting Taubin smoothing\n";
                     
                     if(iter!=0)
                        cout << "\t iteration: " << iter << endl;
                     else
                        cout << "\t     perc.: " << percentage << endl;
                     
                     cout << "\t    lambda: " << lambda << endl;
                     cout << "\t        mu: " << mu   << endl;
                     cout << "\t   weights: " << translateWeights() << endl;
                     break;
        case(CLASSICAL):
                     cout << "Starting Classical smoothing\n";
                     
                     if(iter!=0)
                        cout << "\t iteration: " << iter << endl;
                     else
                        cout << "\t     perc.: " << percentage << endl;
                     
                     cout << "\t    lambda: " << lambda << endl;
                     cout << "\t   weights: " << translateWeights() << endl;
                     break;
    }
}

void taubinSmoothing::checkParameters()
{
    if(mu>0.0)
    {
        cout << "!! !! !! The mu parameter is positive" << endl;
        exit(1);
    }
    else if(lambda<0.0)
    {
        cout << "!! !! !! The lambda parameter is negative" << endl;
        exit(1);
    }
    else if(lambda>((-1.)*mu))
    {
        cout << "!! !! !! The parameters lambda and mu do not satisfy lambda < -mu " << endl;
        exit(1);
    }
}

string taubinSmoothing::translateWeights()
{
    string name;
    switch(weightsToUse)
    {
        case(UNIFORMWEIGHTS):
                            name = "Uniform Weights";
                            break;
        case(FUJIWARAWEIGHTS):
                            name = "Fujiwara Weights";
                            break;
        case(DESBRUNWEIGHTS):
                            name = "Desbrum Weights";
                            break;
    }
    return(name);
}
                
