#include <iostream>
#include "meshSimplification.h"

using namespace geometry;
using namespace std;

/* Method to project the points */
void projectAllPointsOnTheSphere(mesh2d<Triangle> * surf);

int main()
{	
    // variabili in uso 
    mesh2d<Triangle>	          surf;
    simplification2d<Triangle>    simp;	
    isotropicQuality2d<Triangle>   iso;
    createFile                    file;
    downloadMesh                  down;

    // dowload di una mesh di superficie
    down.fileFromVTK("../mesh/sfera.vtk", &surf);

    // initial one 
    file.fileForParaview("mesh0.inp", &surf);
    
    // set for the simplification
    simp.setMeshPointer(&surf);
    simp.simplificateGreedy(800);
    file.fileForParaview("mesh1.inp", &surf);
    
    // set for the isotropic quality
    iso.setMeshPointer(&surf);
    iso.standardImprove();
    iso.refineUniform();
    iso.standardImprove();
    file.fileForParaview("mesh2.inp", &surf);
    
    // project on the sphere 
    projectAllPointsOnTheSphere(&surf);
    std::cout << "Sequence of spheres in the files mesh0, mesh1, mesh2 and mesh3.inp" << std::endl;
    file.fileForParaview("mesh3.inp", &surf);
    
}

void projectAllPointsOnTheSphere(mesh2d<Triangle> * surf)
{
    for(UInt i=0; i<surf->getNumNodes(); ++i)
    {
        Real radius = 0.5;
        point actual(surf->getNode(i));
        actual.normalize();
            
        surf->getNodePointer(i)->setX(actual.getX()*radius);
        surf->getNodePointer(i)->setY(actual.getY()*radius);
        surf->getNodePointer(i)->setZ(actual.getZ()*radius);
    }
}




