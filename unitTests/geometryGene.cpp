#include <iostream>
#include "meshSimplification.h"

using namespace geometry;
using namespace std;

enum kindOfMesh{STRUCTUREDMESH, UNSTRUCTUREDMESH};

/* Method to project the points */
void createPerimeter(point pMax, point pMin, mesh1d<Line> * boundary);
void projectOnGeometry(mesh2d<Triangle> * surf);

int main(int argc, char * argv[])
{	
    // variabili in uso 
    point pMin(0.0,0.0,0.0),pMax(1.0,1.0,0.0);
    mesh1d<Line>                     boundary;
    mesh2d<Triangle>	                 surf;
    createFile                           file;
    downloadMesh                         down;
    triangle2dByShewchuk                 gene;
    
    // set the pointers 
    gene.setBelements(&boundary);
    gene.setElements(&surf);        

    if(argc!=3)
    {
        cout << "Main to create the mesh of the square [0,1]^2, wrong number of inputs\n";
        cout << "1) kind of mesh 0 structured, 1 = unstructured\n";
        cout << "2) number of edge division if structured, mesh size if unstructured\n";
        return(1);
        
    }
    
    kindOfMesh meshType = UNSTRUCTUREDMESH;
    if(atoi(argv[1])==0)
        meshType = STRUCTUREDMESH;
    
    if(meshType == STRUCTUREDMESH)
    {
        UInt div = atoi(argv[2]);
        gene.createMesh2d(pMax, pMin, div, div);
    }
    else if(meshType == UNSTRUCTUREDMESH)
    {
        REAL diameter = atof(argv[2]);
        createPerimeter(pMax, pMin, &boundary);
        gene.createMesh2d(diameter);
        
        // set for the isotropic quality
        isotropicQuality2d<Triangle> iso;
        iso.setMeshPointer(&surf);
        projectOnGeometry(&surf); 
        iso.standardImprove();
        projectOnGeometry(&surf); 

    }
    
    ostringstream outName;
    outName << "paraboloid_" << surf.getNumElements() << ".inp";
    
    projectOnGeometry(&surf);    
    file.fileForParaview(outName.str(), &surf);    
}

//
// Implementation of the function 
//
void projectOnGeometry(mesh2d<Triangle> * surf)
{
    for(UInt i=0; i<surf->getNumNodes(); ++i)
    {   
        Real x = surf->getNode(i).getX();
        Real y = surf->getNode(i).getY();        
        
        Real z = x*x+y*y;        
        
        surf->getNodePointer(i)->setZ(z);
    }
}

void createPerimeter(point pMax, point pMin, mesh1d<Line> * boundary)
{
    boundary->getNodePointer()->clear();
    boundary->getNodePointer()->resize(4);
    
    boundary->getNodePointer(0)->setX(pMin.getX());    boundary->getNodePointer(0)->setY(pMin.getY());
    boundary->getNodePointer(1)->setX(pMax.getX());    boundary->getNodePointer(1)->setY(pMin.getY());
    boundary->getNodePointer(2)->setX(pMax.getX());    boundary->getNodePointer(2)->setY(pMax.getY());
    boundary->getNodePointer(3)->setX(pMin.getX());    boundary->getNodePointer(3)->setY(pMax.getY());
    
    boundary->getElementPointer()->resize(4);
    boundary->getElementPointer(0)->setConnectedId(0,0);    boundary->getElementPointer(0)->setConnectedId(1,1);
    boundary->getElementPointer(1)->setConnectedId(0,1);    boundary->getElementPointer(1)->setConnectedId(1,2);
    boundary->getElementPointer(2)->setConnectedId(0,2);    boundary->getElementPointer(2)->setConnectedId(1,3);
    boundary->getElementPointer(3)->setConnectedId(0,3);    boundary->getElementPointer(3)->setConnectedId(1,0);
    
}





