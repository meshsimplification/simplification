#include <iostream>
#include "meshSimplification.h"

using namespace geometry;
using namespace std;

/* Method to project the points */
void makeACylinder(Real radius, Real heigth, UInt numBase, UInt numHeight, mesh2d<Triangle> * surf);

int main()
{	
    // variabili in uso 
    mesh2d<Triangle>	          surf;
    createFile                    file;

    // input values 
    Real radius = 0.5;
    Real heigth = 1.0;
    UInt numBase = 80;
    UInt numHeight = 20;
    
    // make the mesh 
    makeACylinder(radius, heigth, numBase, numHeight, &surf);
    file.fileForParaview("mesh0.inp", &surf);
    
}

// --------------------------------------------------------------------------
//                   Implementation of the function 
// --------------------------------------------------------------------------
void makeThePoints(Real radius, Real heigth, UInt numBase, UInt numHeight, mesh2d<Triangle> * surf)
{
    std::vector<Real> x(numBase,0.0),y(numBase,0.0);
    for(UInt i=0; i<numBase; ++i)
    {
        Real ang = (static_cast<Real>(i)/static_cast<Real>(numBase))*2.*M_PI;
        x[i] = radius * sin(ang);
        y[i] = radius * cos(ang);
    }
        
    std::vector<Real>  z(numHeight,0.0);
    for(UInt i=0; i<numHeight; ++i)
        z[i] = (static_cast<Real>(i)/static_cast<Real>(numHeight-1));
    
    surf->getNodePointer()->resize(numBase * numHeight + 2);
    UInt cont = 0;
    for(UInt h=0; h<numHeight; ++h)
    {
        for(UInt circle=0; circle<numBase; ++circle)
        {
            surf->getNodePointer(cont)->setX(x[circle]);
            surf->getNodePointer(cont)->setY(y[circle]);
            surf->getNodePointer(cont)->setZ(z[h]);
            ++cont;
        }
    }
    
    // the last two points are at the centers of the basis circles 
    UInt lastBy1Id = numBase * numHeight;
    UInt lastId = numBase * numHeight + 1;
    
    surf->getNodePointer(lastBy1Id)->setX(0.0);
    surf->getNodePointer(lastBy1Id)->setY(0.0);
    surf->getNodePointer(lastBy1Id)->setZ(0.0);
    
    surf->getNodePointer(lastId)->setX(0.0);
    surf->getNodePointer(lastId)->setY(0.0);
    surf->getNodePointer(lastId)->setZ(heigth);
    
}

void makeTheConnections(UInt numBase, UInt numHeight, mesh2d<Triangle> * surf)
{
    surf->getElementPointer()->reserve((numBase*(numHeight-1))*2 + 2 * numBase);
    for(UInt h=0; h<numHeight-1; ++h)
    {
        for(UInt circle=0; circle<numBase-1; ++circle)
        {
            UInt index1 = (circle  ) + (h  ) * numBase;
            UInt index2 = (circle+1) + (h  ) * numBase;
            UInt index3 = (circle+1) + (h+1) * numBase;
            UInt index4 = (circle  ) + (h+1) * numBase;
            
            geoElement<Triangle> tria;
            tria.setConnectedId(0, index1); tria.setConnectedId(1, index2); tria.setConnectedId(2, index3);
            surf->getElementPointer()->push_back(tria);
            tria.setConnectedId(0, index3); tria.setConnectedId(1, index4); tria.setConnectedId(2, index1);
            surf->getElementPointer()->push_back(tria);
        }
        
        UInt index1 = (numBase-1) + (h  ) * numBase;
        UInt index2 = 0           + (h  ) * numBase;
        UInt index3 = 0           + (h+1) * numBase;
        UInt index4 = (numBase-1) + (h+1) * numBase;
        
        geoElement<Triangle> tria;
        tria.setConnectedId(0, index1); tria.setConnectedId(1, index2); tria.setConnectedId(2, index3);
        surf->getElementPointer()->push_back(tria);
        tria.setConnectedId(0, index3); tria.setConnectedId(1, index4); tria.setConnectedId(2, index1);
        surf->getElementPointer()->push_back(tria);
    }
    
    // add the top and bottom basis 
    UInt bottomCenter = surf->getNumNodes()-2;
    UInt topCenter = surf->getNumNodes()-1;
    
    // bottom one 
    for(UInt circle=0; circle<numBase; ++circle)
    {
       UInt index1 = circle;
       UInt index2 = (circle+1) % numBase;
        
       geoElement<Triangle> tria;
       tria.setConnectedId(0, bottomCenter); tria.setConnectedId(1, index2); tria.setConnectedId(2, index1);
       surf->getElementPointer()->push_back(tria); 
    }
    
    // top one 
    UInt stepForTop = (numHeight-1 )* numBase;
    for(UInt circle=0; circle<numBase; ++circle)
    {
       UInt index1 = circle + stepForTop;
       UInt index2 = ((circle+1) % numBase) + stepForTop;
        
       geoElement<Triangle> tria;
       tria.setConnectedId(0, topCenter); tria.setConnectedId(1, index1); tria.setConnectedId(2, index2);
       surf->getElementPointer()->push_back(tria); 
    }
    
}

void makeACylinder(Real radius, Real heigth, UInt numBase, UInt numHeight, mesh2d<Triangle> * surf)
{
    makeThePoints(radius, heigth, numBase, numHeight, surf);
    makeTheConnections(numBase, numHeight, surf);
}