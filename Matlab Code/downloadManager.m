if(strcmp(fileextension,'inp'))
    % read .inp file
    Ared = fopen(filename,'r');
    str=fgets(Ared);
    a = sscanf(str,'%d %d %*d %*d %*d');
    npkt_red = a(1);
    npolygons_red = a(2);
    % read vertices
    [B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
    %B_red = B_red(1:(end-1));
    B_red = reshape(B_red, 3, cnt_red/3);
    C = B_red;
    % read polygons
    [B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
    B_red = reshape(B_red, 3, cnt_red/3);
    L = B_red';
elseif(strcmp(fileextension,'vtk'))
    % read .vtk file
    Al = fopen(filename,'r');
    str = fgets(Al);
    str = fgets(Al);
    str = fgets(Al);
    str = fgets(Al);
    str = fgets(Al);
    str = fgets(Al);
    npkt = sscanf(str,'%*s %d %*s', 1);
    % read vertices
    [B,cnt] = fscanf(Al,'%f %f %f', 3*npkt);
    B = reshape(B, 3, cnt/3);
    C = B;
    % read polygons
    str = fgets(Al);
    str = fgets(Al);
    str = fgets(Al);
    npolygons = sscanf(str,'%*s %d %*s', 1);
    [B,cnt] = fscanf(Al,'%d %d %d %d\n', 4*npolygons);
    B = reshape(B, 4, cnt/4);
    L = B(2:4,:)+1;
    L = L';
end
