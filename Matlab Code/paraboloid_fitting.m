clear all;  close all

filename = '../mesh/goodSphere.inp';
fileextension = 'inp';
downloadManager;

disp('start with the left')

% poolobj = parpool(2);

Nleft = size(C,2);
Mleft = size(L,1);

Gauss_curv = zeros(Nleft,1);
mean_curv = zeros(Nleft,1);

parfor p = 1:Nleft
    if mod(p,1000)==0
        disp('schritt nummer: '); p
    end
    v = C(:,p); % considered point
    a = find(L(:,1)==p);
    b = find(L(:,2)==p);
    c = find(L(:,3)==p);
    list_tria = L([a' b' c'],:);
    neighbor_points = setdiff(unique(list_tria),p);
    % neighbors = (v_1,...,v_n), columns give the coordinates of each neigbor
    neighbors = C(:,neighbor_points);
    n = length(neighbor_points);
    
    % compute the unit normal vector for every triangle i
    N_v_i = zeros(3,n);
    for i = 1:n
        tri = list_tria(i,:);
        kreuzprod = cross(C(:,tri(1))-C(:,tri(2)),C(:,tri(1))-C(:,tri(3)));
        N_v_i(:,i) = kreuzprod./norm(kreuzprod);
    end
    
    % the average of the normals N_v
    N_v_av = (1/n).*sum(N_v_i,2);
    N_v = N_v_av./norm(N_v_av);
    
    R_y = [N_v(3) 0 -sqrt(1-N_v(3)^2); 0 1 0; sqrt(1-N_v(3)^2) 0 N_v(3)];
    R_z = [N_v(1)/sqrt(N_v(1)^2+N_v(2)^2) N_v(2)/sqrt(N_v(1)^2+N_v(2)^2) 0; ...
        -N_v(2)/sqrt(N_v(1)^2+N_v(2)^2) N_v(1)/sqrt(N_v(1)^2+N_v(2)^2) 0; 0 0 1];
    
    R = R_y*R_z;
    
    X_hat = [R*(neighbors-repmat(v,1,n)) [0 0 0]'];
    
    % fit the rotated rpincipal quadratic to the mapped data
    fun = @(x) sum((X_hat(3,:) - (x(1).*X_hat(1,:).^2+x(2).*X_hat(1,:).*X_hat(2,:)+x(3).*X_hat(2,:).^2)).^2);
    %starting guess
    pguess = [0.1 0.1 0.1];
    %optimise
    [params,fminres] = fminsearch(fun,pguess);
    
    Gauss_curv(p) = 4*params(1)*params(3)-params(2)^2;
    mean_curv(p) = params(1)+params(3);
end
% delete(poolonj)

save paraboloid_fitting_left.mat Gauss_curv mean_curv C L

% left
vtkF = fopen(['Gauss_curvature_pol_fit_left.vtk'],'w+');
fprintf(vtkF, '# vtk DataFile Version 3.0\n');
fprintf(vtkF, 'Gauss Curvature on the Brain\n');
fprintf(vtkF, 'ASCII\n\n');
fprintf(vtkF, 'DATASET POLYDATA\n');

fprintf(vtkF, 'POINTS %i float\n', Nleft);
for k=1:Nleft
    fprintf(vtkF, '%0.6f %0.6f %0.6f\n', C(1,k),C(2,k),C(3,k));
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POLYGONS %i %i\n', Mleft, 4*Mleft);
for k=1:Mleft
    fprintf(vtkF, '%i %i %i %i\n', 3, L(k,1)-1,L(k,2)-1,L(k,3)-1);
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POINT_DATA %i\n', Nleft);
fprintf(vtkF, 'SCALARS Gauss_curvature float 1\n');
fprintf(vtkF, 'LOOKUP_TABLE default\n');
for k=1:Nleft
    fprintf(vtkF, '%0.6f \n', Gauss_curv(k));
end
fclose(vtkF);

vtkF = fopen(['Mean_curvature_pol_fit_left.vtk'],'w+');
fprintf(vtkF, '# vtk DataFile Version 3.0\n');
fprintf(vtkF, 'Gauss Curvature on the Brain\n');
fprintf(vtkF, 'ASCII\n\n');
fprintf(vtkF, 'DATASET POLYDATA\n');

fprintf(vtkF, 'POINTS %i float\n', Nleft);
for k=1:Nleft
    fprintf(vtkF, '%0.6f %0.6f %0.6f\n', C(1,k),C(2,k),C(3,k));
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POLYGONS %i %i\n', Mleft, 4*Mleft);
for k=1:Mleft
    fprintf(vtkF, '%i %i %i %i\n', 3, L(k,1)-1,L(k,2)-1,L(k,3)-1);
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POINT_DATA %i\n', Nleft);
fprintf(vtkF, 'SCALARS mean_curvature float 1\n');
fprintf(vtkF, 'LOOKUP_TABLE default\n');
for k=1:Nleft
    fprintf(vtkF, '%0.6f \n', mean_curv(k));
end
fclose(vtkF);



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % right
% clear all;  close all
% 
% load DTI_FA_MD_mean_right_hemisphere.mat
% disp('start with the right')
% 
% % poolobj = parpool(20);
% 
% Nleft = size(C,2);
% Mleft=size(L,1);
% 
% Gauss_curv = zeros(Nleft,1);
% mean_curv = zeros(Nleft,1);
% 
% parfor p = 1:Nleft
%     if mod(p,1000)==0
%         disp('schritt nummer: '); p
%     end
%     
%     v = C(:,p); % considered point
%     a = find(L(:,1)==p);
%     b = find(L(:,2)==p);
%     c = find(L(:,3)==p);
%     list_tria = L([a' b' c'],:);
%     neighbor_points = setdiff(unique(list_tria),p);
%     % neighbors = (v_1,...,v_n), columns give the coordinates of each neigbor
%     neighbors = C(:,neighbor_points);
%     n = length(neighbor_points);
%     
%     % compute the unit normal vector for every triangle i
%     N_v_i = zeros(3,n);
%     for i = 1:n
%         tri = list_tria(i,:);
%         kreuzprod = cross(C(:,tri(1))-C(:,tri(2)),C(:,tri(1))-C(:,tri(3)));
%         N_v_i(:,i) = kreuzprod./norm(kreuzprod);
%     end
%     
%     % the average of the normals N_v
%     N_v_av = (1/n).*sum(N_v_i,2);
%     N_v = N_v_av./norm(N_v_av);
%     
%     R_y = [N_v(3) 0 -sqrt(1-N_v(3)^2); 0 1 0; sqrt(1-N_v(3)^2) 0 N_v(3)];
%     R_z = [N_v(1)/sqrt(N_v(1)^2+N_v(2)^2) N_v(2)/sqrt(N_v(1)^2+N_v(2)^2) 0; ...
%         -N_v(2)/sqrt(N_v(1)^2+N_v(2)^2) N_v(1)/sqrt(N_v(1)^2+N_v(2)^2) 0; 0 0 1];
%     
%     R = R_y*R_z;
%     
%     X_hat = [R*(neighbors-repmat(v,1,n)) [0 0 0]'];
%     
%     % fit the rotated rpincipal quadratic to the mapped data
%     fun = @(x) sum((X_hat(3,:) - (x(1).*X_hat(1,:).^2+x(2).*X_hat(1,:).*X_hat(2,:)+x(3).*X_hat(2,:).^2)).^2);
%     %starting guess
%     pguess = [0.1 0.1 0.1];
%     %optimise
%     [params,fminres] = fminsearch(fun,pguess);
%     
%     Gauss_curv(p) = 4*params(1)*params(3)-params(2)^2;
%     mean_curv(p) = params(1)+params(3);
% end
% % delete(poolonj)
% 
% save paraboloid_fitting_right.mat Gauss_curv mean_curv C L
% 
% disp('start saving')
% 
% vtkF = fopen(['Gauss_curvature_pol_fit_right.vtk'],'w+');
% fprintf(vtkF, '# vtk DataFile Version 3.0\n');
% fprintf(vtkF, 'Gauss Curvature on the Brain\n');
% fprintf(vtkF, 'ASCII\n\n');
% fprintf(vtkF, 'DATASET POLYDATA\n');
% 
% fprintf(vtkF, 'POINTS %i float\n', Nleft);
% for k=1:Nleft
%     fprintf(vtkF, '%0.6f %0.6f %0.6f\n', C(1,k),C(2,k),C(3,k));
% end
% fprintf(vtkF, '\n');
% fprintf(vtkF, 'POLYGONS %i %i\n', Mleft, 4*Mleft);
% for k=1:Mleft
%     fprintf(vtkF, '%i %i %i %i\n', 3, L(k,1)-1,L(k,2)-1,L(k,3)-1);
% end
% fprintf(vtkF, '\n');
% fprintf(vtkF, 'POINT_DATA %i\n', Nleft);
% fprintf(vtkF, 'SCALARS Gauss_curvature float 1\n');
% fprintf(vtkF, 'LOOKUP_TABLE default\n');
% for k=1:Nleft
%     fprintf(vtkF, '%0.6f \n', Gauss_curv(k));
% end
% fclose(vtkF);
% 
% vtkF = fopen(['Mean_curvature_pol_fit_right.vtk'],'w+');
% fprintf(vtkF, '# vtk DataFile Version 3.0\n');
% fprintf(vtkF, 'Gauss Curvature on the Brain\n');
% fprintf(vtkF, 'ASCII\n\n');
% fprintf(vtkF, 'DATASET POLYDATA\n');
% 
% fprintf(vtkF, 'POINTS %i float\n', Nleft);
% for k=1:Nleft
%     fprintf(vtkF, '%0.6f %0.6f %0.6f\n', C(1,k),C(2,k),C(3,k));
% end
% fprintf(vtkF, '\n');
% fprintf(vtkF, 'POLYGONS %i %i\n', Mleft, 4*Mleft);
% for k=1:Mleft
%     fprintf(vtkF, '%i %i %i %i\n', 3, L(k,1)-1,L(k,2)-1,L(k,3)-1);
% end
% fprintf(vtkF, '\n');
% fprintf(vtkF, 'POINT_DATA %i\n', Nleft);
% fprintf(vtkF, 'SCALARS mean_curvature float 1\n');
% fprintf(vtkF, 'LOOKUP_TABLE default\n');
% for k=1:Nleft
%     fprintf(vtkF, '%0.6f \n', mean_curv(k));
% end
% fclose(vtkF);
