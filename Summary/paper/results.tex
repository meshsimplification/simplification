\section{Numerical Results}\label{sec:results}

The smoothing technique introduced in the previous section can immensely reduce the noise on the geometry data and improve the shape of triangles.
Consequently, numerical applications such as the discrete curvature computation become more robust and reliable for this smoothed mesh. We validate the different smoothing techniques on the cortical geometries described in Section \ref{sec:data}.  

To choose reasonable parameter values for the different smoothing techniques, we evaluate the mesh quality, the exact volume loss and the number of iterations for the two smoothing techniques applied until a maximum volume loss is reached.

\subsection{Parameters for Taubin smoothing}
In literature, the parameters $\lambda$ and $\mu$ of the Taubin smoothing are chosen such that their ratio is 1.05~\cite{Taubin}. In Figure \ref{fig:Taubin_paramspace} we visualise the mesh quality, the volume loss and the number of iterations for different parameter choices of $\lambda$, setting $\mu = -1.05 \lambda$ and $\tolV=0.05$. Smoothing with the parameters $\lambda=0.25$, $\lambda=0.45$ or $\lambda=0.6$ results in the highest mesh quality.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.49\textwidth]{Figures/taubin_smoothing_paramspace_quality2.png} &
\includegraphics[width=0.49\textwidth]{Figures/taubin_smoothing_paramspace_volumeloss2.png}\\
(a) mesh quality $q_{\text{mean}}$ & (b) volume loss\\
\end{tabular}
\end{center}
\caption{The mesh quality (a) and volume loss (b) and iteration number for Taubin smoothing applied to the left hemisphere for different parameter values $\lambda$.}
\label{fig:Taubin_paramspace}
\end{figure}

Figure \ref{fig:Taubin_paramspace2} shows the lateral view of the cortical geometry before and after Taubin smoothing with different parameter values $\lambda$. A close-up in particular regions in the proximity of the medial wall shows the impact of the smoothing on stretched triangles and other extreme formations of the mesh. All parameters reduce the triangles' stretch and smoothen out extreme geometric formations. As high numbers of iterations of the smoothing algorithm imply high computational effort, we will use $\lambda = 0.6$ from now on for the Taubin smoothing. This way the mesh quality is maximised while keeping the computational effort at a minimum.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{ccc}
\includegraphics[width=0.3\textwidth]{Figures/brain_orig.png} & 
\includegraphics[width=0.3\textwidth]{Figures/brain2_orig.png} &
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_orig.png} \\
& (a) original geometry & \\ \\
\includegraphics[width=0.3\textwidth]{Figures/brain_taubinUPl025.png} & 
\includegraphics[width=0.3\textwidth]{Figures/brain2_taubinl025.png} &
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_taubinl025.png} \\
& (b) $\lambda = 0.25$ & \\ \\
\includegraphics[width=0.3\textwidth]{Figures/brain_taubinUPl045.png} &
\includegraphics[width=0.3\textwidth]{Figures/brain2_taubinl045.png} &
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_taubinl045.png} \\
& (c) $\lambda = 0.45$ & \\ \\
\includegraphics[width=0.3\textwidth]{Figures/brain_taubinUPl06.png} & 
\includegraphics[width=0.3\textwidth]{Figures/brain2_taubinl06.png} & 
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_taubinl06.png}\\ 
 & (d) $\lambda = 0.6$ &\\
\end{tabular}
\end{center}
\caption{Smoothing results with Taubin smoothing and different parameter values in the lateral view (left) and for particular regions of the left hemisphere.}
\label{fig:Taubin_paramspace2}
\end{figure}

\subsection{Parameters for Laplacian+HC smoothing}
Considering different parameter values $\alpha$ and $\beta$ in the Laplacian+HC smoothing, we compute the mesh quality and the volume loss for each resulting smooth mesh. These values together with the number of iterations are shown in Figure \ref{fig:Laplace_paramspace}.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.49\textwidth]{Figures/laplacian_smoothing_paramspace_quality.png} &
\includegraphics[width=0.49\textwidth]{Figures/laplacian_smoothing_paramspace_volumeloss.png}\\
(a) mesh quality $q_{\text{mean}}$ & (b) volume loss\\
\end{tabular}
\end{center}
\caption{The mesh quality (a) and volume loss (b) for smoothing with Laplacian+HC applied to the left hemisphere for different parameter values $\alpha$ and $\beta$. The black contour lines with numbers indicate the number of iteration steps.}
\label{fig:Laplace_paramspace}
\end{figure}

The graph shows that for small values for $\alpha\in[0, 0.1]$ and for $\beta\in[0, 0.35]$ the mesh quality is the highest. However, very small parameter values result in irregular mesh, which is visualised in Figure \ref{fig:Laplace_paramspace2}. Thus, we opt for a Laplacian+HC smoothing with $\alpha = 0.05$ and $\beta = 0.35$.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{ccc}
\includegraphics[width=0.3\textwidth]{Figures/brain_orig.png} & 
\includegraphics[width=0.3\textwidth]{Figures/brain2_orig.png} &
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_orig.png} \\
& (a) original geometry & \\ \\
\includegraphics[width=0.3\textwidth]{Figures/brain_laplacianUPa005b005.png} & 
\includegraphics[width=0.3\textwidth]{Figures/brain2_laplacianUPa005b005.png} &
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_laplacianUPa005b005.png} \\
& (b) $\alpha = 0.05, \beta = 0.05$ & \\ \\
\includegraphics[width=0.3\textwidth]{Figures/brain_laplacianUPa01b005.png} &
\includegraphics[width=0.3\textwidth]{Figures/brain2_laplacianUPa01b005.png} &
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_laplacianUPa01b005.png} \\
& (c) $\alpha = 0.1, \beta = 0.05$ & \\ \\
\includegraphics[width=0.3\textwidth]{Figures/brain_laplacianUPa005b035.png} & 
\includegraphics[width=0.3\textwidth]{Figures/brain2_laplacianUPa005b035.png} & 
\includegraphics[width=0.3\textwidth,trim={0 3.39cm 4.5cm 0},clip]{Figures/brain3_laplacianUPa005b035.png}\\ 
 & (d) $\alpha = 0.05, \beta = 0.35$ &\\
\end{tabular}
\end{center}
\caption{Smoothing results of the left hemisphere with Laplacian+HC smoothing and different parameter values $\alpha$ and $\beta$.}
\label{fig:Laplace_paramspace2}
\end{figure}

\subsection{Comparing the smoothing techniques}

With these choices for the smoothing parameters, we compare the two schemes and additionally apply edge flipping after every 10 iterations. The mesh quality and the volume loss are visualised in Figure \ref{fig:comparison} at every iteration step. 

Since the Laplacian+HC smoothing only executes 4 iteration steps before reaching the maximum volume loss, no edge flipping is applied. The edge flipping in the Taubin smoothing increases the mesh quality significantly. In all cases the mesh quality is constantly increasing with every iteration step so that the second stopping criterion for the mesh quality is not applied.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.49\textwidth]{Figures/comparison_LHC_Taubin_quality2.png} &
\includegraphics[width=0.49\textwidth]{Figures/comparison_LHC_Taubin_volumeloss2.png}\\
(a) mesh quality $q_{\text{mean}}$ & (b) volume loss\\
\end{tabular}
\end{center}
\caption{The mesh quality (a) and volume loss (b) at every iteration step for the Laplacian+HC and Taubin smoothing scheme with and without edge-flipping applied to the left hemisphere for $\lambda=0.6$ and $\alpha=0.05, \beta=0.35$.}
\label{fig:comparison}
\end{figure}

In conclusion, for the cortical geometry, the Taubin smoothing with edge-flipping and $\lambda=0.6$ gives the best smoothing results. this technique results in the highest mesh quality (see Figure~\ref{fig:comparison}) and visually removes extreme triangular formations (see Figure~\ref{fig:Taubin_paramspace2}). In Section~\ref{sec:curvature} we use these results to estimate the cortical curvature on the smoothed mesh geometry.

% Applying the curvature approximation to the original and the smoothed brain geometry shows that the noise was removed 
% and the curvature computation is not effected by the noise of the surface mesh.
% While the mean curvature of the left hemisphere varies between 0 and 85.4 in the original geometry, it ranges from 0 to 49.98 in the smoothed brain cortex. The Gaussian curvatures emits values between -107.6 and 2787.6 for the original surface, between -50.9 and 2774.4 for the smoothed one. Even though there are still some extreme configuration inducing high curvature values, overall the values of the smoothed surface are more reasonable/realistic. 

% Sharp configurations are the result of imaging and processing errors as  we are talking about soft tissue in a \corr{skull} that is not supposed to form any steep formations. In Figure~\ref{fig:curv_brain} we show the mean curvature on the left hemisphere of the original and smoothed geometry in the lateral and medial view.

% \todo[inline]{I do not understand this sentence and 
% I do not know if Figure~\ref{fig:curv_brain} is meaningful since it is not so clear the difference between the images.
% Imagine if they are printed in black and white... May be it is better to show a detail... }

% \begin{figure}[!htb]
% \begin{center}
% \includegraphics[width=0.9\textwidth]{Figures/left_initial_mean_curvature.png}\\ 
% original mesh\\
% \includegraphics[width=0.9\textwidth]{Figures/left_smoothed_mean_curvature.png} \\
% smoothed mesh\\
% \end{center}
% \caption{Brain geometry example: the mean curvature on the original and smoothed geometry in the lateral and medial view.}
% \label{fig:curv_brain}
% \end{figure}

%\subsection{Coronary artery geometries}
%
%\todo[inline]{I anticipate at the begining of the section the discussion about noise in IMR, 
%then we can also say something in the introduction.}
%
%We apply the proposed smoothing algorithm to a set of three coronary artery geometries obtained from IVUS data. 
%We fix $\lambdaPlus=0.33$, $\lambdaNeg=-0.34$ and $\tolV=0.05$.
%
%The artery geometries are obtained by a more accurate acquisition technique so 
%the resulting initial mesh is less affected by noise than in the brain case.
%Consequently, we can afford a lower volume tolerance in the smoothing procedure.	
%
%In Table~\ref{tab:qualArt} we collect the quality index for the different choices of weights.
%These numerical results are in accordance with the ones obtained on the brain geometry.
%Indeed, the equal weight smoothing is the procedure that provides the better results in terms of $q_{\text{mean}}$.
%
%Moreover, even the noise on this surface mesh is reduced. 
%In Figure~\ref{fig:coronaryNoise} we show a close-up of such an artery geometry in which the de-noising effect of the equal weights can be appreciate without reducing the triangle quality. 
%Indeed, the final meshes obtained by both the Fujiwara and Desbrun weights reduce the noise, but they create very stretched triangles, see the highlighted triangles in Figure~\ref{fig:coronaryNoise}. Consequently, smoothing with the uniform weights results in the more regular and higher quality mesh. 
%
%\begin{table}[!htb]
%\begin{center}
%% \begin{tabular}{|c|cccc|}
%% \hline
%% quality index       &initial     &equal weights  &Fujiwara weights &Desbrun weights \\
%% \hline
%% $q_{\min}$          &0.0597      &0.1322         &0.0019           &0.0183  \\
%% $q_{\max}$          &0.9996      &0.999         &0.9998           &0.9996  \\
%% $q_{\text{mean}}$   &0.6662      &0.7070         &0.6737           &0.6563  \\
%% \hline
%% \end{tabular}
%\begin{tabular}{|c|c|ccc|}
%\cline{2-5}
%\multicolumn{1}{c|}{}         &\multicolumn{1}{c|}{initial}     &equal weights  &Fujiwara weights &Desbrun weights \\
%\hline
%artery 1 &0.0597      &0.1322         &0.0019           &0.0183  \\
%artery 2 &0.9996      &0.999          &0.9998           &0.9996  \\
%artery 3 &0.6662      &0.7070         &0.6737           &0.6563  \\
%\hline
%\end{tabular}
%\end{center}
%\caption{Coronary artery geometry example: quality index $q_{\text{mean}}$ of the surface meshes.}
%\label{tab:qualArt}
%\end{table}

%\begin{table}[!htb]
%\begin{center}
%\begin{tabular}{|c|c|c|c|c|}
%\cline{2-5}
%\multicolumn{1}{c}{\phantom{c}}&\multicolumn{4}{|c|}{$q_{\text{min}}$}\\
%\hline
%mesh    &initial &equal weights &Fujiwara weights &Desbrun weights \\
%\hline
%clinic1 &0.0597 &0.1322 &0.0019 &0.0183 \\
%clinic2 &0.1069 &0.0319 &0.0851 &0.1004 \\
%clinic3 &0.1577 &0.0975 &0.0027 &0.1536 \\
%clinic4 &0.0984 &0.0689 &0.0562 &0.0968 \\
%clinic5 &0.1245 &0.0823 &0.0115 &0.1188 \\
%clinic6 &0.1054 &0.1136 &0.0003 &0.1038 \\
%clinic7 &0.0550 &0.0879 &0.0035 &0.0504 \\
%\hline
%\end{tabular}
%\end{center}
%\caption{Artery geometries example: quality index $q_{\text{min}}$ for each artery geometry.}
%\label{tab:qualArtMin}
%\end{table}
%
%\begin{table}[!htb]
%\begin{center}
%\begin{tabular}{|c|c|c|c|c|}
%\cline{2-5}
%\multicolumn{1}{c}{\phantom{c}}&\multicolumn{4}{|c|}{$q_{\text{max}}$}\\
%\hline
%mesh    &initial &equal weights &Fujiwara weights &Desbrun weights \\
%\hline
%clinic1 &0.9996 &0.9999 &0.9998 &0.9996 \\
%clinic2 &0.9996 &0.9940 &0.9993 &0.9996 \\
%clinic3 &0.9999 &0.9999 &0.9999 &0.9997 \\
%clinic4 &0.9999 &0.9999 &0.9999 &0.9999 \\
%clinic5 &0.9999 &0.9999 &0.9999 &0.9998 \\
%clinic6 &0.9999 &0.9999 &0.9999 &0.9999 \\
%clinic7 &0.9997 &0.9999 &0.9999 &0.9997 \\
%\hline
%\end{tabular}
%\end{center}
%\caption{Artery geometries example: quality index $q_{\text{max}}$ for each artery geometry.}
%\label{tab:qualArtMax}
%\end{table}
%
%\begin{table}[!htb]
%\begin{center}
%\begin{tabular}{|c|c|c|c|c|}
%\cline{2-5}
%\multicolumn{1}{c}{\phantom{c}}&\multicolumn{4}{|c|}{$q_{\text{mean}}$}\\
%\hline
%mesh    &initial &equal weights &Fujiwara weights &Desbrun weights \\
%\hline
%clinic1 &0.6662 &0.7070 &0.6737 &0.6563 \\ 
%clinic2 &0.5251 &0.5258 &0.5175 &0.5159 \\ 
%clinic3 &0.7545 &0.8159 &0.7852 &0.7492 \\ 
%clinic4 &0.7468 &0.8032 &0.7711 &0.7404 \\ 
%clinic5 &0.7536 &0.8089 &0.7768 &0.7468 \\ 
%clinic6 &0.7465 &0.8122 &0.7793 &0.7404 \\ 
%clinic7 &0.6567 &0.6895 &0.6625 &0.6476 \\ 
%\hline
%\end{tabular}
%\end{center}
%\caption{Artery geometries example: quality index $q_{\text{mean}}$ for each artery geometry.}
%\label{tab:qualArtMean}
%\end{table}

%\begin{figure}[!htb]
%\begin{center}
%\begin{tabular}{cc}
%\includegraphics[width=0.42\textwidth]{imm/arteryExe/det.png} & 
%\includegraphics[width=0.42\textwidth]{imm/arteryExe/uniformDet.png} \\
%intial  &equal weights \\ 
%\includegraphics[width=0.42\textwidth]{imm/arteryExe/fujDetHigh.png} & 
%\includegraphics[width=0.42\textwidth]{imm/arteryExe/desDetHigh.png} \\
%Fujiwara weights &Desbrun weights\\ 
%\end{tabular} 
%\end{center}
%\caption{Artery geometries example: a detail of the mesh after the smoothing procedure.}
%\label{fig:coronaryNoise}
%\end{figure}

%After smoothing the artery geometry we approximate the curvature with the algorithm introduced in Section~\ref{sec:curvature}.
