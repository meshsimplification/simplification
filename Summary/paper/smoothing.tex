\section{Data Smoothing Technique}

\corr{Triangulated surfaces coming from MRI imaging technique may present stretched triangles and noise. 
which effect the computation of the surface curvature.
To overcome this issue, 
we propose to apply a smoothing procedure on such data \ev{before} computing the discrete curvature.}

\todo[inline]{I rewrite this first paragraph to better introduce the smoothing presented here.}

The Gaussian smoothing is one of the most common techniques for smoothing curves and surfaces~\cite{LaplSmooth}.
It iteratively moves the mesh points to change the shape of the elements without changing the topology of the mesh, i.e.
without changing the connections point-to-point.
For a triangular surface mesh, the position of a points $\P$ is updated by
\begin{equation}
\P_{new} = \P + \lambda \sum_{\P_i\in\omega_\P} w(\P_i,\,\P)\U_i\,,
\label{eqn:gensmooth}
\end{equation}
where $\P_{new}$ is the new position of the point,
$\lambda\in (0,\,1]$ is a scaling factor, 
$\omega_\P$ is the set of points $\P_i$ connected to $\P$,
$\U_i$ is the direction of the edge $\P_i\P$, i.e. the unit vector from $\P_i$ to $\P$, and
$w$ is a weight function such that $w>0$ and $\sum_{\P_i\in\omega_\P}w(\P_i,\,\P) =1$.

Different smoothing methods are characterized by different choices of the scaling factor and the weight function in Equation~\eqref{eqn:gensmooth}~\cite{LaplSmooth,trapSmoot,Bossen,DassiPDHDE}. The resulting smoothed mesh depends heavily on this choice with regard to the regularity of the triangles and the smoothness. Applying this smoothing technique, will reduce the triangles' stretch and 
make them as close as possible to the equilateral one.
However, one of the drawbacks of this procedure is the mesh shrinkage, i.e. 
applying the node smoothing for many iterations,
will reduce the volume enclosed by the surface. 
This shrinkage may also entail a loss of accuracy, especially for fine, sharp parts of the geometry 
that are essential and no artifacts.

\corr{In this paper} we are interested in a smoothing method that
\begin{enumerate}
 \item reduces the noise of the surface mesh,
 \item preserves the geometry represented by the surface mesh,
 \item reduces the triangles' stretch, producing a more regular mesh.
\end{enumerate}

One of the smoothing techniques that 
satisfies \ev{all} these requirements is the so-called Taubin smoothing~\cite{Taubin,taubinWeight}.
It consists of two consecutive Gaussian smoothing steps with different values of the scale factor $\lambda$.
More precisely, after a first Gaussian smoothing step with a positive value $\lambdaPlus$,
a second Gaussian smoothing step is done with a negative value $\lambdaNeg$, 
these two values are chosen such that $0~<~\lambdaPlus~<~-~\lambdaNeg$.
To produce a significant smoothing effect on the final mesh, 
these two steps have to be repeated iteratively.

The effect of the Taubin smoothing on the mesh is a \ev{low pass filter} effect on the curvature, where 
$\lambdaPlus$ and $\lambdaNeg$ determines the \corr{pass-band} and the stop-band of the curvatures~\cite{Taubin,taubinWeight}.
However, since it is based on the Gaussian smoothing, 
even the Taubin smoothing still tends to \corr{shrink} the input triangular surface mesh.

\subsection{Smoothing parameters}\label{sub:smoothPara}

The equal weights proposed by Taubin give good smoothing results for meshes 
with small variation in the edge length and face angles across the mesh~\cite{Taubin,taubinWeight}. 
For more irregular meshes, like ones obtained from medical imaging, 
other weights might be better suited to compensate the irregular triangulation. 
Several choices of weight functions $w$ have been proposed in~\cite{taubinWeight}, namely:
\begin{itemize}
 \item \textsl{equal weights:} in this case all the edges connected to $\P$ are weighted with the same value, 
 $w$ is a constant function defined as 
 \begin{equation}
 w(\P_i,\,\P) := \frac{1}{|\omega_\P|}\,,
 \end{equation}
 where $|\omega_\P|$ is the number of points connected to $\P$,
 \item \textsl{Fujiwara weights:} the edges are weighted according to the inverse of the edge length, i.e. 
 \begin{equation}
 w(\P_i,\,\P) := \frac{\corr{c}}{||\P_i\P||}\,,
 \end{equation}
 where $||\P_i\P||$ is the Euclidean length of the edge $\P_i\P$ and 
 \corr{$c$ is a proper constant to normalize such weights, i.e. to have the following property
 $$
 \sum_{i=1}^{|\omega_\P|} w(\P_i,\,\P) = 1\,,
 $$}
 \item \textsl{Desbrun weights:} in this case the edges are weighed by the adjacent angles
 $$
 w(\P_i,\,\P) := c\,(\cot(\alpha_i)+\cot(\beta_i))\,,
 $$
 where the angles $\alpha_i$ and $\beta_i$ are the angle opposite to the edge $\P_i\P$, 
 see Figure~\ref{fig:angConf} right,
 \corr{also in this case we put a constant $c$ to normalize such weights.}
\end{itemize}

We notice that the proposed smoothing scheme is an \ev{iterative} procedure.
Consequently, since we are interested in recovering well-shaped triangles and reducing the noise of the surface mesh, 
we would like to apply such smoothing procedure ``as much as'' possible.

One of the classical stopping criterion of this iterative procedure is the maximum number of iterations.
However, it is not clear how \corr{many} iterations are required to obtain a noise reduction in the final mesh 
without losing the surface approximation of the geometry at hand.
To avoid this issue, we adopt a more sophisticated way to break the node smoothing,
see line~\ref{alg:linestop} of Algorithm~\ref{alg:smoothSheme}.
Assume that $\surfMesh$ encloses a volume and 
let $V$ be the measure of the volume after $i^{\text{th}}$ iterations.
We apply the Taubin smoothing until the percentage of volume loss exceeds a certain threshold $\tolV$,
$$
\frac{|V-V_0|}{V_0}<\tolV\,,
$$
where $V_0$ is the volume enclosed by the initial surface mesh.
In this way the stopping criterion is a trade off between the volume reduction and the smoothness.

\subsection{Computation of the volume}

The key aspect of the proposed stopping criterion is the volume computation. 
Since we are dealing with complex surfaces,
we seek a generic algorithm to compute the volume starting from the surface mesh. 
The idea is to use the divergence theorem.

\begin{theorem}
Let $V$ be a subset of $\mathbb{R}^3$ which is compact and has a piecewise smooth boundary $S$.
If $\mathbf{F}$ is a continuously differentiable vector field defined on a neighborhood of $V$,
then we have:
\begin{equation}
\iiint_V \nabla\cdot \mathbf{F}\,dV = \iint_S  \mathbf{F}\cdot\mathbf{n}\,dS\,.
\label{eqn:divTeoGen}
\end{equation}
where $\nabla$ is the standard gradient operator and 
$\mathbf{n}$ is the outward pointing normal of the surface $S$.
\end{theorem}

In Equation~\eqref{eqn:divTeoGen} if $\nabla\cdot \mathbf{F}$ is the identity function,
one can exploit this equation to compute the volume $V$ starting from the surface $S$.
Consider for instance the vector fields, 
$$
\textbf{F}_1 := \left(\begin{array}{c} x \\ 0 \\ 0\end{array}\right)\,,\quad
\textbf{F}_2 := \left(\begin{array}{c} 0 \\ y \\ 0\end{array}\right)\quad\text{and}\quad
\textbf{F}_3 := \left(\begin{array}{c} 0 \\ 0 \\ z\end{array}\right)\,.
$$
then we have 
$$
V = \iiint_V \,dV = \iint_S  \mathbf{F}\cdot\mathbf{n}\,dS\,,
$$

The vector fields $\textbf{F}_1$, $\textbf{F}_2$ or $\textbf{F}_3$ are linear functions over the triangular surface $\surfMesh$
so we can compute such volume exactly, i.e. up to the machine precision,
by taking a proper integration quadrature rule.
In this case a Gaussian quadrature rule with three points is enough to be exact in the computation of the volume.

\subsection{The proposed smoothing scheme}
The mesh smoothing scheme using the algorithm proposed by Taubin 
with an additional restriction to the volume loss is summarized in Algorithm~\ref{alg:smoothSheme}. 

This algorithm takes five inputs: 
an initial mesh $\surfMeshIni$,
the scaling factors $\lambdaPlus$ and $\lambdaNeg$, the type of weights $\wt$ and 
the volume tolerance $\tolV$ as a threshold for the volume loss.

The result is a mesh $\surfMeshFin$, 
where the initial points are moved to reduce the triangles' stretch and the noise of the data. 
Moreover, $V_0$ denotes the volume enclosed by $\surfMeshIni$,
the volume enclosed by $\surfMeshFin$ is lower than $(1+\tolV)V_0$ \corr{and larger than $(1-\tolV)V_0$.}

\begin{algorithm}[!htb]
$\surfMeshFin$ = \textsc{smoothing}($\surfMeshIni$, $\lambdaPlus$, $\lambdaNeg$, $\wt$, $\tolV$)\\
\begin{algorithmic}[1]
\STATE $V_0$ = volume of $\surfMeshIni$ 
\STATE initialise: $\surfMesh = \surfMeshIni$, $V= V_0$;\\
\WHILE{$\left(\frac{|V-V_0|}{V_0}<\tolV\right)$}\label{alg:linestop}
\STATE $\surfMesh^{\text{new}}$ = Gaussian smoothing with $\lambdaPlus$ and $\wt$ on $\surfMesh$ 
\STATE $\surfMesh^{\text{new}}$ = Gaussian smoothing with $\lambdaNeg$ and $\wt$ on $\surfMesh^{\text{new}}$
\STATE $V$  = volume of $\surfMesh^{\text{new}}$ 
\STATE $\surfMesh$ = $\surfMesh^{\text{new}}$
\ENDWHILE
\STATE $\surfMeshFin$ = $\surfMesh$
\end{algorithmic}
\caption{Pseudo-code of the proposed mesh smoothing algorithm.}
\label{alg:smoothSheme}
\end{algorithm}


\subsection{Numerical validation of Taubin smoothing}\label{sub:smoothTest}

In this section we numerically validate the Taubin smoothing procedure.
More specifically we will focus on the weights introduced in the previous subsection and 
their effect in removing the noisy data from a mesh. 
In our numerical experiments we consider the values of $\lambdaPlus$ and $\lambdaNeg$ proposed in~\cite{Taubin}. 

As indicator for the quality obtained with the smoothing,
we consider the so-called aspect ratio~\cite{Shewchuk02elem}, i.e.
$$
q(T) := 2\frac{r_T}{R_T}\,,
$$
where $T$ is a generic triangle of the mesh, 
$r_T$ and $R_T$ are the radii of the inscribed and circumscribed circle, respectively.
For an equilateral triangle $q(T)=1$, while 
if $q(T)$ is close to zero,
$T$ is stretched.
Moreover, to have a single quantity to evaluate the whole mesh quality,
we compute
$$
q_{\text{mean}}:=\frac{1}{\numOfTriaSurfMesh}\sum_{T\in \surfMesh}q(T)\,,
$$
where $\numOfTriaSurfMesh$ is the number of triangles in the surface $\surfMesh$.

In the following, we use $q_{\text{mean}}$ as proxy for mesh quality:
the more this value is close to 1, 
the more the triangles in the mesh are close to the equilateral one.

\paragraph{Meshes with artificial noise.}

Here we consider some well-known surface mesh~\cite{scanRepo}.
Then, to obtain a noisy mesh,
we move some points along the normal to the surface, see Figures~\ref{fig:noisyBunny} 
and~\ref{fig:noisyCow}.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/bunny.png}&
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/bunnyNoisy.png}\\
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/bunnyDet.png}&
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/bunnyNoisyDet.png}\\
\end{tabular}
\end{center}
\caption{Meshes with artificial noise example: initial meshes, left, the same mesh after noisy.}
\label{fig:noisyBunny}
\end{figure}

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.4\textwidth]{imm/cowExe/cow.png}&
\includegraphics[width=0.4\textwidth]{imm/cowExe/cowNoisy.png}\\
\includegraphics[width=0.4\textwidth]{imm/cowExe/cowDet.png}&
\includegraphics[width=0.4\textwidth]{imm/cowExe/cowNoisyDet.png}\\
\end{tabular}
\end{center}
\caption{Meshes with artificial noise example: a detail of the initial meshes, left, the same detail after noisy.}
\label{fig:noisyCow}
\end{figure}

For both these examples, we apply the smoothing procedure described in Algorithm~\ref{alg:smoothSheme}.
We fix the parameters $\lambdaPlus=0.33$, $\lambdaNeg=-0.34$~\cite{Taubin}, 
but we consider all the weight functions defined in Subsection~\ref{sub:smoothPara}.
For the bunny example we take $\tolV=0.01$, while for the cow example $\tolV=0.07$.

In Figures~\ref{fig:resBunny} and~\ref{fig:resCow}, we show the meshes 
obtained after the smoothing procedure with different weights.
In Table~\ref{tab:iter} we report the number of iterations required and 
in Table~\ref{tab:qual} we show the quality index $q_{\text{mean}}$ of the final mesh.

All the weights remove the noise on these two examples, see Figures~\ref{fig:resBunny} and~\ref{fig:resCow}
and they take about the same number of iterations to achieve this goal, see Table~\ref{tab:iter}.
However, the equal weights function is the one that obtains such good results with the best mesh quality.
This fact is clear from the details in Figures~\ref{fig:resBunny} and~\ref{fig:resCow}, 
it becomes more evident from the data in Table~\ref{tab:qual}.
Indeed, from Figures~\ref{fig:resBunny} and~\ref{fig:resCow}
we see that the mesh associated with the equal weights is the one composed by more regular shapes.
Then, in Table~\ref{tab:qual}
the equal weights get quality index $q_{\text{mean}}$ closer to one
with respect to the other weights functions in both the examples.

Moreover, in Figure~\ref{fig:iterAndQual} we show the trend of $q_{\text{mean}}$ varying the iterations and the weights used.
In the case of equal and Fujiwara weights
the smoothing process the quality index increases a lot at the beginning,
then it becomes constant.
This is a further numerical prove 
that the smoothing itself can not improve the quality so much.
Consequently to get a high mesh quality,
one have to exploit other mesh operations, e.g. edge flipping, edge contraction and 
point insertion~\cite{frey-borouchaki-03,NME:NME992,FreyBorouchaki98surfopt,gmsh-paper}.
The smoothing procedure associated with the Desbrun weight has a bad behavior.
Indeed in Figure~\ref{fig:iterAndQual} we observe that the trend of $q_{\text{mean}}$ decreases with the iterations.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/bunnyNoisyDet.png} & 
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/uniformDet.png} \\
intial noisy mesh &equal weights \\ 
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/fujDet.png} & 
\includegraphics[width=0.4\textwidth]{imm/bunnyExe/desDet.png} \\
Fujiwara weights &Desbrun weights\\ 
\end{tabular}
\end{center}
\caption{Meshes with artificial noise example: results of bunny example with different weights.}
\label{fig:resBunny}
\end{figure}

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.4\textwidth]{imm/cowExe/cowNoisyDet.png} & 
\includegraphics[width=0.4\textwidth]{imm/cowExe/uniformDet.png} \\
intial noisy mesh &equal weights \\ 
\includegraphics[width=0.4\textwidth]{imm/cowExe/fujDet.png} & 
\includegraphics[width=0.4\textwidth]{imm/cowExe/desDet.png} \\
Fujiwara weights &Desbrun weights\\ 
\end{tabular}
\end{center}
\caption{Meshes with artificial noise example: results of cow example with different weights.}
\label{fig:resCow}
\end{figure}

\begin{table}[!htb]
\begin{center}
\begin{tabular}{|c|ccc|}
\hline
example &equal weights &Fujiwara weights &Desbrun weights \\
\hline
bunny   &48            &52               &68 \\
cow     &28            &26               &26 \\
\hline
\end{tabular}
\end{center}
\caption{Meshes with artificial noise example: number of iterations required to de-noise the mesh.}
\label{tab:iter}
\end{table}

\begin{table}[!htb]
\begin{center}
% \begin{tabular}{|c|c|ccc|}
% \hline
% example                  &quality index       &equal weights &Fujiwara weights &Desbrun weights \\
% \hline
% \multirow{3}{*}{bunny}   &$q_{\min}$           &0.0315       &0.0003           &0.0001   \\
% 			 &$q_{\max}$           &0.9999       &0.9999           &0.9999   \\
%                          &$q_{\text{mean}}$    &0.8845       &0.8609           &0.8073   \\
% \hline                                                                         
% \multirow{3}{*}{cow}     &$q_{\min}$           &0.0079       &0.0018           &0.0042   \\
% 			 &$q_{\max}$           &0.9998       &0.9998           &0.9997   \\
%                          &$q_{\text{mean}}$    &0.8107       &0.7517           &0.6873   \\
% \hline                                                                         
% \end{tabular}
\begin{tabular}{|c|c|ccc|}
\hline
example    &initial      &equal weights &Fujiwara weights &Desbrun weights \\
\hline
bunny      &0.8144       &0.8845       &0.8609           &0.8073   \\
\hline                                                                         
cow        &0.7399       &0.8107       &0.7517           &0.6873   \\
\hline                                                                         
\end{tabular}
\end{center}
\caption{Meshes with artificial noise example: 
value of $q_{\text{mean}}$ in the initial and final meshes for each weight function.}
\label{tab:qual}
\end{table}

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{c}
\includegraphics[height=0.5\textwidth]{imm/bunnyExe/meanQualAndIterBunny.eps} \\
\includegraphics[height=0.5\textwidth]{imm/cowExe/meanQualAndIterCow.eps}  
\end{tabular}
\end{center}
\caption{Meshes with artificial noise example: value of $q_{\text{mean}}$ at each smoothing iteration 
for each weight function.}
\label{fig:iterAndQual}
\end{figure}




