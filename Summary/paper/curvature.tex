\section{Discrete Curvature Approximation}\label{sec:curvature}

In 2D the curvature definition has various formulations, like the deviation of the curve from the tangent line. However, in 3D the curvature definition needs a little more care, especially in the case of discrete surfaces. Aiming to introduce a curvature approximation for discrete surfaces (in the specific triangulated ones), we first give a short introduction to surface curvatures and then describe the approximation procedure~\cite{Meyer2003,Goldman}.

Considering a sufficiently regular surface $\surf$ and the normal $\N$ at a generic point $\P\in\surf$, we can construct a set of \ev{normal planes} at the point $\P$ 
which contain the normal vector $\N$ and the point $\P$. 
All these planes cut the surface $\surf$ in different plane curves that are characterised by a distinct curvature behaviour~\cite{EncyMath}.
We denote by $k_1$ and $k_2$ the \ev{principal curvatures}, i.e. 
the maximum and minimum values of the curvature of all these curves in $\P$.
Then, two common measures for the curvatures have been defined to describe how the surface bends:
the \ev{Gaussian curvature}, $K(\P):=k_1\,k_2$, and
the \ev{mean curvature} is~$H(\P):=(k_1+k_2)/2$.

For an analytic evaluation of the curvature, a parametrisation of the surface $\surf$ is necessary. In the case of discrete surfaces, like the brain geometry reconstructed from MRI imaging, we do not have such parametrisation. Moreover, since we are dealing with the complex triangular surface of the brain reconstruction, finding such parametrisation will require a high computational effort and sophisticated techniques, see for instance~\cite{paraSurf}.

We avoid such onerous procedure by computing the so-called discrete Gaussian and mean curvature~\cite{Meyer2003}. These computations do not require any parametrisation of the surface, they are based on the computation of angles, areas and edge lengths in the first ring triangles which share the point $\P$. The discrete approximations of the Gaussian and mean curvature read
\begin{eqnarray}
K_h(\P) &=&  
\frac{1}{\mathcal{A}_{mixed}} 
\left(2\pi - \sum_{i=1}^{n_{\mathbf{p}}}\theta_i\right)\,,
\nonumber \\
&& \label{eqn:allDiscrete}\\
H_h(\P) &=& 
\mathlarger{\frac{1}{2}}\,\Bigg|\Bigg|\frac{1}{\mathcal{A}_{mixed}}\sum_{i=1}^{n_{\P}}
(\cot \alpha_i + \cot \beta_i) (\P-\P_i)\,\Bigg|\Bigg|\,,
\nonumber 
\end{eqnarray}
where $\mathcal{A}_{mixed}$ is the discrete cell Voronoi area around the vertex $\P$, $n_{\P}$ is the number of triangles around the point $\P$, $\theta_i$ is the angle at the vertex $\P$ of the triangle $T_i$, $\alpha_i$ and $\beta_i$ are the angles opposite to the edge $\P\P_i$ (see Figure~\ref{fig:angConf} for the notations), and $||\cdot||$ is the standard Euclidean vector norm. The subscript $h$ refers to the discrete mesh with a maximum diameter $h$ of the elements of the triangulation. For a deeper analysis of the above equations, we refer to~\cite{Meyer2003}.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{ccc}
\includegraphics[height=0.15\textheight]{imm/gaussQuant.png} &\phantom{mm} & 
\includegraphics[height=0.15\textheight]{imm/meanQuant.png}\\
(a) Gaussian curvature &&(b) mean curvature\\
\end{tabular}
\end{center}
\caption{Two dimensional planar view of the one ring triangles around the point $\P$, where we highlight the quantities involved in the computation of the Gaussian $K_h(\P)$ (a) and mean curvature $H_h(\P)$ (b).}
\label{fig:angConf}
\end{figure}

\subsection{Numerical validation of discrete curvature}

To validate the discrete approximations of the curvature on simple and well-known geometries, we consider as an example a sphere and a paraboloid whose Gaussian and mean curvature are exactly known. In particular, we have:
\begin{itemize}
\item a sphere $\mathcal{S}$ of radius $r=1/2$. In all the points on $\mathcal{S}$, the principal curvatures are equal, namely $k_1=k_2=1/r=2$. Thus, the Gaussian and mean curvature in every point $\P$ of $\mathcal{S}$ are:
 \begin{equation*}
 K(\P)=k_1\,k_2 = 4 \quad \text{ and } \quad H(\P)=(k_1+k_2)/2 = 2.
 \end{equation*}
 \item an elliptic paraboloid $\mathcal{P}$ with equation $z=x^2+y^2$, parametrised as $(u,v)\mapsto (u,v,u^2+v^2)$. In all points $\P$ this ellipsoid has the Gaussian and mean curvature
\begin{equation*}
K(\P) = \frac{4}{(1+4x_\P^2+4y_\P^2)^2}\quad\quad\text{and}\quad\quad H(\P) = \frac{4x_\P^2+4y_\P^2+2}{(1+4x_\P^2+4y_\P^2)^{3/2}}\,,
\end{equation*}
where $x_\P$ and $y_\P$ are the $x$ and $y$ coordinates of the point $\P$, respectively.\end{itemize}

Then, to proceed with the validation of the discrete curvature estimators, we define the following errors indicators 
\begin{equation}
e_K := || \texttt{K}_h - \texttt{K} ||_{l^2}\quad\quad\text{and}\quad\quad
e_H := || \texttt{H}_h - \texttt{H} ||_{l^2},
\label{eqn:errorCurv}
\end{equation} 
where $\texttt{K}_h$ and $\texttt{H}_h$ are two vectors 
whose entries are the discrete Gaussian and mean curvature at each mesh vertex,
$\texttt{K}$ and $\texttt{H}$ are two vectors whose entries are the exact values of the Gaussian and mean curvature at each mesh vertex, and $||\cdot||_{l^2}$ denotes the standard $l^2$-norm.
 
We build a set of nested unstructured meshes of these two geometries and 
we make a convergence analysis on the error indicators $e_k$ and $e_H$. Figure~\ref{fig:convK} shows the convergence of the error for the two examples.
The error decays with the mesh-size $h$ for both example geometries.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.49\textwidth]{imm/convSphere.eps} &
\includegraphics[width=0.49\textwidth]{imm/convPara.eps}
\end{tabular}
\end{center}
\caption{Curvature example: convergence graphs for $e_K$ and $e_H$ error for the sphere example, left. 
The convergence graphs for $e_K$ and $e_H$ error for the paraboloid example, right.}
\label{fig:convK}
\end{figure}


 
%------------------------------------------------
%            OLD VERSION WITH THE MAX
%------------------------------------------------
% Then we take into consideration different surface discretisations of the two geometries, in the specific a structured and an unstructured triangular mesh with 4800 or rather 4284 elements for the sphere and 1682 or rather 1688 elements for the paraboloid. In order to asses the resulting curvature approximation of the discrete surface we compare the exact curvature values with the approximated ones of the Gaussian $K_h$ and mean curvature $H_h$ computed with Equation~\ref{eqn:allDiscrete}. For a quantitative analysis of the error in the evaluation of such discrete curvatures, we consider the relative errors defined as
% \begin{equation}
% e_K := \frac{\max_{\P\in \surfMesh} | K_h(\P) - K(\P) |}{\max_{\P\in \surfMesh} K(\P)}\quad\quad\text{and}\quad\quad
% e_H := \frac{\max_{\P\in \surfMesh} | H_h(\P) - H(\P) |}{\max_{\P\in \surfMesh} H(\P)}\,.
% \label{eqn:errorCurv}
% \end{equation}
% In Table~\ref{tab:error} we provide the values of both $e_K$ and $e_K$ for the different example geometries and for each mesh type. Both the relative error of the discrete approximation of the Gaussian and the mean curvature are low and the numerical evaluation of the discrete curvature values thus accurate~\cite{Meyer2003}.
% 
% \begin{table}[!htb]
% \begin{center}
% \begin{tabular}{|c||c|c|c|c|}
% \hline
% geometry                    &mesh     &elements &$e_K$ &$e_H$\\
% \hline
% \multirow{2}{*}{sphere}     &struct   &4800 &3.610e-03 &2.263e-03 \\
%                             &unstruct &4284 &5.783e-02 &5.653e-02 \\
% \hline                            
% \multirow{2}{*}{paraboloid} &struct   &1682 &1.188e-03 &1.169e-03 \\
%                             &unstruct &1688 &3.200e-01 &4.087e-01 \\
% \hline
% \end{tabular}
% \end{center}
% \caption{The relative errors $e_K$ and $e_H$ of the discrete Gaussian and the mean curvature for each example with different mesh types.}
% \label{tab:error}
% \end{table}

\subsection{Smoothing impact on curvature computation}

The computation of the curvature the brain geometry is affected by the artefacts. In this section we study the impact of the smoothing approach on the computation of the curvature of the cortical surfaces. We smoothen the brain geometry with the Taubin scheme with edge-flipping that was proven best for this type of geometry (see Section~\ref{sec:results}).

In Figure~\ref{fig:meanCurv} we show details of the brain geometry affected by noise and the curvature values for the original geometry and the one obtained with the smoothing scheme. We highlight the values of the discrete mean curvature, the results for the Gaussian curvature are similar. 

Even though the curvature values in some points without apparent high curvature show extreme values in the smooth, the amount of these points is reduced in comparison tp the original mesh. In the smooth mesh the curvature values give a better indication of the bending of the surface and are less affected by noise.

% \begin{figure}[!htb]
% \begin{center}
% \begin{tabular}{cc}
% initial &
% equal weights\\
% \includegraphics[width=0.45\textwidth]{imm/detCur1/iniCur1.png} &
% \includegraphics[width=0.45\textwidth]{imm/detCur1/uniCur1.png}\\
% \multicolumn{2}{c}{mean curvature}\\
% \multicolumn{2}{c}{$0\quad$\includegraphics[width=0.45\textwidth]{imm/detCur1/legendOk.png}$\quad>1$}\\[1em]
% \end{tabular}
% \end{center}
% \caption{Brain geometry example: the mean curvature on the original and smoothed geometry in the lateral and medial view.}
% \label{fig:det3Curv}
% \end{figure}

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{ccc}
\includegraphics[width=0.3\textwidth]{Figures/orig_mean_curvature.png} &
\includegraphics[width=0.3\textwidth]{Figures/orig_mean_curvature1.png} &
\includegraphics[width=0.3\textwidth]{Figures/orig_mean_curvature2.png}\\
& (a) original & \\[1em]
\includegraphics[width=0.3\textwidth]{Figures/taubin_mean_curvature.png} &
\includegraphics[width=0.3\textwidth]{Figures/taubin_mean_curvature1.png} &
\includegraphics[width=0.3\textwidth]{Figures/taubin_mean_curvature2.png}\\
& (b) smoothed geometry & \\[1em]
\multicolumn{3}{c}{mean curvature}\\
\multicolumn{3}{c}{$0\quad$\includegraphics[width=0.45\textwidth]{Figures/legend.png}$\quad>1$}\\[1em]
\end{tabular}
\end{center}
\caption{The mean curvature on the original (a) and smoothed geometry (b) in the lateral view (left) and for particular regions of the left hemisphere.}
\label{fig:meanCurv}
\end{figure}

%\begin{figure}[!htb]
%\begin{center}
%\begin{tabular}{ccc}
%\includegraphics[width=0.33\textwidth]{Figures/orig_gaussian_curvature.png} &
%\includegraphics[width=0.33\textwidth]{Figures/orig_gaussian_curvature1.png} &
%\includegraphics[width=0.33\textwidth]{Figures/orig_gaussian_curvature2.png}\\
%& (a) original & \\[1em]
%\includegraphics[width=0.33\textwidth]{Figures/taubin_gaussian_curvature.png} &
%\includegraphics[width=0.33\textwidth]{Figures/taubin_gaussian_curvature1.png} &
%\includegraphics[width=0.33\textwidth]{Figures/taubin_gaussian_curvature2.png}\\
%& (b) smoothed geometry & \\[1em]
%\multicolumn{3}{c}{Gaussian curvature}\\
%\multicolumn{3}{c}{$<-1\quad$\includegraphics[width=0.45\textwidth]{Figures/legend.png}$\quad>1$}\\[1em]
%\end{tabular}
%\end{center}
%\caption{The Gaussian curvature on the original (a) and smoothed geometry (b) in the lateral view (left) and for particular regions of the left hemisphere.}
%\label{fig:GaussCurv}
%\end{figure}
