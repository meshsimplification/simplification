%
% - parla dei due tipi 
%   1) Taubin 
%   2) Improved Lapalcian Smoothing 
% - Nostra aggiunta del flipping per migliorare la qualità
% - Dire gli stop criterion 
% - Problemi per quanto riguarda lo smoothing (improved li evita ma Taubin no)
% 

\section{De-noising cortical surface mesh}\label{sec:smoothing}

In this section we describe the smoothing scheme to denoise a triangulated cortical mesh and improve its quality. Before analysing the whole algorithm at the end of the section, we introduce the main mesh modification procedures in Subsections~\ref{sub:smooth} and~\ref{sub:flip}. In Subsection~\ref{sub:stop}, we elaborate on alternative stopping criteria instead of setting a maximum number of iterations for the smoothing scheme and summarise the overall scheme in Subsection~\ref{sub:algo}.

\subsection{Node smoothing}\label{sub:smooth}

Here we briefly describe the smoothing techniques exploited to de-noise a cortical surface mesh. For a more detailed description of such techniques, we refer to~\cite{Taubin,taubinWeight} for the Taubin smoothing and to~\cite{ILS} for the Laplace+HC smoothing, respectively.

The general idea behind a node smoothing procedure is to iteratively move a point \emph{without} changing the connectivity of the mesh. 
Given a point $\P$ of the mesh, a smoothing procedure changes its position via
\begin{equation}
\P_{\text{new}} = f\Big(\P\,,\left\{\Q\right\}_{\Q\in\mathcal{U}_{\P}}\Big)\,, 
\label{eqn:baseSmoothFormula}
\end{equation}
where $f$ is a generic function which depends on the actual position of the point $\P$ itself, and the position of its neighbours, collected in the set $\mathcal{U}_{\P}$. In the literature there are several smoothing techniques which consider different choices of the function $f$ in Equation~\eqref{eqn:baseSmoothFormula}, so the resulting mesh has different characteristics according to this choice, see e.g.~\cite{Bossen,DassiPDHDE,trapSmoot}. For instance, in a standard Gaussian smoothing procedure with equal weights, Equation~\eqref{eqn:baseSmoothFormula} becomes
\begin{equation}
\P_{\text{new}} = \P + \frac{\lambda}{\#\mathcal{U}_{\P}} \sum_{\P_j\in\mathcal{U}_{\P}}\U_j\,,
\label{eqn:gauss}
\end{equation}
where $\P_{\text{new}}$ is the new position of the point, $\lambda\in (0,\,1]$ is a scaling factor,  $\mathcal{U}_{\P}$ is the set of points $\P_j$ connected to $\P$ via an edge, $\#\mathcal{U}_{\P}$ is the cardinality of the set $\mathcal{U}_{\P}$ and $\U_j$ is the direction of the edge $\P_j\P$, i.e. the unit vector from $\P_j$ to $\P$. 

Applying this smoothing technique to a triangular mesh reduces the triangles' stretch, i.e. it will make them as close as possible to the equilateral one~\cite{LaplSmooth}. However, one of the major drawbacks of this procedure is the \emph{shrinkage effect}, i.e. if a surface embeds a volume in space, such volume is reduced after applying several iterations of the smoothing scheme. This shrinkage can entail a loss of accuracy, especially in the fine, sharp parts of the geometry.

There are different options to choose the weights in Equation~\eqref{eqn:baseSmoothFormula}. However, extensive studies on the weights showed that equal weights lead to the best results for cortical geometries reconstructed from MRI scans. We do not demonstrate these results here but refer to \cite{Kroos2019} for a detailed study of the weights and their impact on the mesh quality of cortical meshes.

\paragraph{Taubin smoothing.}
One of the smoothing techniques that reduces the shrinkage effect is the Taubin smoothing~\cite{Taubin,taubinWeight} which consists of two consecutive Gaussian smoothing steps with different values of the scale factor $\lambda$ in Equation~\eqref{eqn:gauss}. More specifically, after a first Gaussian smoothing step with a positive value $\lambdaPlus$, a second Gaussian smoothing step is performed with a negative value $\lambdaNeg$, these two values are chosen such that $0<\lambdaPlus<-\lambdaNeg$.

The effect of the Taubin smoothing on the mesh is a low-pass filter effect on the curvature, where $\lambdaPlus$ and $\lambdaNeg$ determine the pass-band and the stop-band of the curvatures~\cite{Taubin,taubinWeight}.
%Unfortunately, even if it removes the noise on the mesh, such smoothing procedure tends to shrink the input triangular surface mesh.

\paragraph{Laplacian+HC smoothing.} Another smoothing approach is the Laplacian+HC algorithm first proposed in \cite{ILS}. The main idea behind this method is to correct the new position produced by a smoothing algorithm with both the location at the previous step and the original point location. In the $i$-th iteration of the Laplacian+HC smoothing, the location of a point in the next iteration is given by 
\begin{equation}
\P_{\text{new}}^{i} = f\Big(\P^0\,,\left\{\Q\right\}_{\Q\in\mathcal{U}_{\P}}\,,\P_{\text{new}}^{i-1}\Big)\,,  \quad \text{for } i>0,
\label{eqn:smoothHC}
\end{equation}
where $\P_{\text{new}}^{i-1}$ is the location of $\P$ computed at the $(i-1)$-th iteration and $\P^0$ is the position of $\P$ in the initial mesh.

On the one hand, including the original location of the point preserves the features of the geometry a hand. On the other hand, since the original points can be affected by noise, this inclusion can propagate the artefacts.

\subsection{Edge-flipping}\label{sub:flip}

To further increase the mesh quality we include edge flipping which is the most efficient and effective local operation to modify a generic triangular mesh. Considering two adjacent triangles $\textbf{abc}$ and $\textbf{dcb}$ as in Figure~\ref{fig:edgeFlip}, edge-flipping replaces the edge $\textbf{bc}$ with $\textbf{ad}$ so that the triangles $\textbf{abc}$ and $\textbf{dcb}$ are substituted by $\textbf{bda}$ and $\textbf{cad}$.

\begin{figure}[!htb]
\begin{center}
\includegraphics[width=0.4\textwidth]{imm/flip.png}%\\
%\includegraphics[width=0.4\textwidth]{imm/flipWithAng.png}
\end{center}
\caption{An example of edge flipping. The edge $\textbf{bc}$ is replaced by $\textbf{ad}$ then the triangles $\textbf{abc}$ and $\textbf{dcb}$ are substituted by $\textbf{bda}$ and $\textbf{cad}$.}
\label{fig:edgeFlip}
\end{figure}

Edge-flipping is not applicable to every couple of triangles. To flip an edge $\textbf{bc}$, the following conditions have to be verified:
\begin{itemize}
 \item the internal angles of the triangles satisfy
 $$
 \theta_{\mathbf{abc}} + \theta_{\mathbf{dbc}} < \pi \qquad\text{and}\qquad
 \theta_{\mathbf{acb}} + \theta_{\mathbf{dcb}} < \pi\,,
 $$
 \item when we are considering a surface mesh embedded in 3D, the dihedral angle between the planes identified by the triangles $\mathbf{abc}$ and $\mathbf{cbd}$ is greater than some threshold value. In the numerical experiments of Section~\ref{sec:results} we consider $80^\circ$ as threshold.
\end{itemize}

After checking the validity of these criteria we apply the edge-flipping operation to achieve different goals.
In two-dimensional triangular mesh generation the edge flip is used to build a constrained Delaunay triangulation~\cite{geoTopo}, in~\cite{dassiFarSi} it is used to obtain an anisotropic mesh starting from a high dimensional embedding. 

In this paper we will apply such mesh operation with two purposes.
First of all to improve the shape of the triangular elements, i.e. 
to make them as close as possible to the equilateral one.
Then, we will numerically show that the de-noising effect on the brain geometry is improved by this operation.

\subsection{Stopping criterion}\label{sub:stop}

In this paper we are interested in removing noise from the input cortical surface mesh and preserving the geometry the surface mesh is approximating. Both smoothing techniques proposed in Subsection~\ref{sub:smooth} are iterative and thus need a stopping criterion to terminate the procedure.

One of the classical stopping criteria of an iterative procedure is the maximum number of iterations. However, it is not clear how many iterations are required to get a final mesh
\begin{enumerate}[a)]
 \item \emph{without noise} so that we have a good approximation of the geometry at hand;
 \item \emph{with well-shaped} elements to reduce the numerical error due to randomly shaped triangles~\cite{Shewchuk02elem}.
\end{enumerate}
Consequently, to apply the smoothing procedure ``as much as possible'',
we introduce a more sophisticated stopping criterion based on a volume loss and a mesh quality indicators:
\begin{enumerate}[a)]
 \item \textbf{mesh quality:} As an indicator for the mesh quality (resulting from the smoothing scheme), we consider the aspect ratio of the triangles \cite{Shewchuk02elem}. To accumulate the information of a mesh in a single quantity, we define the mesh quality at each iteration step as 
 $$  q^i_{\text{mean}}:=\frac{1}{\numOfTriaSurfMesh}\sum_{T\in \surfMesh}q(T)\,, $$
 with 
 $$  q(T) := 2\frac{r_T}{R_T}\,,  $$
where $T$ is a generic triangle of the mesh $\surfMesh$ and $r_T$ and $R_T$ are the radii of the inscribed and circumscribed circle, respectively. For equilateral triangles this measure is equal to 1, while $q^i_{\text{mean}}$ close to zero indicates that the triangles of the mesh are streched.
 
 \item \textbf{loss of volume:} Assuming that the mesh encloses a volume that we denote by $V^{i}$ at each iteration step, we apply the smoothing scheme until the percentage of volume loss exceeds a certain threshold. For this we define:
 $$
 \delta_V^i := \frac{|V^i-V^0|}{V^0}\,,
 $$
 where $V^0$ is the volume of the initial mesh, while $V^i$ is the volume at the $i-$th iteration. For the computation of the volume enclosed by the mesh, we exploit the divergence theorem, that states:
\begin{theorem}
Let $V \subset \mathbb{R}^3$ be compact and with a piecewise smooth boundary $S$. If $\mathbf{F}$ is a continuously differentiable vector field defined on a neighbourhood of $V$, we have:
\begin{equation}
\iiint_V \nabla\cdot \mathbf{F}\,dV = \iint_S  \mathbf{F}\cdot\mathbf{n}\,dS\,,
\label{eqn:divTeoGen}
\end{equation}
where $\nabla$ is the standard gradient operator and 
$\mathbf{n}$ is the outward pointing normal of the surface $S$.
\end{theorem}

Considering a vector field $\mathbf{F}$ such that $\nabla\cdot \mathbf{F}=1$, we obtain 
 $$
 V^i = \iiint_{V^i} \,dV = \iiint_{V^i}\nabla\cdot \mathbf{F} \,dV = \iint_S  \mathbf{F}\cdot\mathbf{n}\,dS\,.
 $$
\end{enumerate}
Then, we terminate the de-noising procedure when the quality of the mesh does not improve in the next iterative step, that is when $q^i_{\text{mean}}~<~q^{i-1}_{\text{mean}}$, or when the volume loss exceeds a certain threshold $\tolV$, i.e. $\delta_V^i > \tolV$.


\subsection{Proposed algorithms}\label{sub:algo}

In Algorithm~\ref{alg:denoise} we show the whole de-noising procedure proposed in this article. The proposed algorithm has three inputs
\begin{itemize}
 \item $\surfMeshIni$ the initial noisy mesh;
 \item $\texttt{smoothId}$ a flag which is used to understand which type of smoothing procedure is used (1: Taubin smoothing, 0: Laplace+HC smoothing);
 \item $\tolV$ the volume tolerance to stop the procedure.
\end{itemize}
The output is the new mesh $\surfMeshFin$ which is less affected by noise.

\begin{algorithm}[!htb]
$\surfMeshFin$ = \textsc{denoise}($\surfMeshIni$, $\texttt{smoothId}$, $\tolV$)\\[0.5em]
\begin{algorithmic}[1]
\STATE $V^0$ = volume of $\surfMeshIni$; 
\STATE $Q^0$ = quality of $\surfMeshIni$; 
\STATE initialise: $i=1$, $\surfMesh^i = \surfMeshIni$, $V^1 = V^0$, $Q^1=Q^0$;
\WHILE{($|V^i-V^0|<\tolV\,V_0$) \&\& ($Q^i\geq Q^{i-1}$)}\label{algLin:criterion}
\IF{(i\%10==0)}\label{algLin:ifBegin} 
\STATE apply flipping on $\surfMesh^i$;
\ENDIF\label{algLin:ifEnd} 
\IF{(\texttt{smoothId}==1)}\label{algLin:smoothingBegin}
\STATE $\surfMesh^{i+1}$ = apply Taubin smoothing on $\surfMesh^{i}$;
\ELSE
\STATE $\surfMesh^{i+1}$ = apply Laplace+HC smoothing on $\surfMesh^{i}$;
\ENDIF\label{algLin:smoothingEnd}
\STATE $V^{i+1}$  = volume of $\surfMesh^{i+1}$;
\STATE $Q^{i+1}$  = quality of $\surfMesh^{i+1}$; 
\STATE $\surfMeshFin = \surfMesh^{i}$;\label{algLin:lastMesh}
\STATE $i=i+1$;
\ENDWHILE
\RETURN $\surfMeshFin$;
\end{algorithmic}
\vspace{1.em}
\caption{Pseudo-code of the proposed mesh smoothing algorithm.}
\label{alg:denoise}
\end{algorithm}

It is important to underline some aspects of the proposed denoising algorithm. First of all, in Line~\ref{algLin:criterion} both stopping criteria have to be verified to continue the denoising procedure.

To differentiate between the two proposed smoothing schemes, we add a flag $\texttt{smoothId}$ that is implemented in an ``if-else'' environment, see Algorithm~\ref{alg:denoise} from Line~\ref{algLin:smoothingBegin} to Line~\ref{algLin:smoothingEnd}. The flipping procedure to improve the mesh quality is applied each 10 iterations, see
Algorithm~\ref{alg:denoise} from Line~\ref{algLin:ifBegin} to Line~\ref{algLin:ifEnd}.
Node smoothing is an iterative procedure which converges and gradually loses its effectiveness since it simply moves the points without changing the connectivity. Indeed it will converge to one configuration. 
Consequently, if we slightly modify the mesh via an edge flipping,
we both improve the mesh quality and we can restart the smoothing procedure that will converge to another configuration with better quality. Thus, we apply the flipping procedure every 10 iterations, see Line~\ref{algLin:ifBegin}.
 
%The final mesh returned by Algorithm~\ref{alg:denoise} is $\surfMeshFin$ and it is the last mesh that satisfies the constraints on the volume and on the mesh quality, see Line~\ref{algLin:lastMesh}.




