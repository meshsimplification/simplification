iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
 ];
V = [
53.5755  0
53.4027  0.00322492
53.2342  0.00637028
53.0697  0.00944202
52.9087  0.0124453
52.7513  0.0153849
52.597  0.0182649
52.4457  0.0210889
52.2972  0.0238604
52.1513  0.0265825
52.008  0.0292577
51.8671  0.0318887
51.7284  0.0344777
51.5918  0.0370267
51.4573  0.0395378
51.3247  0.0420126
51.1939  0.0444528
51.065  0.0468599
50.9377  0.0492353
50.8121  0.0515804
50.688  0.0538963
50.5654  0.0561843
50.4443  0.0584455
50.3245  0.0606808
50.2061  0.0628912
50.0889  0.0650777
49.973  0.067241
49.8583  0.069382
49.7448  0.0715014
 ];
Q = [
0.733884
0.736803
0.739423
0.741757
0.743821
0.745629
0.7472
0.748551
0.749697
0.750656
0.751443
0.752071
0.752555
0.752905
0.753134
0.753252
0.753269
0.753192
0.753025
0.752771
0.752435
0.752021
0.751535
0.75098
0.75036
0.749679
0.74894
0.748148
0.747304
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
