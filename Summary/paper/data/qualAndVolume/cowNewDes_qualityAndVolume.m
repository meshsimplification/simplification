iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
 ];
V = [
53.5755  0
53.4062  0.0031603
53.2405  0.00625229
53.0783  0.00928085
52.9192  0.0122503
52.7631  0.0151644
52.6097  0.0180267
52.459  0.0208402
52.3107  0.0236078
52.1648  0.026332
52.021  0.0290152
51.8793  0.0316594
51.7397  0.0342666
51.6019  0.0368385
51.4659  0.0393767
51.3316  0.0418828
51.199  0.0443581
51.068  0.0468039
50.9384  0.0492215
50.8104  0.0516119
50.6837  0.0539761
50.5584  0.0563152
50.4344  0.0586301
50.3116  0.0609216
50.19  0.0631905
50.0697  0.0654377
49.9504  0.0676638
49.8322  0.0698695
49.7151  0.0720555
 ];
Q = [
0.733884
0.731614
0.729446
0.727352
0.725309
0.723299
0.72131
0.719334
0.717365
0.7154
0.713439
0.711483
0.709532
0.70759
0.705656
0.703733
0.70182
0.699918
0.698026
0.696144
0.694272
0.692408
0.690553
0.688705
0.686864
0.685029
0.6832
0.681376
0.679557
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
