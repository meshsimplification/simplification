iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
 ];
V = [
53.5755  0
53.4048  0.00318733
53.2395  0.00627199
53.0791  0.00926495
52.9232  0.0121754
52.7713  0.0150113
52.623  0.0177793
52.478  0.0204853
52.3361  0.0231343
52.197  0.0257307
52.0605  0.0282785
51.9264  0.0307812
51.7946  0.0332418
51.6648  0.0356631
51.5371  0.0380476
51.4112  0.0403974
51.287  0.0427148
51.1645  0.0450014
51.0436  0.047259
50.9241  0.049489
50.806  0.051693
50.6893  0.0538722
50.5738  0.0560278
50.4595  0.0581609
50.3464  0.0602725
50.2344  0.0623635
50.1234  0.064435
50.0134  0.0664875
49.9044  0.068522
49.7963  0.0705391
 ];
Q = [
0.733884
0.749936
0.762762
0.772983
0.781126
0.787609
0.792767
0.796872
0.800142
0.802746
0.804813
0.806445
0.807724
0.808713
0.809465
0.810022
0.810416
0.810668
0.810798
0.81082
0.810747
0.810591
0.81036
0.810062
0.809705
0.809293
0.808832
0.808325
0.807778
0.807192
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality cow example','Interpreter', 'latex', 'FontSize', 20);
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
xlabel('Iterations', 'Interpreter', 'latex', 'FontSize', 15);
ylabel('$q_{\textrm{mean}}$', 'Interpreter', 'latex', 'FontSize', 15);
