clear all 
close all

numEle = [540 2160 8640 34560];
h = sqrt(1./numEle)*sqrt(2);

meanError = [0.00837385, 0.00419457, 0.00205741, 0.00101533];
gaussError = [0.0139472,  0.00661119, 0.00311733, 0.00149835];


loglog(h, gaussError, 'b>-',...
       h, meanError,  'r*-',...
       h, h, '--',...
       'LineWidth', 2, 'MarkerSize', 10);
title('paraboloid case', 'Interpreter', 'latex', 'FontSize', 20);
tmpLegend = legend('$e_K$','$e_H$', '$h$');
set(tmpLegend, 'Interpreter', 'latex');
set(tmpLegend, 'FontSize', 15);
set(tmpLegend, 'Location', 'southwest');
xlabel('$h$', 'Interpreter', 'latex', 'FontSize', 15);
ylabel('error', 'Interpreter', 'latex', 'FontSize', 15);

