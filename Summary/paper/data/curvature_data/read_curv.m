clear all; clc; close all

workdir = pwd;
cd 'artery'

name = 'clinico02';

filename = strcat(name,'_mean_curvature.inp');


Ared = fopen(filename,'r');
str=fgets(Ared);
x = sscanf(str,'%d %d %*d %*d %*d');
npkt_red = x(1);
npolygons_red = x(2);
% read vertices
[B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
%B_red = B_red(1:(end-1));
B_red = reshape(B_red, 3, cnt_red/3);
C = B_red;
% read polygons
[B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
B_red = reshape(B_red, 3, cnt_red/3);
L = B_red';
str=fgets(Ared);
str=fgets(Ared);
str=fgets(Ared);
[mean_curv_reg,c] = fscanf(Ared,'%*d %f', npkt_red);

% find all the points at the cap and the border
b = find(mean_curv_reg<1e-4);

%trisurf(L,C(1,:),C(2,:),C(3,:)); hold
%plot3(C(1,b),C(2,b),C(3,b),'r*'); axis equal

a = setdiff([1:length(C)],b);
disp(['The minimum mean curvature is: ', num2str(min(mean_curv_reg(a)))])
disp(['The maximum mean curvature is: ', num2str(max(mean_curv_reg(a)))])

%% Gaussian curvature
filename = strcat(name,'_gauss_curvature.inp');

Ared = fopen(filename,'r');
str=fgets(Ared);
x = sscanf(str,'%d %d %*d %*d %*d');
npkt_red = x(1);
npolygons_red = x(2);
% read vertices
[B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
%B_red = B_red(1:(end-1));
B_red = reshape(B_red, 3, cnt_red/3);
C = B_red;
% read polygons
[B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
B_red = reshape(B_red, 3, cnt_red/3);
L = B_red';
str=fgets(Ared);
str=fgets(Ared);
str=fgets(Ared);
[gauss_curv_reg,c] = fscanf(Ared,'%*d %f', npkt_red);

a = setdiff([1:length(C)],b);
disp(['The minimum Gaussian curvature is: ', num2str(min(gauss_curv_reg(a)))])
disp(['The maximum Gaussian curvature is: ', num2str(max(gauss_curv_reg(a)))])

cd(workdir)