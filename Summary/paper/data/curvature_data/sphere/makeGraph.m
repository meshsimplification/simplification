clear all 
close all


numEle = [4800 19200 76800 307200];
h = sqrt((pi*0.5^3*4./3.)./numEle)*sqrt(2);

meanError = [0.00217239, 0.00119019, 0.000613106, 0.000308246];
gaussError = [0.00808837,  0.00290113, 0.00129439, 0.000625142];


loglog(h, gaussError, 'b>-',...
       h, meanError,  'r*-',...
       h, h, '--',...
       'LineWidth', 2, 'MarkerSize', 10);
title('sphere case', 'Interpreter', 'latex', 'FontSize', 20);
tmpLegend = legend('$e_K$','$e_H$', '$h$');
set(tmpLegend, 'Interpreter', 'latex');
set(tmpLegend, 'FontSize', 15);
set(tmpLegend, 'Location', 'northeast');
xlabel('$h$', 'Interpreter', 'latex', 'FontSize', 15);
ylabel('error', 'Interpreter', 'latex', 'FontSize', 15);


