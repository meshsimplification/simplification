%% Script to safe the partition of the brain in the regions

function [] = partition_brain(filename,matfile)

%disp('The input is: ')
%disp(filename)
%disp(matfile)
workdir = pwd;

load(matfile)

Ared = fopen(filename,'r');
str=fgets(Ared);
a = sscanf(str,'%d %d %*d %*d %*d');
npkt_red = a(1);
npolygons_red = a(2);
% read vertices
[B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
%B_red = B_red(1:(end-1));
B_red = reshape(B_red, 3, cnt_red/3);
C = B_red;
% read polygons
[B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
B_red = reshape(B_red, 3, cnt_red/3);
L = B_red';

[n N] = size(C);

for k = 1:35
    region = k-1;
    %disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    %disp(['Computations started for region: ',num2str(region)])
% find all points in the respective region
a = find(label_sorted==region);

neighbors = [];
for j = 1:length(a)
    % find all points connected to the outer region
    c = find(L(:,1)==a(j));
    d = find(L(:,2)==a(j));
    e = find(L(:,3)==a(j));
    list_tria = L([c' d' e'],:);
    neighbor_points = setdiff(unique(list_tria),a(j));
    neighbors = [neighbors neighbor_points'];
end

b = unique([neighbors a]);
swap_del = [[1:length(b)]' b'];

C_new = C(:,b);

x1 = ismember(L(:,1),a);
x2 = ismember(L(:,2),a);
x3 = ismember(L(:,3),a);
y = [find(x1==1); find(x2==1); find(x3==1)];
y = unique(y);
L_n = L(y,:);
[nel m] = size(L_n);

L_vec = reshape(L_n,[3*nel,1]);

for i=1:length(b)
    j = b(i);
    L_vec(find(L_vec==j)) = find(swap_del(:,2)==j);
end

L_new = reshape(L_vec,[nel,3]);

%%%%%% SAVE AS VTK FILE %%%%%
name = strcat(filename(1:(end-4)),'_part_reg',num2str(region));
N = size(C_new,2);
M = size(L_new,1);


vtkF = fopen([name '.vtk'],'w+');
fprintf(vtkF, '# vtk DataFile Version 3.0\n');
fprintf(vtkF, 'Partition file of the regions\n');
fprintf(vtkF, 'ASCII\n\n');
fprintf(vtkF, 'DATASET POLYDATA\n');

fprintf(vtkF, 'POINTS %i float\n', N);
for k=1:N
    fprintf(vtkF, '%0.6f %0.6f %0.6f\n', C_new(1,k),C_new(2,k),C_new(3,k));
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POLYGONS %i %i\n', M, 4*M);
for k=1:M
    fprintf(vtkF, '%i %i %i %i\n', 3, L_new(k,1)-1,L_new(k,2)-1,L_new(k,3)-1);
end
fclose(vtkF);

end

cd(workdir)

%end

