%% Script to combine the curvature of the partition of the brain

function [] = combine_curvature_partition(filename,matfile)
%disp('The input is: ')
%disp(filename)
%disp(matfile)

workdir = pwd;

load(matfile)

% initialise vectors for curvatures
mean_curv = zeros(length(label_sorted),1);
gauss_curv = zeros(length(label_sorted),1);
Cnew = zeros(size(C));

cd 'Curvature_Partition'
for k =1:35
    region = k-1;
%disp(['Region: ',num2str(region)])
a = find(label_sorted==region);

neighbors = [];
for j = 1:length(a)
    % find all points connected to the outer region
    b = find(L(:,1)==a(j));
    c = find(L(:,2)==a(j));
    d = find(L(:,3)==a(j));
    list_tria = L([d' b' c'],:);
    neighbor_points = setdiff(unique(list_tria),a(j));
    neighbors = [neighbors neighbor_points'];
end
b = unique([neighbors a]);

name = strcat(filename(1:(end-4)),'_part_reg',num2str(region));
strm = strcat(name,'_mean_curvature.inp');
strg = strcat(name,'_gauss_curvature.inp');

Ared = fopen(strm,'r');
str=fgets(Ared);
x = sscanf(str,'%d %d %*d %*d %*d');
npkt_red = x(1);
npolygons_red = x(2);
% read vertices
[B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
%B_red = B_red(1:(end-1));
B_red = reshape(B_red, 3, cnt_red/3);
C_reg = B_red;
% read polygons
[B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
B_red = reshape(B_red, 3, cnt_red/3);
L_reg = B_red';
str=fgets(Ared);
str=fgets(Ared);
str=fgets(Ared);
[mean_curv_reg,c] = fscanf(Ared,'%*d %f', npkt_red);

Ared = fopen(strg,'r');
str=fgets(Ared);
x = sscanf(str,'%d %d %*d %*d %*d');
npkt_red = x(1);
npolygons_red = x(2);
% read vertices
[B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
%B_red = B_red(1:(end-1));
B_red = reshape(B_red, 3, cnt_red/3);
C_reg = B_red;
% read polygons
[B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
B_red = reshape(B_red, 3, cnt_red/3);
L_reg = B_red';
str=fgets(Ared);
str=fgets(Ared);
str=fgets(Ared);
[gauss_curv_reg,c] = fscanf(Ared,'%*d %f', npkt_red);


% find the entries of a in b (boolean indicatin with 1 that the entry in b
% exists also in a
z = ismember(b,a);
% find the entry number of b whos values conincide with a
q = find(z==1);

% fill up the values of the region in the overall curvature vector for all
% points of the mesh
mean_curv(a) = mean_curv_reg(q);
gauss_curv(a) = gauss_curv_reg(q);
Cnew(:,a) = C_reg(:,q);

end

N = size(Cnew,2);
M = size(L,1);
N==length(C)
cd ..

% save mean curvature
vtkF = fopen([filename(1:(end-4)) '_mean_curvature.vtk'],'w+');
fprintf(vtkF, '# vtk DataFile Version 3.0\n');
fprintf(vtkF, 'Mean curvature\n');
fprintf(vtkF, 'ASCII\n\n');
fprintf(vtkF, 'DATASET POLYDATA\n');

fprintf(vtkF, 'POINTS %i float\n', N);
for k=1:N
    fprintf(vtkF, '%0.6f %0.6f %0.6f\n', Cnew(1,k),Cnew(2,k),Cnew(3,k));
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POLYGONS %i %i\n', M, 4*M);
for k=1:M
    fprintf(vtkF, '%i %i %i %i\n', 3, L(k,1)-1,L(k,2)-1,L(k,3)-1);
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POINT_DATA %i\n', N);
fprintf(vtkF, 'SCALARS mean_curvature float 1\n');
fprintf(vtkF, 'LOOKUP_TABLE default\n');
for k=1:N
    fprintf(vtkF, '%0.6f \n', mean_curv(k));
end
fclose(vtkF);

% save gaussian curvature
vtkF = fopen([filename(1:(end-4)) '_gauss_curvature.vtk'],'w+');
fprintf(vtkF, '# vtk DataFile Version 3.0\n');
fprintf(vtkF, 'Gaussian curvature\n');
fprintf(vtkF, 'ASCII\n\n');
fprintf(vtkF, 'DATASET POLYDATA\n');

fprintf(vtkF, 'POINTS %i float\n', N);
for k=1:N
    fprintf(vtkF, '%0.6f %0.6f %0.6f\n', Cnew(1,k),Cnew(2,k),Cnew(3,k));
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POLYGONS %i %i\n', M, 4*M);
for k=1:M
    fprintf(vtkF, '%i %i %i %i\n', 3, L(k,1)-1,L(k,2)-1,L(k,3)-1);
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POINT_DATA %i\n', N);
fprintf(vtkF, 'SCALARS mean_curvature float 1\n');
fprintf(vtkF, 'LOOKUP_TABLE default\n');
for k=1:N
    fprintf(vtkF, '%0.6f \n', gauss_curv(k));
end
fclose(vtkF);

%%% save as matlab file
C=Cnew;
save(strcat(filename(1:(end-4)),'_curvature.mat'), 'C', 'L', 'label_sorted', 'label_names_sorted','mean_curv','gauss_curv')

cd(workdir)
end





