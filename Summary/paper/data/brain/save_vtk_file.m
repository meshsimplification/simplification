function [] = save_vtk_file(Cleft,Lleft,index_left,name)%Cright, Lright, index_right

Nleft = size(Cleft,2);
Mleft=size(Lleft,1);
%Nright = size(Cright,2);
%Mright=size(Lright,1);

% left
vtkF = fopen([name '_left.vtk'],'w+');
fprintf(vtkF, '# vtk DataFile Version 3.0\n');
fprintf(vtkF, 'Global Asymmetry Index on the Brain\n');
fprintf(vtkF, 'ASCII\n\n');
fprintf(vtkF, 'DATASET POLYDATA\n');

fprintf(vtkF, 'POINTS %i float\n', Nleft);
for k=1:Nleft
    fprintf(vtkF, '%0.6f %0.6f %0.6f\n', Cleft(1,k),Cleft(2,k),Cleft(3,k));
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POLYGONS %i %i\n', Mleft, 4*Mleft);
for k=1:Mleft
    fprintf(vtkF, '%i %i %i %i\n', 3, Lleft(k,1)-1,Lleft(k,2)-1,Lleft(k,3)-1);
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POINT_DATA %i\n', Nleft);
fprintf(vtkF, 'SCALARS potential float 1\n');
fprintf(vtkF, 'LOOKUP_TABLE default\n');
for k=1:Nleft
    fprintf(vtkF, '%0.6f \n', index_left(k));
end
fclose(vtkF);
return
%right
vtkF = fopen([name '_right.vtk'],'w+');
fprintf(vtkF, '# vtk DataFile Version 3.0\n');
fprintf(vtkF, 'Global Asymmetry Index on the Brain\n');
fprintf(vtkF, 'ASCII\n\n');
fprintf(vtkF, 'DATASET POLYDATA\n');

fprintf(vtkF, 'POINTS %i float\n', Nright);
for k=1:Nright
    fprintf(vtkF, '%0.6f %0.6f %0.6f\n', Cright(1,k),Cright(2,k),Cright(3,k));
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POLYGONS %i %i\n', Mright, 4*Mright);
for k=1:Mright
    fprintf(vtkF, '%i %i %i %i\n', 3, Lright(k,1)-1,Lright(k,2)-1,Lright(k,3)-1);
end
fprintf(vtkF, '\n');
fprintf(vtkF, 'POINT_DATA %i\n', Nright);
fprintf(vtkF, 'SCALARS potential float 1\n');
fprintf(vtkF, 'LOOKUP_TABLE default\n');
for k=1:Nright
    fprintf(vtkF, '%0.6f \n', index_right(k));
end
fclose(vtkF);