%function [] = preprocessing_labeling(dataname)

%%%% PREPROCESSING
% check and reorder the labeling

workdir = pwd;
load('data_left_hemisphere.mat')
clear C L

%% left hemisphere
filename = 'uniform_left_final.inp';

% read .inp file
    Ared = fopen(filename,'r');
    str=fgets(Ared);
    a = sscanf(str,'%d %d %*d %*d %*d');
    npkt_red = a(1);
    npolygons_red = a(2);
    % read vertices
    [B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
    %B_red = B_red(1:(end-1));
    B_red = reshape(B_red, 3, cnt_red/3);
    C = B_red;
    % read polygons
    [B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
    B_red = reshape(B_red, 3, cnt_red/3);
    L = B_red';

name = 'uniform_left_final_sorted.mat';
save(name,'L', 'C', 'label_names_sorted', 'label_sorted')

clear L C label_names_sorted label_sorted num b label_names_short x y 
%% right hemisphere

load('data_right_hemisphere.mat')
clear C L

%% left hemisphere
filename = 'uniform_right_final.inp';

% read .inp file
    Ared = fopen(filename,'r');
    str=fgets(Ared);
    a = sscanf(str,'%d %d %*d %*d %*d');
    npkt_red = a(1);
    npolygons_red = a(2);
    % read vertices
    [B_red,cnt_red] = fscanf(Ared,'%*d %f %f %f', 3*npkt_red);
    %B_red = B_red(1:(end-1));
    B_red = reshape(B_red, 3, cnt_red/3);
    C = B_red;
    % read polygons
    [B_red,cnt_red] = fscanf(Ared,'%*d %*d %*s %d %d %d', 3*npolygons_red);
    B_red = reshape(B_red, 3, cnt_red/3);
    L = B_red';

name = 'uniform_right_final_sorted.mat';
save(name,'L', 'C', 'label_names_sorted', 'label_sorted')

cd(workdir)
%end