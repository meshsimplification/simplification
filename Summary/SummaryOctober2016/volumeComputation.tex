\section{Volume Computation}

To control the how much the volume is shrunk by the smoothing procedure, 
we have to find a way to compute the volume of a generic triangular surface mesh which
represents a closed volume.

In such a generic setting we can not make any assumption about the shape of the surface so 
we can not use the classical formulas to evaluate volumes like spheres, cylinders, cubes, etc..
To deal with such an issue, one can exploit the three dimensional version of the divergence theorem.

\begin{theorem}
Suppose $V$ a subset of $\mathbb{R}^3$ which is compact and has a piecewise smooth boundary $S$.
If $\mathbf{F}$ is a continuously differentiable vector field defined on a neighborhood of $V$,
then we have:
\begin{equation}
\iiint_V \nabla\cdot \mathbf{F}\,dV = \iint_S  \mathbf{F}\cdot\mathbf{n}\,dS\,,
\label{eqn:divTeoGen}
\end{equation}
where $\nabla$ is the standard gradient operator and 
$\mathbf{n}$ is the outward pointing normal of the surface $S$.
\end{theorem}

Now we show how to use this theorem to evaluate the volume enclosed in a surface $S$.
If we consider a vector field $\mathbf{F}$ 
such that its divergence $\nabla\cdot \mathbf{F}$, 
the right hand side of Equation~\eqref{eqn:divTeoGen} becomes the volume of the subset $V$, i.e., 
\begin{equation}
\iiint_V \,dV = \iint_S  \mathbf{F}\cdot\mathbf{n}\,dS\,.
\label{eqn:volComput}
\end{equation}
As you can see from Equation~\eqref{eqn:volComput} we recover the volume $V$ via an integral over the surface $S$ 
which enclose $V$.
There are several vector fields $\mathbf{F}$ such that $\nabla\cdot \mathbf{F}=1$, 
the more natural choices are the following linear functions 
\begin{eqnarray}
F_1(x,\,y,\,z) &:=& x\,, \nonumber \\ 
F_2(x,\,y,\,z) &:=& y\,, \nonumber \\
F_3(x,\,y,\,z) &:=& z\,, \nonumber \\
F_4(x,\,y,\,z) &:=& \frac{1}{3}(x+y+z)\,. 
\label{eqn:funToUseForVolume}
\end{eqnarray}

Suppose now that we have a standard piecewise triangular surface mesh $S_h$ 
that approximates the smooth surface $S$.
The right hand side of Equation~\eqref{eqn:volComput} is approximated by 
\begin{eqnarray}
\iint_S  \mathbf{F}\cdot\mathbf{n}\,dS &\approx& \iint_{S_h}  \mathbf{F}\cdot\mathbf{n}_h\,dS_h \nonumber \\
&=& \sum_{i=1}^n \iint_{T_i} \mathbf{F}\cdot\mathbf{n}_{h,i}\,dS_h\,, 
\end{eqnarray}
where $\{T_i\}_{i=1}^n$ is the set of triangles of the triangular mesh $S_h$ 
and $\mathbf{n}_{h,i}$ is the normal of the triangle~$T_i$.

If we choose one of the vector fields in Equation~\eqref{eqn:funToUseForVolume}, 
we have to compute the integral of a linear function over the surface $S_h$ so 
one can exploit a one degree Gaussian integral approximation rule to compute this integral ``exactly'', i.e., 
within machine precision. 

Now we underline one aspect of this way to compute the volume enclosed by a surface $S$.
The discrete integral over the surface $S_h$ is ``exact''. 
This fact \emph{does not} mean that we compute exactly the volume of $V$.
Indeed, the error we make on the computation of such volume is effected
by the discrete approximation of the smooth surface $S$, $S_h$.
The more $S_h$ is ``close'' to $S$, 
this numerical approximation of the volume will be close to the volume of $V$.
