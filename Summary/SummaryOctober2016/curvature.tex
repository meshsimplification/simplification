\section{Curvature computation}

\subsection{Matlab Curvature Estimation}

In this subsection we check the reliability of the proposed procedure to evaluate both the mean and the Gaussian curvature.
To achieve this goal we consider a simple sphere geometry where we know exactly the values of these two quantities.

In differential geometry, the Gaussian curvature of a surface $\Gamma$ at a point $x\in\Gamma$
is the product of the two principal curvatures at this point, i.e., 
\begin{equation}
K_{Gauss}(x) := k_1(x)\,k_2(x)\,,
\label{eqn:gauss}
\end{equation}
where $k_1$ and $k_2$ are the principal curvature at the point $x$.

Then the mean curvature of a surface $\Gamma$ at a point $x\in\Gamma$ is the average of the principal curvatures, i.e.,
\begin{equation}
K_{mean}(x) := \frac{k_1(x)+k_2(x)}{2}\,.
\label{eqn:mean}
\end{equation}

Since the curvature of a sphere $\mathcal{S}$ with radius $r$ is constant and $1/r$,
both the Gaussian and the mean curvatures are constant and their values are
$$
K_{Gauss}^{\mathcal{S}} = \left(\frac{1}{r}\right)^2\quad\textnormal{and}\quad K_{mean}^{\mathcal{S}} = \frac{1}{r}\,.
$$

In Figure~\ref{fig:sphereCurv} we provide the estimated Gaussian and mean curvatures. 
In this exaple we consider a sphere of radius $0.5$ so 
we have 
$$
K_{Gauss}^{\mathcal{S}} = 4\quad\textnormal{and}\quad K_{mean}^{\mathcal{S}} = 2\,.
$$
Unfortunately both the estimated curvatures present issues.
First of all the mean curvature is negative but 
this can be an orientation issue since its module is close to the expected value 2.
A bigger issue can be the estimated curvatures at the poles of the mesh.
In both cases we have a ring around the poles $z=\pm 0.5$
where the estimated curvature are far from the exact value.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.45\textwidth]{Figures/gaussSphere.png} &
\includegraphics[width=0.45\textwidth]{Figures/meanSphere.png} \\
(a) &(b)
\end{tabular}
\caption{Estimated Gaussian curvature for a sphere of radius 0.5 (a).
Estimated mean curvature for the same sphere (b).}
\label{fig:sphereCurv}
\end{center}
\end{figure}

We tryed to change the mesh and we notice that we get better results but 
there is still the issues of the negative curvature, see Figure~\ref{fig:sphereCurvBetter}.
Moreover there are still points whose Gaussian curvature is not correct. 
It seems that these points are the ones connected to 4 triangles, 
see the highlighted detail in Figure~\ref{fig:sphereCurvBetter} (a).

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.45\textwidth]{Figures/gaussSphereBetter.png} &
\includegraphics[width=0.45\textwidth]{Figures/meanSphereBetter.png} \\
(a) &(b)
\end{tabular}
\caption{Estimated Gaussian curvature for a sphere of radius 0.5 (a).
Estimated mean curvature for the same sphere (b) with a higher quality mesh.}
\label{fig:sphereCurvBetter}
\end{center}
\end{figure}

Other than the sphere we tried the cylinder.
Even in this case we know the values of both the Gaussian and the mean curvature
$$
K_{Gauss}^{\mathcal{C}} = 0\quad\textnormal{and}\quad K_{mean}^{\mathcal{C}} = \frac{1}{2r}\,.
$$
In this example we consider a cylinder of radius $r=0.5$ so these quantities are 
$$
K_{Gauss}^{\mathcal{C}} = 0\quad\textnormal{and}\quad K_{mean}^{\mathcal{C}} = 1.
$$
In differential geometry a cylinder is an infinite surface but 
to exploit our curvature approximation 
we must have a limited surface so 
we decide to cut and close this surface on the top and the bottom.
The second step was necessary since the curvature fitting program require that the input surface define a finite volume.
Moreover we have not to take into account the computation of the curvature of the points 
on the perimeter of the basis circles since we introduced such covers.

The pictures in Figure~\ref{fig:cylinderCurv} show that the estimated mean curvature value is close to the right value.
Indeed we got $1.0015e+00$ and the exact value is $1$.
On all the points in the interior of the lateral surface of the cylinder we got an estimated curvature of $1.0000e-06$
that is sufficiently close to the exact value $0$.

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.45\textwidth]{Figures/gaussCylinder.png} &
\includegraphics[width=0.45\textwidth]{Figures/meanCylinder.png} \\
(a) &(b)
\end{tabular}
\caption{Estimated Gaussian curvature for a cylinder of radius 0.5 (a).
Estimated mean curvature for the same cylinder (b).}
\label{fig:cylinderCurv}
\end{center}
\end{figure}

\subsection{Discrete Curvature}

In~\cite{Meyer2003} a discrete computation of both the Gaussian and the mean curvature is provided.
This computation is based on the triangular mesh, 
there is no approximation of a smooth surface which fits the input points.
Indeed, to compute the curvature of a point $\mathbf{p}$ of a surface mesh,
this method requires only angles, areas and edge lengths of the first ring triangles which share the point $\mathbf{p}$.

In this document we provide the formula compute such discrete approximation of the curvature, 
we refer to~\cite{Meyer2003} for a deeper analysis:
\begin{eqnarray}
K_{Gauss}(\mathbf{p}) &=&  
\frac{1}{\mathcal{A}_{mixed}} 
\left(2\pi - \sum_{i=1}^{n_{\mathbb{p}}}\theta_i\right)\,,
\nonumber \\
&& \\
K_{mean}(\mathbf{p}) &=& 
\mathlarger{\frac{1}{2}}\,\Bigg|\Bigg|\frac{1}{\mathcal{A}_{mixed}}\sum_{i=1}^{n_{\mathbf{p}}}
(\cot \alpha_{i,j} + \cot \beta_{i,j}) (\mathbf{p}-\mathbf{p}_j)\,\Bigg|\Bigg|\,,
\nonumber 
\label{eqn:allDiscrete}
\end{eqnarray}
where $\mathcal{A}_{mixed}$ is the discrete cell Voronoi area around the vertex $\mathbf{p}$,
$n_{\mathbf{p}}$ is the number of triangles around the point $\mathbf{p}$,
$\theta_i$ is the angle at the vertex $\mathbf{p}$ of the triangle $T_i$,
$\alpha_{i,j}$ and $\beta_{i,j}$ are the angles opposite to the edge $\mathbf{p}\mathbf{p}_j$, see Figure~\ref{fig:angConf},
$||\cdot||$ is the standard Euclidean vector norm.

\begin{figure}[!htb]
\begin{center}
\end{center}
\caption{One ring triangles around the point $\mathbf{p}$, where we highlight the quantities involved in the 
computation of the discrete curvatures.}
\label{fig:angConf}
\end{figure}

We consider the same simple geometries of the previous subsection,
and now we exploit the curvature estimations in Equation~\eqref{eqn:allDiscrete} 
to get both the Gaussian and the mean curvature.
The results are shown in Figure~\ref{fig:discreteCurvatureResults}.

These data seem more accurate than the ones computed before.
In the case of the sphere with a structured mesh, Figure~\ref{fig:discreteCurvatureResults} (a) and (b),
it seems that the computation is wrong since the surface of the sphere is not uniformly colored.
However the maximum and minimum value of the color bar are so close, 
4.014 vs 4.000 and 2.004 vs 1.997 for the Gaussian and the mean curvature, respectively, that 
we can infer that we got a constant value over the whole sphere surface.

In the good quality mesh for the sphere, Figure~\ref{fig:discreteCurvatureResults} (c) and (d),
we still have the issue related to the nodes connected to four triangle, but 
these estimated values of the curvature are closer to the exact value than the ones estimated with the previous method.
We recall that the exact Gauss curvature is 4 and the mean curvature is 2, 
then in Table~\ref{tab:valDiff} we report the values computed with the old method and this new one at some of the point 

\begin{table}[!htb]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
\multicolumn{2}{|c}{Gauss curvature} &\multicolumn{2}{|c|}{Mean curvature} \\
\hline
old        &new          &old &new  \\
\hline
-1.003e+02 &4.277e+00    &-3.107e+00    &2.132e+00    \\
 3.449e+00 &4.102e+00    &-1.848e+00    &1.971e+00    \\
 3.934e+00 &4.112e+00    &-1.984e+00    &2.050e+00    \\
-4.932e+01 &4.025e+00    &-2.006e+00    &2.098e+00    \\
-9.746e+01 &4.012e+00    &-1.827e+00    &1.972e+00    \\
\hline
\end{tabular}
\end{center}
\caption{The values of the estimated curvature of the points which have a value far from the right one with
the new and the old method.}
\label{tab:valDiff}
\end{table}

From Figure~\ref{fig:discreteCurvatureResults} (e) and (f),
we observe that even with the new computation the estimate curvature of the cylinder are close to the exact value. 

\begin{figure}[!htb]
\begin{center}
\begin{tabular}{cc}
Gaussian curvature &mean curvature \\
&\\
\includegraphics[width=0.4\textwidth]{Figures/bad_sphere_new_gauss.png} & 
\includegraphics[width=0.4\textwidth]{Figures/bad_sphere_new_mean.png}\\
(a) &(b)\\ 
\includegraphics[width=0.4\textwidth]{Figures/good_sphere_new_gauss.png} & 
\includegraphics[width=0.4\textwidth]{Figures/good_sphere_new_mean.png}\\
(c) &(d)\\ 
\includegraphics[width=0.4\textwidth]{Figures/cylinder_new_gauss.png} & 
\includegraphics[width=0.4\textwidth]{Figures/cylinder_new_mean.png}\\
(e) &(f)\\ 
\end{tabular}
\end{center}
\caption{Results with the new method.}
\label{fig:discreteCurvatureResults}
\end{figure}

