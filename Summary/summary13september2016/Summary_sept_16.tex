\documentclass[a4paper,oneside,10pt,toc=bibliography]{article}
\usepackage[latin1]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{subfigure}
%\usepackage{subfig}
\usepackage{amssymb}
\usepackage{listings}   
\usepackage{amsmath}
\usepackage{pifont}
\usepackage{mathtools}
\usepackage{float}
\usepackage{listings}
\lstset{language=Matlab}
\usepackage{movie15}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{caption}
\usepackage[latin1]{inputenc}
\usepackage{framed} 
\usepackage{xcolor}
\colorlet{shadecolor}{gray!40}
\usepackage{setspace}
\onehalfspacing
\usepackage[left=3cm,right=2.5cm,top=1.5cm,bottom=1.5cm,includeheadfoot]{geometry}
\newcommand*{\xchapter}{\setcounter{section}{0}\addchap}
\usepackage{natbib}
%\usepackage[round]{natbib}
\usepackage{authblk}
\numberwithin{equation}{section}
\numberwithin{figure}{section}
% Zeilen im Pdf durchnummerieren
%\usepackage{lineno}
%\linenumbers
\usepackage{color, colortbl}
\definecolor{myred}{rgb}{0.93,0.76,0.75}
\usepackage{wrapfig}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{pifont}
\usepackage{tikz}
\usetikzlibrary{calc,trees,positioning,arrows,chains,shapes.arrows, decorations.pathreplacing,decorations.pathmorphing,shapes,matrix,shapes.symbols}

\renewcommand{\vec}[1]{\mbox{\boldmath $#1$}}

\lstdefinestyle{mystyle}
{
   backgroundcolor=\color{gray!30!white},
   % basicstyle=\scriptsize\color{white}\ttfamily
}

\title{\textbf{Results Meeting 13$^{th}$ - 16$^{th}$ September Milan}}
\author{\vspace{-5ex}}
\date{\today}

\begin{document}
\maketitle

\tableofcontents

\section*{Objectives}

In this document we give a short summary of the work process made during the meeting from 13th till 15th of September in Milan.

First of all we shortened and comprised the C$^{++}$ based programm written by Franco Dassi and reduced it to the functions and utilities we need for the smoothening of the triangulated mesh of the brain's cortex. We aim to smoothen the mesh preserving its features and the characteristic bending of the sulci and gyri of the cortex but eliminate noise and discretisation errors originating from the MRI scan or the data assembling. The main objectives are:
\begin{enumerate}
\item redefine the cost function to identify noisy data points (generate an output to visualise these outliers)
\item redefine the contraction rules for those outliers
\item introduce the new material ID (in our case the number of the region each point is associated to)
\item manage the association of the material ID and new data points, data points after redefining the mesh
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Technical Remarks}
\subsection{How to Compile the Mesh Simplification Program}

For the execution of the files redefine the program path for CMake (in order to use the latest Version 3.2) and watch out to execute this in the bash terminal (in the tcsh terminal aliases do not work).

\begin{lstlisting}[frame=trBL]
% go tho the folder MeshDoctorSimp
cd MeshDoctorSimp
% the first time you run it, create the folder 'build' and 
% change to that folder
mkdir build
cd build
% Run CMake (if you changed something in the files) and watch out with 
% the directory path of CMake. This is reading and executing the 
% CMakeList.txt file in MeshDoctorSimp
cmake ..
% Run make (faster on 2 Nodes by adding -j 2)
make
% execute the commands written in the sim2dTest file
./simp2dTest
\end{lstlisting}

\subsection{How to use the Git Repository}
For a more efficient coordination of the files we create a git repository. To get a copy of an existing Git repository, create a new folder for the files from the repository and change to that folder. In order to clone a repository use the command \textit{git clone} with the respective url, e.g.
\begin{lstlisting}[frame=trBL]
git clone https://bitbucket.org/meshsimplification/simplification/src
\end{lstlisting}
To check the status of files, use the command \textit{git status}. If there have been made changes in your folder, the output gives you exact information about which files has been altered in comparison to the latest version in the online repository. For example changes in the LaTex file \textit{Summary\_sept\_16.tex} gives:
\begin{lstlisting}[style=mystyle]
On branch master
Your branch is up-to-date with 'origin/master'.

Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git checkout -- <file>..." to discard changes in working directory)

modified:   Summary/summary13september2016/Summary_sept_16.tex
\end{lstlisting}
To check the exact difference between the file in the working directory and  the staging are, use the command \textit{git diff} with the file name. The result gives the changes, made that have not been staged yet.
\begin{lstlisting}[frame=trBL]
git diff  Summary/summary13september2016/Summary_sept_16.tex
\end{lstlisting}
To add changes to the git repository and upload them in the repository, use the following commands
\begin{lstlisting}[frame=trBL]
% begin tracking a file 
git add Summary/summary13september2016/Summary_sept_16.tex
% now the file is tracked and staged to be committed, 
% this can also be checked by
git status
% commit the changes and add a description of the changes, that appears 
% in the "Commits" section of the Navigation menu
git commit -m "Update of the Summary"
% push the file online
git push origin master
\end{lstlisting}
There is a class of files that should not automatically be added and be tracked in the repository, the build folder containing the results of the mesh simplification and the files produced during the compilation of the LaTex file. Therefor we create a file in the repository named .gitignore, which contains the list of files to be ignore. 
\begin{lstlisting}[style=mystyle]
build/
*.aux
*.log
*.out
*.pdf
*.toc
\end{lstlisting}
In order to update the files in the working directory and download the latest changes in the repository, use the \textit{git pull} command.

\section{Cost Functions}

\subsection{Garland}

For performing a contraction we need to describe the cost of a contraction and we will refer here to the description of \cite{Garland1997}. The cost of a contraction can be characterised by the error at each vertex. Therefore we relate every vertex with a symmetric $4 \times 4$ matrix $Q$ and define the error at the vertex $v=[v_x v_y v_z 1]^T$ as the quadratic form $\Delta (v)=v^TQ v$. The matrix $Q$ is also called error quadric and we are going to explain the derivation of this matrix in detail in the following.

We can associate every vertex with a set of plane whose intersections result in the respective vertex, and then we define the error of the vertex regarding to the set as the sum of squared distances to its planes:
\begin{equation}
\Delta (v) = \Delta([v_x v_y v_z 1]^T) = \sum_{p\in \pi_v} (p^T v)^2,
\label{eq:error_metric}
\end{equation}
where $p=[a, b, c, d]^T$ is a representation of a generic plane in $\mathbb{R}^3$ defined by the equation $ax+by+cz+d = 0$ with $a^2+b^2+c^2=1$ and $\pi_v$ is the set of planes generated by the triangles of the mesh that have $v$ as a vertex. This implies that for all elements $v$ of the plane, $p^T v = 0$. The error metric in \eqref{eq:error_metric} can be rewritten as
\begin{eqnarray*}
\Delta(v) &=& \sum_{p\in \pi_v} (v^T p)(p^Tv) \\
&=& \sum_{p\in \pi_v} v^T (p p^T) v\\
&=& v^T \left( \sum_{p\in \pi_v} \mathbf{K}_p \right)  v\\
&=& v^T Q v,
\end{eqnarray*}
where $\mathbf{K}_p$ is the fundamental error quadric 
\begin{equation*}
\mathbf{K}_p = pp^T = 
\begin{bmatrix} 
a^2 & ab & ac & ad\\
ab & b^2 & bc & bd\\
ac & bc & c^2 & cd \\
ad & bd & cd & d^2\\ 
\end{bmatrix},
\end{equation*}
which can be used to compute the squared distance of a point to the plane $\vec{p}$. The symmetric matrix $\mathbf{Q}$ is the sum of these fundamental quadrics and gives a comprised summary of the whole set of planes. 

So for a given edge $e$ with the two vertices $v_1$ and $v_2$ and their associated matrices $\mathbf{Q}_{v_1}$ and $\mathbf{Q}_{v_2}$ we define the symmetric matrix $\mathbf{Q}_e=\mathbf{Q}_{v_1}+\mathbf{Q}_{v_2}$. As a cost function for the contraction of $e$ we consider the quantity
\begin{equation*}
c(v) = v^{*T} \mathbf{Q}_e v^*
\end{equation*}
for a generic point $v*$. This can estimate the loss of geometric accuracy when contracting $v_1$ and $v_2$ into the node $v^*$.

\subsection{Derivation of a Cost Function detecting Noise}
In order to detect the noise in the data we choose a different type of cost function which penalizes points that have a big distance to their neighbouring points and deviate heavily from the plane created by their neighbours.
For an approximation of the distance of each point to this plane we compute the regression plane of the neighbours and the minimum distance of the considered point to this surface. This is an eigenvalue problem.

For a generic point $v$ on the mesh we consider its neighboring points $v_1, \ldots, v_m$ with $v_i=[x_i,y_i,y_i]^T \in \mathbb{R}^3$ for $i = 1, \ldots, m$. For the distance to the regression plane through the points $v_1, \ldots, v_m$ we are looking for values $a,b,c,d \in \mathbb{R}$ that minimize the function
\begin{equation}
f(a,b,c,d)=\sum_{i=1}^m \frac{|a x_i + b y_i+c z_i +d|^2}{a^2+b^2+c^2}.
\label{eq:1}
\end{equation}
Setting the partial derivative with respect to $d$ equal to zero, we can solve this equation for $d$:
\begin{eqnarray*}
\frac{\partial f}{\partial d} (a,b,c,d) = 0 \quad \Longleftrightarrow \quad d = -(a \bar{x}+ b \bar{y} + c \bar{z}),
\end{eqnarray*}
where $\bar{x}, \bar{y}$ and $\bar{z}$ are the centroid of the data, i.e. $\bar{x} = \sum_{i=1}^m x_i$ and equivalently for the second and third coordinate component. This means that the centroid is contained in the least squares plane. Substituting this in equation \eqref{eq:1} we can rewrite the function $f(a,b,c,d)$ as
\begin{equation}
f(a,b,c,d)=\sum_{i=1}^m \frac{|a (x_i-\bar{x}) + b (y_i-\bar{y})+c (z_i-\bar{z}) |^2}{a^2+b^2+c^2}.
\label{eq:2}
\end{equation}
Switching to the matrix representation we define the vector $w \in \mathbb{R}^3$ and the matrix $M \in \mathbb{R}^{3 \times 3}$ as
\begin{eqnarray*}
w &=& [a \quad b \quad c]\\
M &=& \begin{bmatrix} 
x_1 - \bar{x} & y_1 -\bar{y} & z_1-\bar{z}\\
x_2 - \bar{x} & y_2 -\bar{y} & z_2-\bar{z}\\
\vdots & \vdots & \vdots \\
x_m - \bar{x} & y_m -\bar{y} & z_m-\bar{z}
\end{bmatrix} .
\end{eqnarray*}
Thus we can rewrite the function $f(a,b,c,d)$ in \eqref{eq:2} as
\begin{eqnarray*}
f(w) = \frac{(w^T M^T) M w}{w^T w} = \frac{w^T (M^T M) w }{w^T w}.
\end{eqnarray*}
Defining the matrix $A = M^T M$, which when divided by the number of data points becomes the covariance matrix of the data, we obtain
\begin{eqnarray*}
f(A,w) =  \frac{w^T A w }{w^T w},
\end{eqnarray*}
which is the Rayleigh quotient that reaches its (absolute) minimum value $\lambda_{min}$ (the smallest eigenvalue of $A$) when $w$ is $w_{min}$ (the corresponding eigenvalue). So the orthogonal least squares 3D plane contains the centroid $[\bar{x}, \bar{y}, \bar{z}]^T$ of the data, and its normal vector is $w_{min}$, the eigenvector of the matrix $A$ that is corresponding to its smallest eigenvalue. The distance $l$ of the vertex $v$ to the regression plane of his direct neighbours is then given by
\begin{equation*}
l(v) =  |(v-\bar{v}) \cdot w_{min}|,
\end{equation*}
where $\bar{v}=[\bar{x}, \bar{y}, \bar{z}]^T$ is the centroid of the data.\\

\begin{figure}[H]
\centering
\includegraphics[width = 0.8\textwidth]{Figures/plot_regression_plane.png}
\caption{A visualisation of the vertex $v$, its neighbors $v_i$, the regression plane and the perpendicular who's length indicates the distance of $v$ to the plane.}
\end{figure}

But instead of just using just the distance of each point to the regression plan of its neighbours, we also weight this distance by a measure of the size of the surface patch generated by the triangles of the neightbours. Therefore we choose the minimum distance $l_{min}$ of the neighbours to each other, so that our cost function $c$ in vertex $v$ results in
\begin{equation*}
c(v) = \frac{l}{l_{min}}.
\end{equation*}

\section{Contraction laws}

\section{Association of Material ID to the new grid points}

As our data also includes information about the respective Brodmann's region (which we also call Material ID for the more general case) a point belongs to, we need to formulate a treatment for those information in the case of contractions. In general there are three different cases for the distribution of the material ID in a triangle: all vertices are associated to the same material ID, two vertices have the same ID and the other vertex has a different one, and all vertices are associated to different material IDs. These different cases and the way we treat contractions (e.g. of the edge between $P_2$ and $P_3$) is visualised in \ref{fig:contraction_matID}. For the contraction we consider a contraction to one of the vertices and their midpoints ({\color{red} still to be implemented}). The different cases are treated as follows:
\begin{enumerate}
\item contraction between vertices with the same material ID: new contracted point obtains the same ID
\item 
\end{enumerate}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{Figures/scheme.png}
\end{center}
\caption{The association of new contracted points to the material ID. The red squares and black circle show the associated region of each point and the dotted line the original region border}
\label{fig:contraction_matID}
\end{figure}

\section{How to make an artificial noisy data}

In order to test the new cost function we use a common mesh example and introduce artificially noise. 
We move the points along the normal to the surface by $\alpha l_{\textnormal{noise}}$, i.e., 
the new location of the point, $x_{\textnormal{old}}$, is 
\begin{equation}
x_{\textnormal{new}} = x_{\textnormal{old}} + \alpha l_{\textnormal{noise}} n\,,
\end{equation}
where $\alpha\in [0,\,1]$, 
$l_{\textnormal{noise}}$ is the diameter of the patch of the triangles 
which share the point $x_{\textnormal{old}}$. 

\subsection{Academic Tests}

\begin{figure}[H]
\centering
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/bunny_nonoise.png}}
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/noisy_bunny.png}} \\
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/noisy_bunny_Garlandcostfunction.png}}
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/noisy_bunny_Garlandcostfunction_zoom.png}}\\
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/noisy_bunny_noisecostfunction.png}}
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/noisy_bunny_noisecostfunction_zoom.png}}
\caption{Visualisation of the original mesh of a bunny (a), the mesh with artificially added noise (b), the Garland cost function from section 2.1 plotted on the geometry (c) and a zoom of the cost function (d), the noise detecting cost function from section 2.2 plotted on the geometry (c) and a zoom of the cost function (d).}
\label{fig:noisybunny}
\end{figure}

\begin{figure}[H]
\centering
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/noisycow_noisecost_zoom.png}}
\subfigure[]{\includegraphics[width = 0.45\textwidth]{Figures/noisycow_Garlandcost_notgood.png}} 
\caption{Visualisation of the original mesh of a cow (a), the mesh with artificially added noise (b), the Garland cost function from section 2.1 plotted on the geometry (c) and a zoom of the cost function (d), the noise detecting cost function from section 2.2 plotted on the geometry (c) and a zoom of the cost function (d).}
\label{fig:noisycow}
\end{figure}

From Figures~\ref{fig:noisycow} and~\ref{fig:noisybunny} we numerically show that
the proposed cost function identifies the outliers points.
However there are some points marked by this cost function that
represent important features of the input geometry which do not have to be removed, 
see the highlighted cow part in Figure~\ref{fig:noisycow}.

\section{The brain geometry}

The proposed cost function does not behave in the right way even for the brain geometry.
On the one hand there are some points on the brain mesh identified for the removal which are important features of the brain.
On the other hand there are even some noisy point that are not identified by this cost function, see Figure~\ref{fig:brain_costfkt} (c).

\begin{figure}[H]
\centering
\subfigure[The brain gemeometry]{\includegraphics[width = 0.9\textwidth]{Figures/left_hemisphere.png}}\\
\subfigure[The cost function]{\includegraphics[width = 0.9\textwidth]{Figures/left_hemisphere_costfunction.png}}\\
\subfigure[A close-up of the visual cortex (left) and the corpus callosum (right)]{\includegraphics[width = 0.9\textwidth]{Figures/left_hemisphere_costfunction_zoom.png}}
\caption{Visualisation of the original brain mesh of the left hemisphere in the lateral and medial view (a), the cost function plotted on the geometry (b) and a zoom of the cost function (c).}
\label{fig:brain_costfkt}
\end{figure}



\section{Ideas and Prospect}
\begin{enumerate}
\item Idea: Combine the two cost functions (run the mesh simplification with the Garland cost function and then apply the noisy cost function detecting noisy data points)
\end{enumerate}


\nocite{*}
\bibliography{quellen_summary_milan_sept}
\bibliographystyle{unsrt}

\end{document}
