A = [ 530474  0
523213  0.0136895
516346  0.0266342
509809  0.0389565
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
