% comparison Taubin and Laplacian smoothing with different techniques

clear all; close all; clc

it_L = [0:3];
V_L = [
530474  0
523213  0.0136895
516346  0.0266342
509809  0.0389565
 ];
Q_L = [
0.718774
0.726288
0.733029
0.739143
 ];


it_TUP = [0:12];
V_TUP = [
530474  0
527747  0.00514191
525146  0.010045
522659  0.0147326
520275  0.0192265
517984  0.0235458
515777  0.0277061
513647  0.031721
511589  0.0356017
509596  0.0393581
507665  0.0429986
505791  0.0465307
503971  0.049961
 ];
Q_TUP = [
0.718774
0.721038
0.723752
0.726593
0.72929
0.731778
0.734068
0.736186
0.738156
0.74
0.741732
0.743367
0.744916
 ];

V_TUPAQWS = [
530474  0
527747  0.00514191
525146  0.010045
522659  0.0147326
520275  0.0192265
517984  0.0235458
515777  0.0277061
513647  0.031721
511589  0.0356017
509596  0.0393581
507665  0.0429986
505791  0.0465307
503977  0.0499499
 ];
Q_TUPAQWS = [
0.718774
0.721038
0.723752
0.726593
0.72929
0.731778
0.734068
0.736186
0.738156
0.74
0.741732
0.756804
0.763865
 ];

figure(1)
plot(it_L,Q_L,'>-','MarkerSize',15,'LineWidth',3); hold
plot(it_TUP,Q_TUP,'o-','MarkerSize',15,'LineWidth',3);
plot(it_TUP,Q_TUPAQWS,'*-','MarkerSize',12,'LineWidth',3);
xlabel('iterations')
ylabel('q_{mean}')
legend({'Laplacian+HC','Taubin','Taubin with edge-flipping'},'Location','southeast')
set(gca,'FontSize',20)

figure(2)
plot(it_L,V_L(:,2),'>-','MarkerSize',15,'LineWidth',3); hold
plot(it_TUP,V_TUP(:,2),'o-','MarkerSize',15,'LineWidth',3);
plot(it_TUP,V_TUPAQWS(:,2),'*-','MarkerSize',12,'LineWidth',3);
xlabel('iterations')
ylabel('volume loss')
legend({'Laplacian+HC','Taubin','Taubin with edge-flipping'},'Location','southeast')
set(gca,'FontSize',20)