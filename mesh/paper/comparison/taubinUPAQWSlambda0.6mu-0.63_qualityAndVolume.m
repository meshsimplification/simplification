iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
 ];
V = [
530474  0
527747  0.00514191
525146  0.010045
522659  0.0147326
520275  0.0192265
517984  0.0235458
515777  0.0277061
513647  0.031721
511589  0.0356017
509596  0.0393581
507665  0.0429986
505791  0.0465307
503977  0.0499499
 ];
Q = [
0.718774
0.721038
0.723752
0.726593
0.72929
0.731778
0.734068
0.736186
0.738156
0.74
0.741732
0.756804
0.763865
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
