A = [ 530474  0
523422  0.0132954
515458  0.0283085
506498  0.0451977
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
