A = [ 530474  0
523456  0.0132311
515524  0.0281835
506593  0.04502
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
