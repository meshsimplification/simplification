A = [ 530474  0
523318  0.0134907
515257  0.0286863
506210  0.045741
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
