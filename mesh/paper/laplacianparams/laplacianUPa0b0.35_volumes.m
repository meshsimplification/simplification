A = [ 530474  0
523213  0.0136895
516670  0.0260235
510722  0.0372347
505275  0.0475045
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
