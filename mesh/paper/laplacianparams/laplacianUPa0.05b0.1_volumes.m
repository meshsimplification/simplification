A = [ 530474  0
523387  0.0133601
516637  0.0260844
510177  0.038263
503966  0.0499704
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
