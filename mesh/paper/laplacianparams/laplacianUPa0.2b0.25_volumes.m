A = [ 530474  0
523283  0.0135566
515507  0.0282155
507107  0.0440509
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
