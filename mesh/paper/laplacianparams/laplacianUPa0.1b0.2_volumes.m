A = [ 530474  0
523318  0.0134907
516203  0.0269034
509105  0.0402836
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
