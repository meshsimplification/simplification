A = [ 530474  0
523318  0.0134907
515887  0.027499
508162  0.0420617
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
