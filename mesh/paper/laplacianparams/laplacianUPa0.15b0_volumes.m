A = [ 530474  0
523456  0.0132311
516138  0.0270251
508503  0.0414186
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
