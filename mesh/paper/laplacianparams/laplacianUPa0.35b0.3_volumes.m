A = [ 530474  0
523248  0.0136228
514488  0.0301363
503962  0.0499788
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
