A = [ 530474  0
523456  0.0132311
516447  0.0264441
509426  0.0396783
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
