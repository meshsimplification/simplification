A = [ 530474  0
523105  0.0138917
516510  0.0263248
510540  0.037578
505085  0.0478617
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
