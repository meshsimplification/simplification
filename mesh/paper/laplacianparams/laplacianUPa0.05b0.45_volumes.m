A = [ 530474  0
523141  0.0138239
516233  0.0268461
509674  0.0392104
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
