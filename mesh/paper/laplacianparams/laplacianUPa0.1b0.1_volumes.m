A = [ 530474  0
523387  0.0133601
516325  0.0266736
509266  0.0399799
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
