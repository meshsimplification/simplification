A = [ 530474  0
523105  0.0138917
515848  0.0275728
508656  0.0411304
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
