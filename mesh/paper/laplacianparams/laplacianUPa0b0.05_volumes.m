A = [ 530474  0
523422  0.0132954
517008  0.0253862
511136  0.0364551
505725  0.0466553
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
