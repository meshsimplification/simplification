A = [ 530474  0
523353  0.0134252
515324  0.0285601
506306  0.0455596
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
