A = [ 530474  0
523456  0.0132311
514912  0.0293371
504600  0.0487753
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
