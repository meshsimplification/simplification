A = [ 530474  0
523069  0.0139598
515127  0.0289317
506597  0.0450116
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
