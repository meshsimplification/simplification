A = [ 530474  0
523353  0.0134252
515012  0.0291481
505304  0.0474481
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
