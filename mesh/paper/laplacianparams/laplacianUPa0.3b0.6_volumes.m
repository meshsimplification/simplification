A = [ 530474  0
523033  0.0140284
514399  0.0303036
504425  0.0491063
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
