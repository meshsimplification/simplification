A = [ 530474  0
523456  0.0132311
515218  0.0287609
505606  0.0468789
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
