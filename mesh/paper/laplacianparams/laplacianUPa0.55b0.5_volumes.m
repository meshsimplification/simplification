A = [ 530474  0
523105  0.0138917
512906  0.033119
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
