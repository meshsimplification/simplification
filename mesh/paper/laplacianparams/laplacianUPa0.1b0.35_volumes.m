A = [ 530474  0
523213  0.0136895
516022  0.0272435
508872  0.0407226
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
