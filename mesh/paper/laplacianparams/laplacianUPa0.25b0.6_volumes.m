A = [ 530474  0
523033  0.0140284
514732  0.0296759
505482  0.047113
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
