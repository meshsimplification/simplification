A = [ 530474  0
523422  0.0132954
516386  0.0265587
509347  0.0398279
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
