A = [ 530474  0
523213  0.0136895
514737  0.0296674
504902  0.0482075
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
