A = [ 530474  0
523387  0.0133601
515081  0.0290184
505406  0.0472566
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
