A = [ 530474  0
523248  0.0136228
516082  0.0271311
508948  0.0405794
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
