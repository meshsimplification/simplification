A = [ 530474  0
523456  0.0132311
516755  0.025862
510327  0.0379795
504126  0.0496697
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
