A = [ 530474  0
523387  0.0133601
516013  0.0272617
508333  0.0417387
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
