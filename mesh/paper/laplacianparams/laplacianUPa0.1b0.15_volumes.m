A = [ 530474  0
523353  0.0134252
516264  0.0267887
509185  0.0401323
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
