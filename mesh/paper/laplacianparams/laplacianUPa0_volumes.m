A = [ 530474  0
523456  0.0132311
517065  0.0252786
511206  0.0363233
505792  0.0465282
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
