A = [ 530474  0
523033  0.0140284
516409  0.0265155
510429  0.0377875
504971  0.0480769
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
