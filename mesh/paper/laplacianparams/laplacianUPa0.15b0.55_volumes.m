A = [ 530474  0
523069  0.0139598
515459  0.0283062
507604  0.0431137
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
