A = [ 530474  0
523177  0.0137565
516615  0.0261257
510660  0.0373532
505208  0.0476291
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
