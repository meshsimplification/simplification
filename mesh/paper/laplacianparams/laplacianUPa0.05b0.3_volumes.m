A = [ 530474  0
523248  0.0136228
516403  0.0265261
509879  0.0388237
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
