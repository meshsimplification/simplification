A = [ 530474  0
523283  0.0135566
515824  0.0276172
508077  0.0422211
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
