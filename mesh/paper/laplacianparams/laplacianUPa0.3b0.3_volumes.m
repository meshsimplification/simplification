A = [ 530474  0
523248  0.0136228
514805  0.0295379
505001  0.0480201
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
