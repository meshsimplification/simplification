A = [ 530474  0
523213  0.0136895
515057  0.0290634
505926  0.0462756
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
