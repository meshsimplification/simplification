A = [ 530474  0
523141  0.0138239
516562  0.0262262
510599  0.0374677
505145  0.0477479
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
