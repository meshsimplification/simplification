A = [ 530474  0
523248  0.0136228
516725  0.0259197
510787  0.0371121
505344  0.0473737
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
