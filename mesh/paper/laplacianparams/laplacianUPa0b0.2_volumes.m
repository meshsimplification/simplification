A = [ 530474  0
523318  0.0134907
516837  0.0257084
510923  0.0368557
505493  0.0470936
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
