A = [ 530474  0
523353  0.0134252
514701  0.0297349
504283  0.0493744
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
