A = [ 530474  0
523177  0.0137565
515963  0.0273548
508798  0.0408623
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
