A = [ 530474  0
523248  0.0136228
515124  0.0289382
506020  0.0460996
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
