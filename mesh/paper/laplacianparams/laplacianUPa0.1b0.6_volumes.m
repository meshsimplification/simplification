A = [ 530474  0
523033  0.0140284
515736  0.027784
508521  0.0413843
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
