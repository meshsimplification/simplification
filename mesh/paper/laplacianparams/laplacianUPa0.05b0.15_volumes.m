A = [ 530474  0
523353  0.0134252
516578  0.0261956
510101  0.0384061
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
