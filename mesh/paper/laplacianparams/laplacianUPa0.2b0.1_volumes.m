A = [ 530474  0
523387  0.0133601
515702  0.0278485
507378  0.0435385
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
