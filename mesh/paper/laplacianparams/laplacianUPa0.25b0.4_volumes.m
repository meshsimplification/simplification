A = [ 530474  0
523177  0.0137565
514991  0.029188
505834  0.046449
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
