A = [ 530474  0
523283  0.0135566
515190  0.0288124
506114  0.0459212
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
