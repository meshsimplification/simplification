beta=0


Quality summary
 mean quality: 0.752136
  max quality: 1
  min quality: 1.28008e-05

beta=0.05


Quality summary
 mean quality: 0.750468
  max quality: 1
  min quality: 1.6057e-06

beta=0.1


Quality summary
 mean quality: 0.748685
  max quality: 0.999999
  min quality: 2.44309e-05

beta=0.15


Quality summary
 mean quality: 0.746998
  max quality: 0.999997
  min quality: 9.63758e-06

beta=0.2


Quality summary
 mean quality: 0.74557
  max quality: 0.999998
  min quality: 2.33895e-05

beta=0.25


Quality summary
 mean quality: 0.744515
  max quality: 0.999999
  min quality: 1.23407e-05

beta=0.3


Quality summary
 mean quality: 0.743985
  max quality: 1
  min quality: 6.68366e-05

beta=0.35


Quality summary
 mean quality: 0.743947
  max quality: 1
  min quality: 1.14932e-05

beta=0.4


Quality summary
 mean quality: 0.744111
  max quality: 0.999999
  min quality: 1.62281e-05

beta=0.45


Quality summary
 mean quality: 0.744405
  max quality: 0.999999
  min quality: 2.22975e-05

beta=0.5


Quality summary
 mean quality: 0.744783
  max quality: 0.999997
  min quality: 5.22317e-07

beta=0.55


Quality summary
 mean quality: 0.745183
  max quality: 0.999999
  min quality: 2.12646e-05

beta=0.6


Quality summary
 mean quality: 0.745559
  max quality: 0.999999
  min quality: 1.24667e-05

