beta=0


Quality summary
 mean quality: 0.740108
  max quality: 0.999999
  min quality: 1.29086e-05

beta=0.05


Quality summary
 mean quality: 0.739144
  max quality: 1
  min quality: 7.0581e-06

beta=0.1


Quality summary
 mean quality: 0.738207
  max quality: 1
  min quality: 6.47884e-05

beta=0.15


Quality summary
 mean quality: 0.737326
  max quality: 0.999999
  min quality: 2.24421e-05

beta=0.2


Quality summary
 mean quality: 0.736561
  max quality: 0.999999
  min quality: 2.72361e-05

beta=0.25


Quality summary
 mean quality: 0.735984
  max quality: 1
  min quality: 1.0429e-05

beta=0.3


Quality summary
 mean quality: 0.735705
  max quality: 0.999998
  min quality: 3.0244e-06

beta=0.35


Quality summary
 mean quality: 0.735658
  max quality: 0.999999
  min quality: 1.13319e-05

beta=0.4


Quality summary
 mean quality: 0.735695
  max quality: 1
  min quality: 5.34588e-06

beta=0.45


Quality summary
 mean quality: 0.735807
  max quality: 0.999998
  min quality: 1.22594e-05

beta=0.5


Quality summary
 mean quality: 0.736027
  max quality: 0.999999
  min quality: 9.43833e-06

beta=0.55


Quality summary
 mean quality: 0.736346
  max quality: 0.999999
  min quality: 1.26781e-06

beta=0.6


Quality summary
 mean quality: 0.736741
  max quality: 1
  min quality: 3.3806e-05

