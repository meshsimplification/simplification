A = [ 530474  0
523033  0.0140284
515400  0.0284161
507531  0.0432507
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
