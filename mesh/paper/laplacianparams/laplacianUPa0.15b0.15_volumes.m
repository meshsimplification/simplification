A = [ 530474  0
523353  0.0134252
515950  0.0273804
508247  0.0419006
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
