A = [ 530474  0
523283  0.0135566
516142  0.0270176
509026  0.0404329
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
