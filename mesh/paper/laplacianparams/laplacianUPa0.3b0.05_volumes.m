A = [ 530474  0
523422  0.0132954
515149  0.0288893
505507  0.0470663
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
