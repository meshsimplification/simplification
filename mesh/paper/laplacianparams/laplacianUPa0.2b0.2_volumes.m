A = [ 530474  0
523318  0.0134907
515572  0.0280933
507197  0.0438812
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
