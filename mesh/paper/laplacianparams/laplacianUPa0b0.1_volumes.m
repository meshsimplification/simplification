A = [ 530474  0
523387  0.0133601
516951  0.0254939
511065  0.0365892
505649  0.0467982
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
