A = [ 530474  0
523283  0.0135566
516780  0.0258146
510855  0.0369856
505417  0.0472365
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
