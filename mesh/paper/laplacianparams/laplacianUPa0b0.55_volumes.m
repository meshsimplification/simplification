A = [ 530474  0
523069  0.0139598
516459  0.0264212
510484  0.0376846
505027  0.0479711
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
