A = [ 530474  0
523141  0.0138239
514925  0.0293116
505744  0.0466195
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
