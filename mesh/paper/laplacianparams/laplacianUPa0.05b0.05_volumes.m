A = [ 530474  0
523422  0.0132954
516696  0.025973
510253  0.0381201
504050  0.0498126
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
