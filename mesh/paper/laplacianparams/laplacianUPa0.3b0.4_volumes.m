A = [ 530474  0
523177  0.0137565
514668  0.0297963
504803  0.0483926
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
