A = [ 530474  0
523283  0.0135566
514874  0.0294081
505102  0.0478307
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
