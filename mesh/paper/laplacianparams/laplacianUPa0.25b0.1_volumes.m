A = [ 530474  0
523387  0.0133601
515391  0.0284341
506403  0.0453781
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
