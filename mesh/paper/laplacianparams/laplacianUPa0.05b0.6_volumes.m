A = [ 530474  0
523033  0.0140284
516072  0.0271505
509487  0.0395628
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
