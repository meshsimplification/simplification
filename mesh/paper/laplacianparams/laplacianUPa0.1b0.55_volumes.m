A = [ 530474  0
523069  0.0139598
515791  0.0276793
508588  0.0412591
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
