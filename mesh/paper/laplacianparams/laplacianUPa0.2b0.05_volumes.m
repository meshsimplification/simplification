A = [ 530474  0
523422  0.0132954
515766  0.0277264
507469  0.0433677
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
