A = [ 530474  0
523248  0.0136228
515442  0.0283372
507018  0.0442185
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
