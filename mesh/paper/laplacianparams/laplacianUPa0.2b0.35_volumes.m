A = [ 530474  0
523213  0.0136895
515378  0.0284582
506930  0.0443834
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
