A = [ 530474  0
523141  0.0138239
515251  0.0286973
506760  0.044704
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
