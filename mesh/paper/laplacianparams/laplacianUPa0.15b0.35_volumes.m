A = [ 530474  0
523213  0.0136895
515700  0.0278515
507912  0.042532
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
