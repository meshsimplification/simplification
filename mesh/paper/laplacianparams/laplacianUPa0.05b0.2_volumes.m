A = [ 530474  0
523318  0.0134907
516520  0.0263065
510026  0.0385479
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
