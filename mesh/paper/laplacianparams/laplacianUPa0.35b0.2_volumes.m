A = [ 530474  0
523318  0.0134907
514630  0.0298686
504175  0.0495765
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
