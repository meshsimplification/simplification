A = [ 530474  0
523456  0.0132311
515831  0.0276049
507558  0.0431995
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
