A = [ 530474  0
523105  0.0138917
515189  0.0288152
506678  0.0448594
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
