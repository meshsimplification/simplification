A = [ 530474  0
523141  0.0138239
514600  0.0299245
504707  0.0485752
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
