A = [ 530474  0
523248  0.0136228
515762  0.0277348
507994  0.042378
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
