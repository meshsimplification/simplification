A = [ 530474  0
523069  0.0139598
514796  0.0295557
505568  0.0469515
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
