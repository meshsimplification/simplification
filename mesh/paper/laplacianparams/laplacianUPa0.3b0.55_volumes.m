A = [ 530474  0
523069  0.0139598
514466  0.0301783
504517  0.048932
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
