A = [ 530474  0
523177  0.0137565
515639  0.0279672
507832  0.0426826
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
