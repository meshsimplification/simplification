A = [ 530474  0
523283  0.0135566
516461  0.0264168
509952  0.0386874
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
