iter = [
0
1
2
3
4
5
6
7
8
9
10
 ];
V = [
530474  0
527256  0.00606724
524215  0.0118002
521329  0.0172396
518581  0.022421
515954  0.0273735
513435  0.0321203
511016  0.0366804
508688  0.0410698
506443  0.0453019
504275  0.0493884
 ];
Q = [
0.718774
0.721485
0.724838
0.72817
0.731206
0.733948
0.736444
0.738738
0.740862
0.742841
0.744694
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
