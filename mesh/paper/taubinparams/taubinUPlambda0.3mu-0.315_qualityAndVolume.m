iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
 ];
V = [
530474  0
529848  0.00118073
529228  0.00234919
528615  0.00350558
528008  0.00465009
527407  0.00578294
526812  0.00690436
526223  0.00801456
525640  0.00911379
525062  0.0102022
524491  0.0112802
523924  0.0123478
523363  0.0134053
522808  0.014453
522257  0.015491
521711  0.0165195
521171  0.0175387
520635  0.0185489
520104  0.0195501
519577  0.0205427
519055  0.0215267
518538  0.0225023
518024  0.0234697
517515  0.0244291
517011  0.0253805
516510  0.0263243
516014  0.0272604
515521  0.028189
515032  0.0291103
514547  0.0300244
514066  0.0309314
513589  0.0318315
513115  0.0327247
512645  0.0336112
512178  0.0344911
511715  0.0353645
511255  0.0362315
510798  0.0370922
510345  0.0379466
509895  0.038795
509448  0.0396374
509004  0.0404738
508564  0.0413045
508126  0.0421293
507691  0.0429486
507260  0.0437622
506831  0.0445703
506405  0.045373
505982  0.0461704
505562  0.0469625
505145  0.0477494
504730  0.0485311
504318  0.0493078
 ];
Q = [
0.718774
0.71929
0.719811
0.720343
0.720887
0.721446
0.722019
0.722604
0.723201
0.723806
0.724418
0.725033
0.725648
0.726262
0.726871
0.727476
0.728074
0.728665
0.729247
0.729821
0.730387
0.730943
0.73149
0.732029
0.732558
0.733078
0.73359
0.734093
0.734588
0.735074
0.735553
0.736024
0.736488
0.736944
0.737393
0.737836
0.738272
0.738701
0.739124
0.739541
0.739952
0.740357
0.740757
0.741151
0.74154
0.741924
0.742303
0.742677
0.743046
0.743411
0.743771
0.744127
0.744478
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
