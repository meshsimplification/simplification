iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
 ];
V = [
530474  0
528198  0.00429155
526009  0.00841758
523901  0.0123908
521868  0.0162239
519903  0.0199283
518001  0.0235141
516157  0.0269901
514367  0.0303641
512628  0.0336427
510936  0.036832
509289  0.0399372
507684  0.0429629
506119  0.0459135
504591  0.0487928
 ];
Q = [
0.718774
0.720648
0.722807
0.725137
0.727449
0.729646
0.731704
0.733628
0.73543
0.737125
0.738724
0.740239
0.741679
0.743051
0.744361
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
