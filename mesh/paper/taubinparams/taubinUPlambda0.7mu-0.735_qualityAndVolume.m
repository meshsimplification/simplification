iter = [
0
1
2
3
4
5
6
7
8
 ];
V = [
530474  0
526726  0.00706697
523219  0.0136779
519917  0.0199011
516793  0.0257911
513823  0.0313897
510991  0.0367292
508282  0.0418358
505685  0.0467305
 ];
Q = [
0.718774
0.722007
0.726053
0.729825
0.733155
0.736126
0.738813
0.741269
0.743534
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
