% explore parameter space for taubin smoothing

clear all; close all; clc

l = [0.7:-0.05:0.2];
lambda = fliplr(l);
%mu = [-0.63, -0.5775, -0.525, -0.4725, -0.42, -0.3675, -0.315, -0.2625, -0.21];

q = [0.743534, 0.744694, 0.744916, 0.744361, 0.744203, 0.744796, 0.744587, 0.74441, 0.744478, 0.744728, 0.744687];
quality = fliplr(q);

it = [9, 11, 13, 15, 18, 23, 29, 38, 53, 80, 132];
iterations = fliplr(it);

v = [0.0467305, 0.0493884, 0.049961, 0.0487928, 0.0485069, 0.0498819, 0.049467, 0.0491182, 0.0493078, 0.0498947, 0.0498285];
volume = fliplr(v);

% contour(X,Y,quality); hold
% colormap jet
% xlabel('\alpha')
% ylabel('\beta')
% set(gca, 'Fontsize',18)
% hcb=colorbar;
% title(hcb,'q_{mean}')
% x = [0:0.01:0.6];
% y = 1.1.*x;
% plot(x,y,'r-')


fig4 = figure(1);
set(fig4,'Name','mesh quality','NumberTitle','off')
set(fig4,'Position',[1078 197 691 497])
line(lambda,quality,'Color', [0.02, 0.38, 0.683],'LineWidth',3,'Marker','>','MarkerSize',15); 
ax1 = gca; % current axes
ax1.YColor = [0.02, 0.38, 0.683];
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,'YAxisLocation','right','Color','none');
ax2.YColor = [0.734 0.118 0.094];
line(lambda,iterations,'Parent',ax2,'Color',[0.734 0.118 0.094],'LineWidth',3,'Marker','*','MarkerSize',15)
ylabel(ax1,'q_{mean}','Fontsize',20) % left y-axis
ylabel(ax2,'iteration number','Fontsize',20) 
set(ax1,'Fontsize',20); set(ax2,'Fontsize',20)
xlabel(ax1,'\lambda')

fig4 = figure(2);
set(fig4,'Name','volume loss','NumberTitle','off')
set(fig4,'Position',[1078 197 691 497])
line(lambda,volume,'Color', [0.02, 0.38, 0.683],'LineWidth',3,'Marker','>','MarkerSize',15); 
ax1 = gca; % current axes
ax1.YColor = [0.02, 0.38, 0.683];
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,'YAxisLocation','right','Color','none');
ax2.YColor = [0.734 0.118 0.094];
line(lambda,iterations,'Parent',ax2,'Color',[0.734 0.118 0.094],'LineWidth',3,'Marker','*','MarkerSize',15)
ylabel(ax1,'volume loss','Fontsize',20) % left y-axis
ylabel(ax2,'iteration number','Fontsize',20) 
set(ax1,'Fontsize',20); set(ax2,'Fontsize',20)
xlabel(ax1,'\lambda')