A = [ 530474  0
526726  0.00706697
523219  0.0136779
519917  0.0199011
516793  0.0257911
513823  0.0313897
510991  0.0367292
508282  0.0418358
505685  0.0467305
 ];
figure(1);                               
plot(A(:,1));                            
title('Values of the volumes');          
figure(2);                               
plot(A(:,2));                            
title('Values of the relative error');   
