iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
 ];
V = [
530474  0
528980  0.00281774
527522  0.00556482
526101  0.00824436
524714  0.0108597
523359  0.0134142
522034  0.015911
520739  0.0183532
519471  0.0207434
518229  0.0230842
517012  0.0253781
515819  0.0276271
514649  0.0298332
513500  0.0319984
512372  0.0341242
511265  0.0362122
510176  0.0382639
509107  0.0402807
508055  0.0422638
507020  0.0442144
506002  0.0461336
505000  0.0480225
504013  0.0498819
 ];
Q = [
0.718774
0.719999
0.721309
0.722718
0.724198
0.725701
0.727188
0.728634
0.730029
0.731369
0.732653
0.733884
0.735065
0.7362
0.737291
0.738343
0.739358
0.740338
0.741286
0.742204
0.743094
0.743957
0.744796
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
