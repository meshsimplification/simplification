iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
 ];
V = [
530474  0
528609  0.00351667
526802  0.00692275
525051  0.0102248
523350  0.0134295
521699  0.0165434
520092  0.0195725
518527  0.0225221
517002  0.0253971
515514  0.0282017
514062  0.03094
512642  0.0336154
511255  0.036231
509897  0.0387899
508569  0.0412946
507268  0.0437475
505993  0.0461509
504743  0.0485069
 ];
Q = [
0.718774
0.720304
0.721997
0.723839
0.725734
0.727599
0.72939
0.731093
0.732706
0.734235
0.735687
0.737069
0.738387
0.739648
0.740855
0.742014
0.743129
0.744203
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
