iter = [
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
 ];
V = [
530474  0
529310  0.0021952
528168  0.0043477
527048  0.00645887
525949  0.00853021
524871  0.0105633
523812  0.0125596
522772  0.0145206
521749  0.0164477
520744  0.0183423
519756  0.0202056
518783  0.0220388
517826  0.0238431
516884  0.0256195
515956  0.0273689
515042  0.0290923
514141  0.0307907
513253  0.0324647
512377  0.0341151
511514  0.0357428
510662  0.0373483
509822  0.0389323
508993  0.0404955
508174  0.0420384
507366  0.0435615
506568  0.0450655
505781  0.0465507
505002  0.0480178
504233  0.049467
 ];
Q = [
0.718774
0.719729
0.720724
0.721775
0.722878
0.72402
0.725179
0.726336
0.727478
0.728595
0.729682
0.730737
0.731759
0.732749
0.733707
0.734635
0.735535
0.736407
0.737254
0.738077
0.738877
0.739655
0.740413
0.741152
0.741872
0.742575
0.743261
0.743932
0.744587
 ];
figure(1);                                                                
plot(iter, V(:,1), '-r','LineWidth',2');                                  
title('Values of the volumes');                                           
tmpLegend = legend('volume values');                                      
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(2);                                                                
plot(iter, V(:,2), '-r','LineWidth',2');                                  
title('Values of the relative error');                                    
tmpLegend = legend('relative error','Location','southeast');              
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
figure(3);                                                                
plot(iter, Q, '-b','LineWidth',2');                                       
title('Mean quality');                                                    
tmpLegend = legend('$q_{\textrm{mean}}$','Location','southeast');        
set(tmpLegend,'Interpreter','latex');                                     
set(tmpLegend,'FontSize', 12);                                            
