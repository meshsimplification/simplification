#include <iostream>
#include "meshSimplification.h"

using namespace geometry;
using namespace std;

/* Method to project the points */
void computeCurvatureError(mesh2d<Triangle> & surf);
void projectOnGeometry(mesh2d<Triangle> * surf);

int main(int argc, char * argv[])
{	
    // variabili in uso 
    mesh2d<Triangle>	                 surf;
    createFile                           file;
    downloadMesh                         down;
    
    if(argc!=3)
    {
        cout << "Main to make a convergence analysis on the curvature of a sphere with radius 0.5, wrong number of inputs\n";
        cout << "1) initial sphere mesh .vtk";
        cout << "2) how many steps\n";
        return(1);
        
    }
    
    string filename(argv[1]);
    down.fileFromVTK(filename, &surf);
    
    // set for the isotropic quality
    isotropicQuality2d<Triangle> iso;
    iso.setMeshPointer(&surf);
    
    UInt numberOfSteps = atoi(argv[2]);
    UInt actualStep = 0;
    do
    {
        computeCurvatureError(surf);
        iso.refineUniform();
        projectOnGeometry(&surf); 
        ++actualStep;
        
    }while(actualStep<numberOfSteps);
}

//
// Implementation of the function 
//
void projectOnGeometry(mesh2d<Triangle> * surf)
{
    for(UInt i=0; i<surf->getNumNodes(); ++i)
    {   
        Real x = surf->getNode(i).getX();
        Real y = surf->getNode(i).getY();
        Real z = surf->getNode(i).getZ();                
        
        Real radius = sqrt(x*x+y*y+z*z);

        Real dirX = x/radius;
        Real dirY = y/radius;
        Real dirZ = z/radius;
        
        surf->getNodePointer(i)->setX(dirX*(1./2.));
        surf->getNodePointer(i)->setY(dirY*(1./2.));
        surf->getNodePointer(i)->setZ(dirZ*(1./2.));
    }
}

REAL computeErrorMeanCurvature(point pointWhereEvaluate, REAL approximation)
{
    Real curvature = 2.;
    REAL error = fabs(curvature - approximation)/curvature;    
    return(error);
}


REAL computeErrorMeanCurvatureSmallL2(point pointWhereEvaluate, REAL approximation)
{
    Real curvature = 2.;
    REAL error = (curvature - approximation)*(curvature - approximation);
    return(error);
}

REAL computeErrorGaussianCurvature(point pointWhereEvaluate, REAL approximation)
{
    REAL curvature  = 4.;
    REAL error = fabs(curvature - approximation);
    return(error);
}

REAL computeErrorGaussianCurvatureSmallL2(point pointWhereEvaluate, REAL approximation)
{
    REAL curvature  = 4.;
    REAL error = (curvature - approximation)*(curvature - approximation);
    return(error);
}

void computeCurvatureError(mesh2d<Triangle> & surf)
{
    //-------------------------------------------------------------------------
    //                      compute the curvature 
    //-------------------------------------------------------------------------
    tricky2d<Triangle> trick;
    trick.setMeshPointer(&surf);
    
    std::vector<Real> nodeMeanCurvature(surf.getNumNodes());
    std::vector<Real> errorMean(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getMeanNodeCurvature(i);
        errorMean[i] = computeErrorMeanCurvature(surf.getNode(i), nodeMeanCurvature[i]);
    }
    std::cout << "Mean curvature done!                                          \n";
    
    std::vector<Real> nodeGaussianCurvature(surf.getNumNodes());
    std::vector<Real> errorGaussian(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeGaussianCurvature[i] = trick.getGaussianNodeCurvature(i);
        errorGaussian[i] = computeErrorGaussianCurvature(surf.getNode(i), nodeGaussianCurvature[i]);
    }
    std::cout << "Gaussian curvature done!                                       \n";
    
    std::vector<Real> errorMeanSmallL2(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getMeanNodeCurvature(i);
        errorMeanSmallL2[i] = computeErrorMeanCurvatureSmallL2(surf.getNode(i), nodeMeanCurvature[i]);
    }
    REAL errorMeanSmallL2Value = std::accumulate(errorMeanSmallL2.begin(), errorMeanSmallL2.end(), 0.0);
    errorMeanSmallL2Value /= static_cast<Real>(surf.getNumNodes());
    errorMeanSmallL2Value = sqrt(errorMeanSmallL2Value);
    std::cout << "Mean l2 curvature done!                                          \n";
    
    std::vector<Real> errorGaussianSmallL2(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getGaussianNodeCurvature(i);
        errorGaussianSmallL2[i] = computeErrorGaussianCurvatureSmallL2(surf.getNode(i), nodeMeanCurvature[i]);
    }
    REAL errorGaussianSmallL2Value = std::accumulate(errorGaussianSmallL2.begin(), errorGaussianSmallL2.end(), 0.0);
    errorGaussianSmallL2Value /= static_cast<Real>(surf.getNumNodes());
    errorGaussianSmallL2Value = sqrt(errorGaussianSmallL2Value);
    std::cout << "Gaussian l2 curvature done!                                          \n";
    
    createFile  file;
    
    // plot the volumes 
    ostringstream curvatureMeanName,errorMeanName;
    curvatureMeanName << "sphera_" << surf.getNumElements() << "_mean_curvature.inp";
    errorMeanName << "sphera_" << surf.getNumElements() << "_mean_curvature_error.inp";
    cout << "writing file " << curvatureMeanName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureMeanName.str(), &surf, &nodeMeanCurvature);
    cout << "writing file " << errorMeanName.str() << endl;
    file.fileForParaviewNodePropriety(errorMeanName.str(), &surf, &errorMean);        
    
    ostringstream curvatureGaussName,errorGaussName;
    curvatureGaussName << "sphera_" << surf.getNumElements() << "_gauss_curvature.inp";
    errorGaussName << "sphera_" << surf.getNumElements() << "_gauss_curvature_error.inp";
    cout << "writing file " << curvatureGaussName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureGaussName.str(), &surf, &nodeGaussianCurvature);
    cout << "writing file " << errorGaussName.str() << endl;
    file.fileForParaviewNodePropriety(errorGaussName.str(), &surf, &errorGaussian);        
    
    std::cout << "\n\nError summary:\n";
    std::cout << "\t     mean curvature error: " << *std::max_element(errorMean.begin(), errorMean.end()) << std::endl;
    std::cout << "\t Gaussian curvature error: " << *std::max_element(errorGaussian.begin(), errorGaussian.end()) << std::endl;
    std::cout << "\t     mean small l^2 error: " << errorMeanSmallL2Value << std::endl;
    std::cout << "\t Gaussian small l^2 error: " << errorGaussianSmallL2Value << std::endl;
    std::cout << std::endl;
}




