#include <iostream>
#include "meshSimplification.h"

using namespace geometry;
using namespace std;

enum kindOfMesh{STRUCTUREDMESH, UNSTRUCTUREDMESH};

/* Method to project the points */
void createPerimeter(point pMax, point pMin, mesh1d<Line> * boundary);
void computeCurvatureError(mesh2d<Triangle> & surf);
void projectOnGeometry(mesh2d<Triangle> * surf);

int main(int argc, char * argv[])
{	
    // variabili in uso 
    point pMin(0.0,0.0,0.0),pMax(1.0,1.0,0.0);
    mesh1d<Line>                     boundary;
    mesh2d<Triangle>	                 surf;
    createFile                           file;
    downloadMesh                         down;
    triangle2dByShewchuk                 gene;
    
    // set the pointers 
    gene.setBelements(&boundary);
    gene.setElements(&surf);        

    if(argc!=2)
    {
        cout << "Main to create the mesh of the square [0,1]^2, wrong number of inputs\n";
        cout << "1) how many steps\n";
        return(1);
        
    }
    
    Real diameter = 0.025;
    createPerimeter(pMax, pMin, &boundary);
    gene.createMesh2d(diameter);
        
    // set for the isotropic quality
    isotropicQuality2d<Triangle> iso;
    iso.setMeshPointer(&surf);
    projectOnGeometry(&surf); 
    iso.standardImprove();
    
    UInt numberOfSteps = atoi(argv[1]);
    UInt actualStep = 0;
    do
    {
        computeCurvatureError(surf);
        iso.refineUniform();
        projectOnGeometry(&surf); 
        ++actualStep;
        
    }while(actualStep<numberOfSteps);
}

//
// Implementation of the function 
//
void createPerimeter(point pMax, point pMin, mesh1d<Line> * boundary)     
{
    boundary->getNodePointer()->resize(4);
    boundary->getNodePointer(0)->setX(pMin.getX());    boundary->getNodePointer(0)->setY(pMin.getY());
    boundary->getNodePointer(1)->setX(pMax.getX());    boundary->getNodePointer(1)->setY(pMin.getY());
    boundary->getNodePointer(2)->setX(pMax.getX());    boundary->getNodePointer(2)->setY(pMax.getY());
    boundary->getNodePointer(3)->setX(pMin.getX());    boundary->getNodePointer(3)->setY(pMax.getY());
    
    boundary->getElementPointer()->resize(4);
    boundary->getElementPointer(0)->setConnectedId(0,0);    boundary->getElementPointer(0)->setConnectedId(1,1);
    boundary->getElementPointer(1)->setConnectedId(0,1);    boundary->getElementPointer(1)->setConnectedId(1,2);
    boundary->getElementPointer(2)->setConnectedId(0,2);    boundary->getElementPointer(2)->setConnectedId(1,3);
    boundary->getElementPointer(3)->setConnectedId(0,3);    boundary->getElementPointer(3)->setConnectedId(1,0);
}

void projectOnGeometry(mesh2d<Triangle> * surf)
{
    for(UInt i=0; i<surf->getNumNodes(); ++i)
    {   
        Real x = surf->getNode(i).getX();
        Real y = surf->getNode(i).getY();        
        
        Real z = x*x+y*y;        
        
        surf->getNodePointer(i)->setZ(z);
    }
}

REAL computeErrorMeanCurvature(point pointWhereEvaluate, REAL approximation)
{
    Real curvature = approximation;
    // I do not chek the boundary 
    if(pointWhereEvaluate.getBoundary()==0)
    {
        Real x = pointWhereEvaluate.getX();
        Real y = pointWhereEvaluate.getY();
        
        curvature = (8.*x*x+8.*y*y+4.)/(2.*sqrt((4.*x*x+4.*y*y+1.)*(4.*x*x+4.*y*y+1.)*(4.*x*x+4.*y*y+1.)));
    }
    
    REAL error = fabs(curvature - approximation)/curvature;    
    return(error);
}

REAL computeErrorMeanCurvatureSmallL2(point pointWhereEvaluate, REAL approximation)
{
    Real curvature = approximation;
    // I do not chek the boundary 
    if(pointWhereEvaluate.getBoundary()==0)
    {
        Real x = pointWhereEvaluate.getX();
        Real y = pointWhereEvaluate.getY();
        
        curvature = (8.*x*x+8.*y*y+4.)/(2.*sqrt((4.*x*x+4.*y*y+1.)*(4.*x*x+4.*y*y+1.)*(4.*x*x+4.*y*y+1.)));
    }
    
    REAL error = (curvature - approximation)*(curvature - approximation);
    return(error);
}

REAL computeErrorGaussianCurvature(point pointWhereEvaluate, REAL approximation)
{
    REAL curvature  = approximation;
    // I do not chek the boundary 
    if(pointWhereEvaluate.getBoundary()==0)
    {
       Real x = pointWhereEvaluate.getX();
       Real y = pointWhereEvaluate.getY();
            
       curvature = 4. / ((1.+4.*x*x+4.*y*y)*(1.+4.*x*x+4.*y*y));
    }
    
    REAL error = fabs(curvature - approximation);
    if(fabs(curvature)>1e-10)
        error /= fabs(curvature);
    
    return(error);
}

REAL computeErrorGaussianCurvatureSmallL2(point pointWhereEvaluate, REAL approximation)
{
    REAL curvature  = approximation;
    // I do not chek the boundary 
    if(pointWhereEvaluate.getBoundary()==0)
    {
       Real x = pointWhereEvaluate.getX();
       Real y = pointWhereEvaluate.getY();
            
       curvature = 4. / ((1.+4.*x*x+4.*y*y)*(1.+4.*x*x+4.*y*y));
    }
    
    REAL error = (curvature - approximation)*(curvature - approximation);
    
    return(error);
}

void computeCurvatureError(mesh2d<Triangle> & surf)
{
    //-------------------------------------------------------------------------
    //                      compute the curvature 
    //-------------------------------------------------------------------------
    tricky2d<Triangle> trick;
    trick.setMeshPointer(&surf);
    
    std::vector<Real> nodeMeanCurvature(surf.getNumNodes());
    std::vector<Real> errorMean(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getMeanNodeCurvature(i);
        errorMean[i] = computeErrorMeanCurvature(surf.getNode(i), nodeMeanCurvature[i]);
    }
    std::cout << "Mean curvature done!                                          \n";
    
    std::vector<Real> nodeGaussianCurvature(surf.getNumNodes());
    std::vector<Real> errorGaussian(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeGaussianCurvature[i] = trick.getGaussianNodeCurvature(i);
        errorGaussian[i] = computeErrorGaussianCurvature(surf.getNode(i), nodeGaussianCurvature[i]);
    }
    std::cout << "Gaussian curvature done!                                       \n";
    
    std::vector<Real> errorMeanSmallL2(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getMeanNodeCurvature(i);
        errorMeanSmallL2[i] = computeErrorMeanCurvatureSmallL2(surf.getNode(i), nodeMeanCurvature[i]);
    }
    REAL errorMeanSmallL2Value = std::accumulate(errorMeanSmallL2.begin(), errorMeanSmallL2.end(), 0.0);
    errorMeanSmallL2Value /= static_cast<Real>(surf.getNumNodes());
    errorMeanSmallL2Value = sqrt(errorMeanSmallL2Value);
    std::cout << "Mean l2 curvature done!                                          \n";
    
    std::vector<Real> errorGaussianSmallL2(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getGaussianNodeCurvature(i);
        errorGaussianSmallL2[i] = computeErrorGaussianCurvatureSmallL2(surf.getNode(i), nodeMeanCurvature[i]);
    }
    REAL errorGaussianSmallL2Value = std::accumulate(errorGaussianSmallL2.begin(), errorGaussianSmallL2.end(), 0.0);
    errorGaussianSmallL2Value /= static_cast<Real>(surf.getNumNodes());
    errorGaussianSmallL2Value = sqrt(errorGaussianSmallL2Value);
    std::cout << "Gaussian l2 curvature done!                                          \n";
    
    createFile  file;
    
    // plot the volumes 
    ostringstream curvatureMeanName,errorMeanName;
    curvatureMeanName << "paraboloid_" << surf.getNumElements() << "_mean_curvature.inp";
    errorMeanName << "paraboloid_" << surf.getNumElements() << "_mean_curvature_error.inp";
    cout << "writing file " << curvatureMeanName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureMeanName.str(), &surf, &nodeMeanCurvature);
    cout << "writing file " << errorMeanName.str() << endl;
    file.fileForParaviewNodePropriety(errorMeanName.str(), &surf, &errorMean);        
    
    ostringstream curvatureGaussName,errorGaussName;
    curvatureGaussName << "paraboloid_" << surf.getNumElements() << "_gauss_curvature.inp";
    errorGaussName << "paraboloid_" << surf.getNumElements() << "_gauss_curvature_error.inp";
    cout << "writing file " << curvatureGaussName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureGaussName.str(), &surf, &nodeGaussianCurvature);
    cout << "writing file " << errorGaussName.str() << endl;
    file.fileForParaviewNodePropriety(errorGaussName.str(), &surf, &errorGaussian);        
    
    std::cout << "\n\nError summary:\n";
    std::cout << "\t     mean curvature error: " << *std::max_element(errorMean.begin(), errorMean.end()) << std::endl;
    std::cout << "\t Gaussian curvature error: " << *std::max_element(errorGaussian.begin(), errorGaussian.end()) << std::endl;
    std::cout << "\t     mean small l^2 error: " << errorMeanSmallL2Value << std::endl;
    std::cout << "\t Gaussian small l^2 error: " << errorGaussianSmallL2Value << std::endl;
    std::cout << std::endl;
}




