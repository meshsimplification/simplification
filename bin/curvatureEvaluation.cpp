#include <iostream>

#include "meshSimplification.h"


using namespace geometry;
using namespace std;

int main(int argc, char * argv[])
{	
    // variabili in uso 
    mesh2d<Triangle> surf;
    createFile  file;
    downloadMesh down;
    
    // Check of the input
    if(argc!=4)
    {
        cout << "Wrong input for this function" << endl;
        cout << "1) file extension 1=.inp, 2=.vtk\n";
        cout << "2) name of the file\n";
        cout << "3) name of the output\n";
        exit(1);
    }
    
    //  save the parameters
    UInt extension = atoi(argv[1]);
    string filename(argv[2]);
    string outputName(argv[3]);
    
    //-------------------------------------------------------------------------
    //                           get the file 
    //-------------------------------------------------------------------------
    if(extension==1)
    {
        down.fileFromParaview(filename, &surf);
    }
    else if(extension==2)
    {
        down.fileFromVTK(filename, &surf);
    }
    else 
    {
        cout << "!! !! Unknown file extension " << extension << std::endl;
        exit(1);
    }
        
    // print the cost function
    ostringstream initailName;
    initailName << outputName << "_initial.inp";
    cout << "writing file " << initailName.str() << endl;
    file.fileForParaview(initailName.str(), &surf);
    
    //-------------------------------------------------------------------------
    //                      compute the curvature 
    //-------------------------------------------------------------------------
    tricky2d<Triangle> trick;
    trick.setMeshPointer(&surf);
    
    std::vector<Real> nodeMeanCurvature(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getMeanNodeCurvature(i);
    }
    std::cout << "Mean curvature done!                                          \n";
    
    std::vector<Real> nodeGaussianCurvature(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeGaussianCurvature[i] = trick.getGaussianNodeCurvature(i);
    }
    std::cout << "Gaussian curvature done!                                       \n";
    
    // plot the volumes 
    ostringstream curvatureMeanName;
    curvatureMeanName << outputName << "_mean_curvature.inp";
    cout << "writing file " << curvatureMeanName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureMeanName.str(), &surf, &nodeMeanCurvature);
    
    ostringstream curvatureGaussName;
    curvatureGaussName << outputName << "_gauss_curvature.inp";
    cout << "writing file " << curvatureGaussName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureGaussName.str(), &surf, &nodeGaussianCurvature);
    
}
