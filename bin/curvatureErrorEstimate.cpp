#include <iostream>

#include "meshSimplification.h"


using namespace geometry;
using namespace std;

// internal function 
REAL computeErrorMeanCurvature(UInt example, point pointWhereEvaluate, REAL approximation);
REAL computeErrorGaussianCurvature(UInt example, point pointWhereEvaluate, REAL approximation);

int main(int argc, char * argv[])
{	
    // variabili in uso 
    mesh2d<Triangle> surf;
    createFile  file;
    downloadMesh down;
    
    // Check of the input
    if(argc!=5)
    {
        cout << "Wrong input for this function" << endl;
        cout << "1) file extension 1=.inp, 2=.vtk\n";
        cout << "2) name of the file\n";
        cout << "3) name of the output\n";
        cout << "4) example type 1 = sphere radius 0.5, 2 = paraboloid, 3 = sin function\n";
        exit(1);
    }
    
    //  save the parameters
    UInt extension = atoi(argv[1]);
    string filename(argv[2]);
    string outputName(argv[3]);
    UInt example = atoi(argv[4]);
    
    //-------------------------------------------------------------------------
    //                           get the file 
    //-------------------------------------------------------------------------
    if(extension==1)
    {
        down.fileFromParaview(filename, &surf);
    }
    else if(extension==2)
    {
        down.fileFromVTK(filename, &surf);
    }
    else 
    {
        cout << "!! !! Unknown file extension " << extension << std::endl;
        exit(1);
    }
        
    // print the cost function
    ostringstream initailName;
    initailName << outputName << "_initial.inp";
    cout << "writing file " << initailName.str() << endl;
    file.fileForParaview(initailName.str(), &surf);
    
    //-------------------------------------------------------------------------
    //                      compute the curvature 
    //-------------------------------------------------------------------------
    tricky2d<Triangle> trick;
    trick.setMeshPointer(&surf);
    
    std::vector<Real> nodeMeanCurvature(surf.getNumNodes());
    std::vector<Real> errorMean(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeMeanCurvature[i] = trick.getMeanNodeCurvature(i);
        errorMean[i] = computeErrorMeanCurvature(example, surf.getNode(i), nodeMeanCurvature[i]);
    }
    std::cout << "Mean curvature done!                                          \n";
    
    std::vector<Real> nodeGaussianCurvature(surf.getNumNodes());
    std::vector<Real> errorGaussian(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        std::cout << "computing " << i << " over " << surf.getNumNodes() << "                                \r";
        nodeGaussianCurvature[i] = trick.getGaussianNodeCurvature(i);
        errorGaussian[i] = computeErrorGaussianCurvature(example, surf.getNode(i), nodeGaussianCurvature[i]);
    }
    std::cout << "Gaussian curvature done!                                       \n";
    
    // plot the volumes 
    ostringstream curvatureMeanName,errorMeanName;
    curvatureMeanName << outputName << "_mean_curvature.inp";
    errorMeanName << outputName << "_mean_curvature_error.inp";
    cout << "writing file " << curvatureMeanName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureMeanName.str(), &surf, &nodeMeanCurvature);
    cout << "writing file " << errorMeanName.str() << endl;
    file.fileForParaviewNodePropriety(errorMeanName.str(), &surf, &errorMean);        
    
    ostringstream curvatureGaussName,errorGaussName;
    curvatureGaussName << outputName << "_gauss_curvature.inp";
    errorGaussName << outputName << "_gauss_curvature_error.inp";
    cout << "writing file " << curvatureGaussName.str() << endl;
    file.fileForParaviewNodePropriety(curvatureGaussName.str(), &surf, &nodeGaussianCurvature);
    cout << "writing file " << errorGaussName.str() << endl;
    file.fileForParaviewNodePropriety(errorGaussName.str(), &surf, &errorGaussian);        
    
    std::cout << "\n\nError summary:\n";
    std::cout << "\t     mean curvature error: " << *std::max_element(errorMean.begin(), errorMean.end()) << std::endl;
    std::cout << "\t Gaussian curvature error: " << *std::max_element(errorGaussian.begin(), errorGaussian.end()) << std::endl;
    std::cout << std::endl;
}

//----------------------------------------------------------------------------------------
//                    Implementation of the fucntion 
//----------------------------------------------------------------------------------------
REAL computeErrorMeanCurvature(UInt example, point pointWhereEvaluate, REAL approximation)
{
    Real curvature = approximation;
    if(example==1)
    {
        curvature = 2.;
    }
    else if(example==2)
    {
        // I do not chek the boundary 
        if(pointWhereEvaluate.getBoundary()==0)
        {
            Real x = pointWhereEvaluate.getX();
            Real y = pointWhereEvaluate.getY();
            
            curvature = (8.*x*x+8.*y*y+4.)/(2.*sqrt((4.*x*x+4.*y*y+1.)*(4.*x*x+4.*y*y+1.)*(4.*x*x+4.*y*y+1.)));
        }
    }
    else if(example==3)
    {
        // I do not chek the boundary 
        if(pointWhereEvaluate.getBoundary()==0)
        {
            Real x = pointWhereEvaluate.getX();
            Real y = pointWhereEvaluate.getY();
            
            curvature = 0.0;
        }
    }
    else
    {
        cout << "!! !! !! Unknown example\n";
        exit(1);
    }
    
    REAL error = fabs(curvature - approximation)/curvature;    
    return(error);
}

REAL computeErrorGaussianCurvature(UInt example, point pointWhereEvaluate, REAL approximation)
{
    REAL curvature  = approximation;
    if(example==1)
    {
        curvature = 4.;
    }
    else if(example==2)
    {
        // I do not chek the boundary 
        if(pointWhereEvaluate.getBoundary()==0)
        {
            Real x = pointWhereEvaluate.getX();
            Real y = pointWhereEvaluate.getY();
            
            curvature = 4. / ((1.+4.*x*x+4.*y*y)*(1.+4.*x*x+4.*y*y));
        }
    }
    else if(example==3)
    {
        // I do not chek the boundary 
        if(pointWhereEvaluate.getBoundary()==0)
        {
            Real x = pointWhereEvaluate.getX();
            Real y = pointWhereEvaluate.getY();
            
            REAL pi2 = M_PI*M_PI;
            REAL pi4 = pi2*pi2;
            
            REAL sinX2 = sin(2.*M_PI*x);
            REAL sinY2 = sin(2.*M_PI*y);            
            REAL cosX2 = cos(2.*M_PI*x);
            REAL cosY2 = cos(2.*M_PI*y);
            
            
            curvature = (4.*pi4*(sinX2*sinY2-cosX2*cosY2))/(2.*pi2*(cosX2*sinY2+cosY2*sinX2+1.)*(cosX2*sinY2+cosY2*sinX2+1.));
        }
    }
    else
    {
        cout << "!! !! !! Unknown example\n";
        exit(1);
    }
    
    REAL error = fabs(curvature - approximation);
    if(fabs(curvature)>1e-10)
        error /= fabs(curvature);
    
    return(error);
}


