#include <iostream>

#include "meshSimplification.h"


using namespace geometry;
using namespace std;

int main(int argc, char * argv[])
{	
    // variabili in uso 
    mesh2d<Triangle> surf;
    createFile  file;
    downloadMesh down;
        
    // Check of the input
    if(argc!=3)
    {
        cout << "Wrong input for this function" << endl;
        cout << "1) file extension 1=.inp, 2=.vtk\n";
        cout << "2) name of the file\n";
        exit(1);
    }
    
    //-------------------------------------------------------------------------
    //                          save the parameters
    //-------------------------------------------------------------------------
    UInt extension = atoi(argv[1]);
    string filename(argv[2]);
    
    //-------------------------------------------------------------------------
    //                           get the file 
    //-------------------------------------------------------------------------
    if(extension==1)
    {
        down.fileFromParaview(filename, &surf);
    }
    else if(extension==2)
    {
        down.fileFromVTK(filename, &surf);
    }
    else 
    {
        cout << "!! !! Unknown file extension " << extension << std::endl;
        exit(1);
    }
    
    //-------------------------------------------------------------------------
    //                         apply the smoothing 
    //-------------------------------------------------------------------------

    // run the smoothing 
    isotropicQuality2d<Triangle> iso(&surf);
    
    std::vector<Real> qual(surf.getNumElements());
    for(UInt i=0; i<qual.size(); ++i)
        qual[i] = iso.triangleQual(i);
    
    Real maxQual = *std::max_element(qual.begin(), qual.end());
    Real minQual = *std::min_element(qual.begin(), qual.end());
    Real meanQual = std::accumulate(qual.begin(), qual.end(), 0.0)/static_cast<Real>(qual.size());
    
    
    std::cout << "\n\nQuality summary\n";
    std::cout << " mean quality: " << meanQual << std::endl;
    std::cout << "  max quality: " << maxQual << std::endl;
    std::cout << "  min quality: " << minQual << std::endl << std::endl;
    
    iso.printElementQuality("qual.inp");
    
}

