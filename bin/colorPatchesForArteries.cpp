#include <iostream>

#include "meshSimplification.h"


using namespace geometry;
using namespace std;

void findBoundary(mesh2d<Triangle> & surf, string & outputName, mesh1d<Line> & border);
void fixAloneTriangles(mesh2d<Triangle> & surf);

int main(int argc, char * argv[])
{	
    // variabili in uso 
    mesh2d<Triangle> surf;
    createFile  file;
    downloadMesh down;
    
    // parameters 
    string outputName("output");
    
    // Check of the input
    if(argc!=3 && argc!=4)
    {
        cout << "Wrong input for this function" << endl;
        cout << "1) file extension 1=.inp, 2=.vtk\n";
        cout << "2) name of the file\n";
        cout << "3) name of the output (default output)\n";
        exit(1);
    }
    
    //-------------------------------------------------------------------------
    //                          save the parameters
    //-------------------------------------------------------------------------
    UInt extension = atoi(argv[1]);
    string filename(argv[2]);
    
    if(argc==4)     outputName = argv[3];
    
    //-------------------------------------------------------------------------
    //                           get the file 
    //-------------------------------------------------------------------------
    if(extension==1)
    {
        down.fileFromParaview(filename, &surf);
    }
    else if(extension==2)
    {
        down.fileFromVTK(filename, &surf);
    }
    else 
    {
        cout << "!! !! Unknown file extension " << extension << std::endl;
        exit(1);
    }
        
    // print the cost function
    ostringstream initailName;
    initailName << outputName << "_initial.inp";
    cout << "writing file " << initailName.str() << endl;
    file.fileForParaview(initailName.str(), &surf);
    
    //-------------------------------------------------------------------------
    //                         find the boundaries 
    //-------------------------------------------------------------------------

    // find the boundary accornding to the jump 
    mesh1d<Line> border;
    findBoundary(surf, outputName, border);
    
    // diffuse the virus
    virus2d<Triangle> vir;
    vir.setMeshPointer(&surf);
    vir.virusDiffusion(&border);
    
    // fix some triangle that are alone 
    fixAloneTriangles(surf);
    
    // print the cost function
    ostringstream coloredName;
    coloredName << outputName << "_final.inp";
    cout << "writing file " << coloredName.str() << endl;
    file.fileForParaview(coloredName.str(), &surf);
}

//------------------------------------------------------------------------
//                   FUNCTION IMPLEMENTATION 
//------------------------------------------------------------------------
void findBoundary(mesh2d<Triangle> & surf, string & outputName, mesh1d<Line> & border)
{
    tricky2d<Triangle> trik;
    trik.setMeshPointer(&surf);
    
    geoElement<Line> tmpLine;
    set<geoElement<Line> > tmpListOfLines;
    for(UInt i=0; i<surf.getNumElements(); ++i)
    {
        for(UInt j=0; j<3; ++j)
        {
            UInt id1 = surf.getElement(i).getConnectedId(j%3);
            UInt id2 = surf.getElement(i).getConnectedId((j+1)%3);
            
            vector<UInt> eleOnEdge;
            trik.elementOnEdge(id1, id2, &eleOnEdge);
            
            if(eleOnEdge.size()==2 && !trik.normalVarr(eleOnEdge[0], eleOnEdge[1], M_PI*(1./4.)))
            {
                tmpLine.setConnectedId(0, id1);
                tmpLine.setConnectedId(1, id2);
                
                tmpListOfLines.insert(tmpLine);
            }
        }
    }
    
    border.insertNode(surf.getNodePointer());
    border.insertElement(&tmpListOfLines);
    
    ostringstream borderName;
    borderName << outputName << "_border.inp";
    cout << "writing file " << borderName.str() << endl;
    
    createFile  file;
    file.fileForParaview(borderName.str(), &border);
}

UInt makeMapOfColors(mesh2d<Triangle> & surf, map<UInt, UInt> & colorToNumberOfTriangles)
{
    // count the colors 
    set<UInt> colors;
    for(UInt i=0; i<surf.getNumElements(); ++i) colors.insert(surf.getElement(i).getGeoId());
    
    // count how many triangles are associated with the colors
    colorToNumberOfTriangles.clear();
    for(set<UInt>::iterator it = colors.begin(); it!=colors.end(); ++it)   colorToNumberOfTriangles[*it] = 0;
    
    for(UInt i=0; i<surf.getNumElements(); ++i)
    {
        UInt geoId = surf.getElement(i).getGeoId();
        
        UInt actual = colorToNumberOfTriangles.find(geoId)->second;
        ++actual;
        colorToNumberOfTriangles[geoId] = actual;
    }
    
    // final count 
    cout << "List of the colors and the triangles\n";
    for(set<UInt>::iterator it = colors.begin(); it!=colors.end(); ++it)
        cout << " material id: " << *it << "  " << colorToNumberOfTriangles[*it] << endl;
    
    
    return(colors.size());
}

void colorLessFrequentWithTheMostFrequent(mesh2d<Triangle> & surf, map<UInt, UInt> & colorToNumberOfTriangles)
{
    // find the more frequent and the less frequent 
    UInt totalColor = colorToNumberOfTriangles.size(); 
    UInt minColor = colorToNumberOfTriangles[1];
    UInt maxColor = colorToNumberOfTriangles[1];
    for(UInt i=2; i<=totalColor; ++i)
    {
        UInt actualNumber = colorToNumberOfTriangles[i];
        if(minColor>actualNumber)   minColor = i;
        if(maxColor<actualNumber)   maxColor = i;
    }
    
    // color the less frequent 
    for(UInt i=0; i<surf.getNumElements(); ++i)
    {
        UInt geoId = surf.getElement(i).getGeoId();
        if(geoId==minColor) 
        {
            surf.getElementPointer(i)->setGeoId(maxColor);
        }
    }
}

void fixAloneTriangles(mesh2d<Triangle> & surf)
{
    
    map<UInt, UInt> colorToNumberOfTriangles;
    UInt numOfColors = makeMapOfColors(surf, colorToNumberOfTriangles);
    
    while(numOfColors>3)
    {
        colorLessFrequentWithTheMostFrequent(surf, colorToNumberOfTriangles);
        numOfColors = makeMapOfColors(surf, colorToNumberOfTriangles);
    }
}




