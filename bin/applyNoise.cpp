#include <iostream>

#include "meshSimplification.h"


using namespace geometry;
using namespace std;

// method to make some noise on the data 
void noiseTheData(mesh2d<Triangle> & surf, int frequency);

int main(int argc, char * argv[])
{	
    // variabili in uso 
    mesh2d<Triangle> surf;
    createFile  file;
    downloadMesh down;
    
    // parameters 
    int frequency=10;
    string outputName("output");
    
    // Check of the input
    if(argc!=3 && argc!=4 && argc!=5)
    {
        cout << "Wrong input for this function" << endl;
        cout << "1) file extension 1=.inp, 2=.vtk\n";
        cout << "2) name of the file\n";
        cout << "3) name of the output (default output)\n";
        cout << "4) frequency of the (noise default 10)\n";
        exit(1);
    }
    
    //-------------------------------------------------------------------------
    //                          save the parameters
    //-------------------------------------------------------------------------
    UInt extension = atoi(argv[1]);
    string filename(argv[2]);
    
    if(argc==4)
    {
        outputName = argv[3];
    }
    else if(argc==5)
    {
        outputName = argv[3];   
        frequency = atoi(argv[4]);
    }
    
    //-------------------------------------------------------------------------
    //                           get the file 
    //-------------------------------------------------------------------------
    if(extension==1)
    {
        down.fileFromParaview(filename, &surf);
    }
    else if(extension==2)
    {
        down.fileFromVTK(filename, &surf);
    }
    else 
    {
        cout << "!! !! Unknown file extension " << extension << std::endl;
        exit(1);
    }
    
    //-------------------------------------------------------------------------
    //                           make some noise 
    //-------------------------------------------------------------------------
    noiseTheData(surf,frequency);
    
    // print the cost function
    ostringstream noiseName;
    noiseName << outputName << "_noisy.inp";
    cout << "writing file " << noiseName.str() << endl;
    file.fileForParaview(noiseName.str(), &surf);
}

//
// Implementation of functions
//
void noiseTheData(mesh2d<Triangle> & surf, int frequency)
{
    tricky2d<Triangle> trick(&surf);
    
    int couter = 0;
    std::vector<point> newPoints(surf.getNumNodes());
    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        newPoints[i] = surf.getNode(i);
        if((i%frequency)==0)
        {
            // get all the points around 
            std::vector<UInt> around;
            trick.getNodeAround(i, 1, &around);
            
            // get the distance for the noise
            Real dist = 0.;
            for(UInt j=0; j<around.size(); ++j)
            {
                REAL tmpDist = (surf.getNode(i)-surf.getNode(around[j])).norm2();
                if(dist<tmpDist)
                    dist = tmpDist;
            }
            
            // get the new position
            point normal = trick.getPointNormal(i);
            // effect updown
            if((couter%2)==1) normal = normal * (-1.);
            newPoints[i].add(normal,dist);
            ++couter;
        }
    }

    for(UInt i=0; i<surf.getNumNodes(); ++i)
    {
        surf.getNodePointer(i)->setX(newPoints[i].getX());
        surf.getNodePointer(i)->setY(newPoints[i].getY());
        surf.getNodePointer(i)->setZ(newPoints[i].getZ());
    }
    
}

