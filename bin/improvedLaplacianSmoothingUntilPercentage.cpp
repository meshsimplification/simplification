#include <iostream>

#include "meshSimplification.h"


using namespace geometry;
using namespace std;

int main(int argc, char * argv[])
{	
    // variabili in uso 
    mesh2d<Triangle> surf;
    createFile  file;
    downloadMesh down;
    
    // parameters 
    Real perc = 0.1;
    Real beta = 0.6;
    Real alpha = 0.1;
    string outputName("output");
    
    // Check of the input
    if(argc!=3 && argc!=4 && argc!=6 && argc!=7)
    {
        cout << "Wrong input for this function" << endl;
        cout << "1) file extension 1=.inp, 2=.vtk\n";
        cout << "2) name of the file\n";
        cout << "3) percentage to reach (default 0.1)\n";
        cout << "4) alpha parameter (default 0.1) \n";
        cout << "5) beta parameter (default 0.6) \n";
        cout << "6) name of the output (default output)\n";
        exit(1);
    }
    
    //-------------------------------------------------------------------------
    //                          save the parameters
    //-------------------------------------------------------------------------
    UInt extension = atoi(argv[1]);
    string filename(argv[2]);
    
    if(argc==4)
    {
        perc = atof(argv[3]);
    }
    else if(argc==6)
    {
        perc = atof(argv[3]);
        alpha = atof(argv[4]);
        beta = atof(argv[5]);
    }
    if(argc==7)
    {
        perc = atof(argv[3]);
        alpha = atof(argv[4]);
        beta = atof(argv[5]);
        outputName = argv[6];
    }
    
    //-------------------------------------------------------------------------
    //                           get the file 
    //-------------------------------------------------------------------------
    if(extension==1)
    {
        down.fileFromParaview(filename, &surf);
    }
    else if(extension==2)
    {
        down.fileFromVTK(filename, &surf);
    }
    else 
    {
        cout << "!! !! Unknown file extension " << extension << std::endl;
        exit(1);
    }
        
    // print the cost function
    ostringstream initailName;
    initailName << outputName << "_initial.inp";
    cout << "writing file " << initailName.str() << endl;
    file.fileForParaview(initailName.str(), &surf);
    
    //-------------------------------------------------------------------------
    //                         apply the smoothing 
    //-------------------------------------------------------------------------

    // run the smoothing 
    HSSmoothing smooth(&surf, alpha, beta);
    smooth.runTheSmoothingAccordingToTheVolume(perc);
    
    // plot the volumes 
    ostringstream volumeName;
    volumeName << outputName << "_volumes.m";
    smooth.writeMatlabFileWithVolumes(volumeName.str());

    // plot the volumes 
    ostringstream qualAndVolName;
    qualAndVolName << outputName << "_qualityAndVolume.m";
    smooth.writeMatlabFileWithQualityAndVolumes(qualAndVolName.str());
    
    // print the cost function
    ostringstream denoiseName;
    denoiseName << outputName << "_final.inp";
    cout << "writing file " << denoiseName.str() << endl;
    file.fileForParaview(denoiseName.str(), &surf);
}
