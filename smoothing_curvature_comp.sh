#!/usr/local/Cellar/bash/4.4.12/bin/bash

# this file runs a matlab code for the brain partition into the regions, saves those 
# files and computes the curvature for every region. Then all the curvature values are
# processed into one

echo Start computations date

DATA_NAME='uniform_right_final'

s=.inp
sort=_sorted.mat
partreg=_part_reg

MY_DATA=/Users/jkroos/Documents/Simplification_Stuff/simplification/Summary/paper/data/brain
CURV_DIR=/Users/jkroos/Documents/Simplification_Stuff/simplification/build


cd "$MY_DATA"
mkdir Curvature_Partition
echo "############################################"
echo "# 3.1 run matlabcode for region partition   #"
echo "# for the LEFT hemisphere   #"
echo "############################################"
mystring="matlab -nodisplay -nodesktop -nosplash -r \"partition_brain('$DATA_NAME$s','$DATA_NAME$sort');exit\""
eval $mystring
echo "# copy files to the folder Curvature_Partition"
mv *'_part_reg'*.vtk Curvature_Partition/


echo "############################################"
echo "# 4.1 run c code for the surface computation#"
echo "# on each of the vtk files produced by	 #"
echo "# the matlab file partition_brain()        #"
echo "############################################"
name=$DATA_NAME'_part_reg'

cd "$CURV_DIR"
for i in {0..34}
  do 
    echo "Curvature computation for region: " $i

    # create string for filename and move that filename of the partitioned reg to the mesh folder
    str=$name$i
    cp "$MY_DATA/Curvature_Partition/$str.vtk" ../mesh/

    # run curvature evaluation program
    ./curvatureEvaluation 2 ../mesh/$str.vtk $str

    # copy files to previous folder and remove the one in the mesh folder
    mv $str* "$MY_DATA/Curvature_Partition"
    rm ../mesh/$str.vtk
    st=_initial.inp
    rm "$MY_DATA/Curvature_Partition/$str$st"
  done

echo "############################################"
echo "# 5.1 run matlabcode putting the curvature  #"
echo "# values of all the regions together in    #"
echo "#one file                                  #"
echo "############################################"
cd "$MY_DATA"
mystring="matlab -nodisplay -nodesktop -nosplash -r \"combine_curvature_partition('$DATA_NAME$s','$DATA_NAME$sort');exit\""
eval $mystring
echo "# create complete curvature file"
rm -rf Curvature_Partition
echo "# delete the curvature partition file"

echo "# END #"
